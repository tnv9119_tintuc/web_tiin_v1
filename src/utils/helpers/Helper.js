import {DECRYPT_KEY, MOCHAVIDEO_CLIENT_ID, ID_CHANNEL_NOT_SHOW, USER_INFO, EXPIRE_TIME} from "../../config/Config";

var xxtea = require('xxtea-node');
import Cookies from 'universal-cookie';
import AuthService from "../../services/auth/AuthService";

var rand = require('../../utils/php/math/rand');
var md5 = require('../../utils/php/strings/md5');

import {
    getUA,
} from "react-device-detect";

import Router from 'next/router';


var ranges = [
    {divider: 1e18, suffix: 'E'},
    {divider: 1e15, suffix: 'P'},
    {divider: 1e12, suffix: 'T'},
    {divider: 1e9, suffix: 'G'},
    {divider: 1e6, suffix: 'M'},
    {divider: 1e3, suffix: 'K'}
];

function formatNumber(n) {
    if (!isNaN(n) && n > 0) {
        for (var i = 0; i < ranges.length; i++) {
            if (n >= ranges[i].divider) {
                return (n / ranges[i].divider).toFixed(1).toString().replace(".0", "") + ranges[i].suffix;
            }
        }

        return n.toString();
    }
    return 0;

}

function formatWithCommas(n) {
    if (!isNaN(n) && n > 0) {
        return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    return 0;
}

var templates = {
    prefix: "",
    suffix: " trước",//" ago",
    seconds: "gần 1 phút",//"less than a minute",
    minute: "1 phút",//"about a minute",
    minutes: "%d phút",//"%d minutes",
    hour: "1 giờ",//"about an hour",
    hours: "%d giờ",//"about %d hours",
    day: "1 ngày",//"a day",
    days: "%d ngày",//"%d days",
    month: "1 tháng",//"about a month",
    months: "%d tháng",//"%d months",
    year: "1 năm",//"about a year",
    years: "%d năm" //"%d years"
};
var template = function (t, n) {
    return templates[t] && templates[t].replace(/%d/i, Math.abs(Math.round(n)));
};

function getTimeAgo(time) {
    if (!time) return;

    //time = time.replace(/\.\d+/, ""); // remove milliseconds
    //time = time.replace(/-/, "/").replace(/-/, "/");
    //time = time.replace(/T/, " ").replace(/Z/, " UTC");
    //time = time.replace(/([\+\-]\d\d)\:?(\d\d)/, " $1$2"); // -04:00 -> -0400
    //time = new Date(time * 1000 || time);
    time = new Date(time);

    var now = new Date();
    var seconds = ((now.getTime() - time) * .001) >> 0;
    var minutes = seconds / 60;
    var hours = minutes / 60;
    var days = hours / 24;
    var years = days / 365;

    return templates.prefix + (
        seconds < 45 && template('seconds', seconds) ||
        seconds < 90 && template('minute', 1) ||
        minutes < 45 && template('minutes', minutes) ||
        minutes < 90 && template('hour', 1) ||
        hours < 24 && template('hours', hours) ||
        hours < 42 && template('day', 1) ||
        days < 30 && template('days', days) ||
        days < 45 && template('month', 1) ||
        days < 365 && template('months', days / 30) ||
        years < 1.5 && template('year', 1) ||
        template('years', years)
    ) + templates.suffix;
}

function getPathname(pathname) {
    let regexV = /^(.+)-v([0-9]+)/;
    let regexT = /^(.+)-t([0-9]+)/;
    pathname = pathname.replace(".html", "");
    let end = pathname.length;
    let beginSearch = pathname.lastIndexOf("?");
    if (beginSearch > -1) {
        let seachStr = pathname.slice(beginSearch, end);
        pathname = pathname.replace(seachStr, "");
    }
    if (regexV.test(pathname)) {
        return pathname;
    } else if (regexT.test(pathname)) {
        return pathname.substring(0, pathname.lastIndexOf('-t')) + '-v' + pathname.substring(pathname.lastIndexOf('-t') + 2);
    }

    return pathname;
}

function getIdFromPathname(pathname) {
    let id = null;

    let regexV = /^(.+)-v([0-9]+)/;
    let regexT = /^(.+)-t([0-9]+)/;
    let regexM = /^(.+)-m([0-9]+)/;
    let end = pathname.length;
    let beginSearch = pathname.lastIndexOf("?");
    if (beginSearch > -1) {
        let seachStr = pathname.slice(beginSearch, end);
        pathname = pathname.replace(seachStr, "");
    }

    end = pathname.length;
    if (regexV.test(pathname)) {
        let begin = pathname.lastIndexOf("-v");
        id = pathname.slice(begin + 2, end);
    } else if (regexT.test(pathname)) {
        let begin = pathname.lastIndexOf("-t");
        id = pathname.slice(begin + 2, end);
    }else if (regexM.test(pathname)) {
        let begin = pathname.lastIndexOf("-m");
        id = pathname.slice(begin + 2, end);
    }
    return id;
}

function replaceUrl(url) {
    if (url && typeof url == "string") {
        return url.replace("http://video.mocha.com.vn", "").replace("http://m.video.mocha.com.vn", "").replace("http://webvideo.mocha.com.vn", "").replace("http://wapvideo.mocha.com.vn", "").replace("http://mocha.com.vn", "").replace(".html", "");
    }
    return '/';
}

function formatDate(inputDate) {
    var date = new Date(inputDate);
    if (!isNaN(date.getTime())) {
        var month = date.getMonth();
        var getMonth = month + 1;
        return date.getDate() + '/' + getMonth + '/' + date.getFullYear();
    }
}


function format(inputDate) {
    var date = new Date(inputDate);
    if (!isNaN(date.getTime())) {
        var month = date.getMonth();
        var getMonth = month + 1;
        return date.getDate() + '/' + getMonth + '/' + date.getFullYear();
    }
}


var remove084 = function (username) {
    if (username.startsWith('0')) {
        username = username.substring(1, username.length);
        return username;
    }

    if (username.startsWith('84')) {
        username = username.substring(2, username.length);
        return username;
    }

    if (username.startsWith('+84')) {
        username = username.substring(3, username.length);
        return username;
    }
    return username;
}

//bắt lỗi 016
var validatePhoneFormat016 = function (username) {
    // var intRegex = /^[0-1-6 -()+]+$/;
    var intRegex = /^016[2-9]{1}[0-9]{7}$/;
    if (!intRegex.test(username)) {
        return false;
    }
    return true;
};

var validatePhoneStartWith16 = function (username) {
    var arrayStartWith = ["16"];
    var i;
    for (i = 0; i < arrayStartWith.length; i++) {
        if (username.startsWith(arrayStartWith[i])) {
            return false;
        }
    }
    return true;
};

var validatePhoneFormat = function (username) {
    var intRegex = /^[0-9 -()+]+$/;
    if (!intRegex.test(username)) {
        return false;
    }
    return true;
};


function highlightWords(fstring, searchValue) {

    searchValue = unsignString(searchValue);
    //alert(searchValue);
    let string = unsignMaxString(fstring);
    let offs = searchValue.length;
    //alert(string);
    var myKey = trim(searchValue);
    var myStringVar = trim(string);
    if (myStringVar) {
        var first = myStringVar.indexOf(myKey);
        if (first != -1) {
            //alert(first);
            var last = first + offs;
            if (last != -1) {
                if (last > first) {
                    var text = fstring.substring(first, last);

                    fstring = fstring.replace(trim(text), '<span>' + trim(text) + '</span>');
                }
            }

        }
    }
    return fstring;
}

function unsignString(str) {
    if (str != undefined && str != null && str != '') {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, " ");
        /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
        str = str.replace(/-+-/g, ""); //thay thế 2- thành 1-
        str = str.replace(/^\-+|\-+$/g, "");
        str = str.replace("-", " ");
        //cắt bỏ ký tự - ở đầu và cuối chuỗi
    }
    return str;
}

function unsignMaxString(str) {
    if (str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace("-", " ");
        //cắt bỏ ký tự - ở đầu và cuối chuỗi
        return str;
    } else {
        return null;
    }
}

function trim(s) {
    if (s) {
        var l = 0;
        var r = s.length - 1;
        while (l < s.length && s[l] == ' ') {
            l++;
        }
        while (r > l && s[r] == ' ') {
            r -= 1;
        }
        return s.substring(l, r + 1);
    } else {
        return null;
    }
}

function md5Str(str) {
    return md5(str);
}


function checkObjExist(obj, strings) {
    //var args = Array.prototype.slice.call(arguments, 1);
    var args = strings.split('.');

    for (var i = 0; i < args.length; i++) {
        if (!obj || !obj.hasOwnProperty(args[i])) {
            return false;
        }
        obj = obj[args[i]];
    }
    return true;
}

function checkArrNotEmpty(obj, strings) {

    //var args = Array.prototype.slice.call(arguments, 1);
    var args = strings.split('.');

    for (var i = 0; i < args.length; i++) {
        if (!obj || !obj.hasOwnProperty(args[i])) {
            return false;
        }
        obj = obj[args[i]];
    }

    if (!(obj.constructor === Array && obj.length > 0)) {
        return false;
    }
    return true;
}


function logShareFb(id = 0, item_type = 0) {
    LogApi.logShareFB(id, item_type);

}

function count_words(strs) {
    let strConvert = strip(strs);

    //exclude  start and end white-space
    let str1 = strConvert.replace(/(^\s*)|(\s*$)/gi, "");
    //convert 2 or more spaces to 1
    str1 = str1.replace(/[ ]{2,}/gi, " ");
    // exclude newline with a start spacing
    str1 = str1.replace(/\n /, "\n");
    return str1.split(' ').length;
}


//VIDEO MOCHA START
function getDate(date) {
    var myDate = new Date(date);
    var year = myDate.getFullYear();
    var month = myDate.getMonth() + 1;
    if (month <= 9)
        month = '0' + month;
    var day = myDate.getDate();
    if (day <= 9)
        day = '0' + day;
    var prettyDate = year + '-' + month + '-' + day;
    return prettyDate;
}

function getDateTime() {
    var m = new Date();
    var dateString = m.getFullYear() + "-" +
        ("0" + (m.getMonth() + 1)).slice(-2) + "-" +
        ("0" + m.getDate()).slice(-2) + " " +
        ("0" + m.getHours()).slice(-2) + ":" +
        ("0" + m.getMinutes()).slice(-2) + ":" +
        ("0" + m.getSeconds()).slice(-2);
    return dateString;
}

function checkInputUploadYoutube(url, category) {

    if (url.trim() === '') {
        return {
            error: 1,
            message: "Mời bạn nhập link youtube."
        };
    }
    var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    if (!url.match(p)) {
        return {
            error: 1,
            message: "Mời bạn nhập lại link youtube."
        };
    }

    if (isNaN(parseFloat(category)) || !isFinite(category) || category <= 0) {
        return {
            error: 1,
            message: "Mời bạn chọn chuyên mục."
        };
    }

    return {
        error: 0,
        message: ''
    };
}

function checkInputUploadByUser(name, desc, category) {

    if (name.trim() === '') {
        return {
            error: 1,
            message: "Mời bạn nhập tên video."
        };
    }

    if (isNaN(parseFloat(category)) || !isFinite(category) || category <= 0) {
        return {
            error: 1,
            message: "Mời bạn chọn chuyên mục."
        };
    }

    return {
        error: 0,
        message: ''
    };
}

function decryptData(dataEnc) {
    try {
        let tokenDecrypted = xxtea.decryptToString(dataEnc, DECRYPT_KEY);
        return JSON.parse(tokenDecrypted);
    } catch (e) {}
    return null;
}

function encryptData(data) {
    try {
        return xxtea.encryptToString(JSON.stringify(data), DECRYPT_KEY);

    } catch (e) {}
    return null;
}

function encryptStr(data) {
    try {
        return xxtea.encryptToString(data, DECRYPT_KEY);
    } catch (e) {}
    return null;
}

function decryptStr(data) {
    try {
        return xxtea.decryptToString(data, DECRYPT_KEY);
    } catch (e) {}
    return null;
}

function getUserInfo() {
    let expireTime = getCookie(EXPIRE_TIME);
    if (!expireTime) {
        removeCookie(USER_INFO);
        return null;
    }

    let userInfo = getCookie(USER_INFO);
    if (userInfo) {
        try {
            var dataDecrypted = xxtea.decryptToString(userInfo, DECRYPT_KEY);
            return JSON.parse(dataDecrypted);
        } catch (e) {}
    }
    return null;
}

function checkUserLoggedIn() {
    let expireTime = getCookie(EXPIRE_TIME);
    if (!expireTime) {
        removeCookie(USER_INFO);
        return false;
    }

    let user = getCookie(USER_INFO);
    if (user) {
        return true;
    }
    return false;
}

function getTokenParamForUrl() {
    let userInfo = getCookie(USER_INFO);
    if (userInfo) {
        try {
            let dataDecrypted = JSON.parse(xxtea.decryptToString(userInfo, DECRYPT_KEY));
            return encodeURIComponent(dataDecrypted.token);
        } catch (e) {}
    }

    return '';
}

function getToken() {
    let userInfo = getCookie(USER_INFO);
    if (userInfo) {
        try {
            var dataDecrypted = JSON.parse(xxtea.decryptToString(userInfo, DECRYPT_KEY));
            return dataDecrypted.token;
        } catch (e) {}
    }

    return '';
}

function setUserInfo(user) {
    //var timeExp = new Date();
    //timeExp.setTime(timeExp.getTime() + 7200000); //2hours
    setCookie(USER_INFO, user);
}

function hasNumber(str) {
    var numbers = /[0-9]/g;
    if (str.match(numbers)) {
        return true;
    }
    return false;
}

function hasCharacter(str) {
    var lowerCaseLetters = /[a-z]|[A-Z]/g;
    if (str.match(lowerCaseLetters)) {
        return true;
    }
    return false;
}

function shareAll(type) {
    var urlCur = document.URL.replace(window.location.search, '');
    if (type === "facebook") {
        //href = "https://www.facebook.com/sharer.php?u=<%=url %>?userid=0?&_rdr"
        window.open('https://m.facebook.com/sharer.php?u=' + urlCur + '?src=' + type, '_blank');
    } else if (type === "google") {
        //href="https://plus.google.com/share?url=<%=url%>"
        window.open('https://plus.google.com/share?url=' + urlCur + '?src=' + type, '_blank');
    } else if (type === "twitter") {
        //href="https://twitter.com/intent/tweet?url=<%=url %>"
        window.open('https://twitter.com/intent/tweet?url=' + urlCur + '?src=' + type, '_blank');
    } else if (type === "linkedin") {
        //href="https://www.linkedin.com/shareArticle?mini=true&url=<%=url %>"
        window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + urlCur + '?src=' + type, '_blank');
    } else if (type === "mail") {
        //href="mailto:?body=<%=url %>"
        window.open('mailto:?body=' + urlCur + '?src=' + type, '_self');
    }
}

function copyUrlShare() {
    var urlText = document.getElementById('txtUrlShare');
    urlText.select();
    document.execCommand("Copy");

    /*$("#popshareAll").hide();
    $("#popup_copy").fadeIn("slow");
    setTimeout(function() { $("#popup_copy").fadeOut("slow"); }, 3000);*/
}

function checkTokenExpired(data) {
    if (data && (data.code === 419 || data.code === 401)) {
        //AuthService.logout();

        removeCookie(USER_INFO);
        removeCookie(EXPIRE_TIME);

        let location = {
            pathname: '/login',
            // state: history.location.pathname
        };
        Router.push(location);
    }
    return true;
}

function renewToken(data) {
    if (data && data.code === 200 && getCookie(EXPIRE_TIME)) {
        let timeExp = new Date().getTime() + 1800000;
        let expires = new Date(timeExp); // 30 minutes
        setCookie(EXPIRE_TIME, timeExp, expires);
    }
}


function inArr(arr, value) {
    if (arr.indexOf(value) > -1) {
        return true;
    }
    return false;
}

function getAvatar(phoneNumber = '00') {
    if (!isNaN(phoneNumber) && phoneNumber > 0) {
        return "http://hlvip2.mocha.com.vn/api/thumbnail/download-cache?ac=" + phoneNumber + "&t=20180503&u=video";
    }
    return "http://hlvip2.mocha.com.vn/api/thumbnail/download-cache?ac=00&t=20180503&u=video";
    //return "http://hlvip2.mocha.com.vn/api/thumbnail/download-cache?ac=" + xxtea.decryptToString(phoneNumber, DECRYPT_KEY) + "&t=20180503&u=video";
}

function showphoneNumber(phoneNumber) {
    if (phoneNumber && phoneNumber.length > 3) {
        return phoneNumber.substring(0, phoneNumber.length - 3) + "***";
    }
    return phoneNumber;
}


function checkIsVideoDetail(path) {
    let regexV = /^(.+)-v([0-9]+)/;
    let regexT = /^(.+)-t([0-9]+)/;
    if (regexV.test(path) || regexT.test(path)) {
        return true;
    }
    return false;
}

function getIdByLink(link, sLink) {
    link = replaceUrl(link);

    let end = link.length;
    let beginSearch = link.lastIndexOf("?");
    if (beginSearch > -1) {
        let seachStr = link.slice(beginSearch, end);
        link = link.replace(seachStr, "");
    }
    // let channelLink = user.channelInfo.link;
    let channelId = 0;
    if (link) {
        channelId = link.substring(
            link.lastIndexOf(sLink) + 3,
            link.length
        );
    }
    if (!isNaN(channelId)) {
        return parseInt(channelId);
    }
    return 0;
}

function generateClientId() {
    let timeStamp = new Date().getTime();
    let randomInt = rand(1, 1000000);
    return md5(timeStamp * randomInt);
}

function setClientId() {
    let clientId = getCookie(MOCHAVIDEO_CLIENT_ID);
    if (!clientId) {
        clientId = generateClientId();
        setCookie(MOCHAVIDEO_CLIENT_ID, clientId);
    }
}

function getClientId() {
    let clientId = getCookie(MOCHAVIDEO_CLIENT_ID);
    if (clientId) {
        return clientId;
    }
    return '';
}

function getUserAgent() {
    return getUA;
}

function getCookie(key) {
    var name = key + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}

function setCookie(key, value, expires = 0, path = '/') {
    let expireStr = expires ? ";expires=" + expires.toUTCString() : '';
    let pathStr = ";path=" + path;

    document.cookie = key + "=" + value + expireStr + pathStr;
}

function removeCookie(key) {
    document.cookie = key + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}

function checkIsChannelNotShowVideo(data) {
    // loc video trong kenh
    if (data) {
        for (var i = 0; i < data.length; i++) {
            let dataChannels = data[i].channels[0];
            if (checkObjExist(dataChannels, 'id')) {
                if (data[i].channels[0].id && data[i].channels[0].id == ID_CHANNEL_NOT_SHOW) {
                    data.splice(i, 1);
                    i--;
                }
            }
        }
        return data;
    }
    return [];
}

function checkIsChannelNotShow(data) {
    //loc kenh
    if (data) {
        for (var i = 0; i < data.length; i++) {
            if (checkObjExist(data[i], 'id')) {
                if (data[i].id == ID_CHANNEL_NOT_SHOW) {
                    data.splice(i, 1);
                    i--;
                }
            }
        }
        return data;
    }
    return [];
}

function sortChannelSearch(data, value) {
    //loc kenh
    var query = '';
    if (value) {
        var replaceValue = value.replace(/[^0-9a-zàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ\s]/gi, '');
        var b = trim(replaceValue);
        if (b) {
            var replace = b.replace(/ /g, "-");
            query = replace.replace(/-+-/g, "-");
        }
    }
    var listResult = [];

    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].name).toUpperCase();
        const nameQuery = removeAccents(value).toUpperCase();
        if (nameQuery === nameUpperCase) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExist(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }

    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].name).toUpperCase();
        const nameQuery = query.toUpperCase();
        if (nameQuery === nameUpperCase) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExist(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].name).toUpperCase();
        const nameQuery = removeAccents(value).toUpperCase();
        if (nameUpperCase.search(nameQuery) === 0) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExist(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].name).toUpperCase();
        const nameQuery = query.toUpperCase();
        if (nameUpperCase.search(nameQuery) === 0) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExist(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].name).toUpperCase();
        const nameQuery = removeAccents(value).toUpperCase();
        if (nameUpperCase.search(nameQuery) != -1) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExist(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].name).toUpperCase();
        const nameQuery = query.toUpperCase();
        if (nameUpperCase.search(nameQuery) != -1) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExist(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const newNameStr = data[i].name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        const newQueryStr = query.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        if (removeAccents(newNameStr) === removeAccents(newQueryStr)) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExist(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const newNameStr = data[i].name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        const newQueryStr = query.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        let a = removeAccents(newNameStr).toUpperCase();
        let b = removeAccents(newQueryStr).toUpperCase();
        if (a.search(b) === 0) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExist(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const newNameStr = data[i].name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        const newQueryStr = query.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        let a = removeAccents(newNameStr).toUpperCase();
        let b = removeAccents(newQueryStr).toUpperCase();
        if (a.search(b) != -1) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExist(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    return listResult;
}

function sortFilmSearch(data, value) {
    //loc kenh
    var query = '';
    if (value) {
        var replaceValue = value.replace(/[^0-9a-zàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ\s]/gi, '');
        var b = trim(replaceValue);
        if (b) {
            var replace = b.replace(/ /g, "-");
            query = replace.replace(/-+-/g, "-");
        }
    }
    var listResult = [];
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].name).toUpperCase();
        const nameQuery = removeAccents(value).toUpperCase();
        if (nameQuery === nameUpperCase) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExist(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].filmGroup.groupName).toUpperCase();
        const nameQuery = query.toUpperCase();
        if (nameQuery === nameUpperCase) {
            listResult.push(data[i]);
        }
    }
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].filmGroup.groupName).toUpperCase();
        const nameQuery = query.toUpperCase();
        if (nameUpperCase.search(nameQuery) === 0) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExistFilm(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].filmGroup.groupName).toUpperCase();
        const nameQuery = query.replace(/[^a-z0-9\s]/gi, '').toUpperCase();
        if (nameUpperCase.search(nameQuery) != -1) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExistFilm(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const newNameStr = data[i].filmGroup.groupName.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        const newQueryStr = query.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        if (removeAccents(newNameStr) === removeAccents(newQueryStr)) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExistFilm(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const newNameStr = data[i].filmGroup.groupName.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        const newQueryStr = query.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        let a = removeAccents(newNameStr).toUpperCase();
        let b = removeAccents(newQueryStr).toUpperCase();
        if (a.search(b) === 0) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExistFilm(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const newNameStr = data[i].filmGroup.groupName.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        const newQueryStr = query.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        let a = removeAccents(newNameStr).toUpperCase();
        let b = removeAccents(newQueryStr).toUpperCase();
        if (a.search(b) != -1) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExistFilm(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        if (listResult.length === 0)
            listResult.push(data[i]);
        else if (listResult.length > 0 && !checkExistFilm(listResult, data[i])) {
            listResult.push(data[i]);
        }
    }
    return listResult;
}

function sortVideoSearch(data, value) {
    var listResult = [];
    var query = '';
    if (value) {
        var replaceValue = value.replace(/[^0-9a-zàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ\s]/gi, '');
        var b = trim(replaceValue);
        if (b) {
            var replace = b.replace(/ /g, "-");
            query = replace.replace(/-+-/g, "-");
        }
    }
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].name).toUpperCase();
        const nameQuery = removeAccents(value).toUpperCase();
        if (nameQuery === nameUpperCase) {
            if (listResult.length === 0)
                listResult.push(data[i]);
            else if (listResult.length > 0 && !checkExist(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].name).toUpperCase();
        const nameQuery = query.toUpperCase();
        if (nameQuery === nameUpperCase) {
            if (listResult.length === 0) {
                listResult.push(data[i]);
            } else if (listResult.length > 0 && !checkExistVideo(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].name).toUpperCase();
        const nameQuery = query.toUpperCase();
        if (nameUpperCase.search(nameQuery) === 0) {
            if (listResult.length === 0) {
                listResult.push(data[i]);
            } else if (listResult.length > 0 && !checkExistVideo(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const nameUpperCase = removeAccents(data[i].name).toUpperCase();
        const nameQuery = query.replace(/[^a-z0-9\s]/gi, '').toUpperCase();
        if (nameUpperCase.search(nameQuery) != -1) {
            if (listResult.length === 0) {
                listResult.push(data[i]);
            } else if (listResult.length > 0 && !checkExistVideo(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const newNameStr = data[i].name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        const newQueryStr = query.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        if (removeAccents(newNameStr) === removeAccents(newQueryStr)) {
            if (listResult.length === 0) {
                listResult.push(data[i]);
            } else if (listResult.length > 0 && !checkExistVideo(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const newNameStr = data[i].name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        const newQueryStr = query.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        let a = removeAccents(newNameStr).toUpperCase();
        let b = removeAccents(newQueryStr).toUpperCase();
        if (a.search(b) === 0) {
            if (listResult.length === 0) {
                listResult.push(data[i]);
            } else if (listResult.length > 0 && !checkExistVideo(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const newNameStr = data[i].name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        const newQueryStr = query.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        let a = removeAccents(newNameStr).toUpperCase();
        let b = removeAccents(newQueryStr).toUpperCase();
        if (a.search(b) === 0) {
            if (listResult.length === 0) {
                listResult.push(data[i]);
            } else if (listResult.length > 0 && !checkExistVideo(listResult, data[i])) {
                listResult.push(data[i]);
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        const newNameStr = query.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
        var des;
        if (data[i].description != undefined && data[i].description != '') {
            des = data[i].description.replace(/[^0-9a-zàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ\s]/gi, '');
        }
        if (des != '' && des != undefined) {
            if (des.search(newNameStr) != -1) {
                if (listResult.length === 0)
                    listResult.push(data[i]);
                else if (listResult.length > 0 && !checkExistVideo(listResult, data[i])) {
                    listResult.push(data[i]);
                }
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        if (listResult.length === 0)
            listResult.push(data[i]);
        else if (listResult.length > 0 && !checkExistVideo(listResult, data[i])) {
            listResult.push(data[i]);
        }
    }
    return listResult;
}

function removeAccents(value) {
    var query = '';
    if (value) {
        var trimValue = trim(value);
        var replaceValue = trimValue.replace(/ /g, "-");
        query = replaceValue.replace(/-+-/g, "-");
    }
    return query;
}

function checkExist(array, item) {
    let result = false;
    for (var i = 0; i < array.length; i++) {
        if (array[i].id === item.id) {
            result = true;
        }
    }
    return result;
}

function checkExistFilm(array, item) {
    let result = false;
    for (var i = 0; i < array.length; i++) {

        if (array[i].filmGroup.groupName === item.filmGroup.groupName) {
            result = true;
        }
    }
    return result;
}

function checkExistVideo(array, item) {
    let result = false;
    for (var i = 0; i < array.length; i++) {
        if (array[i].id === item.id) {
            result = true;
        }
    }
    return result;
}

function getActionLogTraffic(pathname) {
    let action = "NORMAL";
    if (!getCookie("ACTION_LOGTRAFFIC")) {
        let regex = /^(.+)-t([0-9]+)/;
        if (regex.test(pathname)) {//history.location.pathname
            action = "ADVERTISEMENT";
            setCookie("ACTION_LOGTRAFFIC", "ADVERTISEMENT");
        }
    } else {
        action = "ADVERTISEMENT";
    }
    return action;
}

//VIDEO MOCHA END

export default {
    removeAccents,
    checkExistVideo,
    checkExistFilm,
    checkExist,
    formatNumber,
    formatWithCommas,
    getTimeAgo,
    getPathname,
    getIdFromPathname,
    getDate,
    getDateTime,
    replaceUrl,
    formatDate,
    format,
    remove084,
    validatePhoneStartWith16,
    validatePhoneFormat016,
    validatePhoneFormat,
    highlightWords,
    unsignString,
    unsignMaxString,
    trim,

    checkObjExist,
    checkArrNotEmpty,
    logShareFb,
    count_words,
    md5Str,
    checkInputUploadYoutube,
    checkInputUploadByUser,
    decryptData,
    encryptData,
    encryptStr,
    decryptStr,
    getUserInfo,
    checkUserLoggedIn,
    getTokenParamForUrl,
    getToken,
    setUserInfo,
    hasNumber,
    hasCharacter,
    shareAll,
    copyUrlShare,
    checkTokenExpired,
    renewToken,
    inArr,
    getAvatar,
    showphoneNumber,
    checkIsVideoDetail,
    getIdByLink,
    generateClientId,
    setClientId,
    getClientId,
    getUserAgent,
    getCookie,
    setCookie,
    removeCookie,
    checkIsChannelNotShowVideo,
    checkIsChannelNotShow,
    sortChannelSearch,
    sortFilmSearch,
    sortVideoSearch,
    getActionLogTraffic
};
