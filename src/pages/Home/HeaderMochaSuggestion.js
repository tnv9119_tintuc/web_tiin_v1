import React, {Component} from 'react';

import Link from 'next/link';
//import "../../../styles/VideoMocha/VideoItem.css";
import Helper from "../../utils/helpers/Helper";
import {DEFAULT_IMG} from "../../config/Config";
import {ImageSSR} from "../../components/Global/ImageSSR/ImageSSR";

class HeaderMochaSuggestion extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        var videos = this.props.videos;
        var channels = this.props.channels;
        var films = this.props.films;
        var query = this.props.query ? this.props.query : '';

        if (videos.length > 0 || channels.length > 0 || films.length > 0) {
            return (
                <div id="suggestions" className="suggestion">
                    <ul className="sug-ul sug-ul-video">
                        <li className="sug-title">
                            <a><span className="icon-sug"/>
                                <p><b>Video</b></p>
                            </a>
                        </li>
                        <div className="sug-ul-border">
                            {this.showSearchByType(Helper.sortVideoSearch(videos, query), 'video', query)}
                        </div>
                    </ul>


                    <ul className="sug-ul sug-ul-song">
                        <li className="sug-title">
                            <a><span className="icon-sug"/>
                                <p><b>Kênh</b></p>
                            </a>
                        </li>
                        <div className="sug-ul-border">
                            {this.showSearchByType(Helper.sortChannelSearch(channels, query), 'channel', query)}
                        </div>
                    </ul>


                    <ul className="sug-ul sug-ul-album">
                        <li className="sug-title">
                            <a><span className="icon-sug"/>
                                <p><b>Phim</b></p>
                            </a>
                        </li>

                        {this.showSearchByType(Helper.sortFilmSearch(films, query), 'film', query)}
                    </ul>

                    <div className="sugges-viewall">
                        <Link href={"/search?key=" + encodeURIComponent(query.trim())}
                              as={"/search.html?key=" + encodeURIComponent(query.trim())}
                        >
                            <a>
                                Xem tất cả kết quả tìm kiếm &gt;
                            </a>

                        </Link>
                    </div>

                </div>
            )
        } else {
            return '';
        }
    }

    showSearchByType(results, type, query) {
        switch (type) {
            // -- video --
            case 'video': {
                let videoResult = results.map((result, index) => {

                    if (index < 5) {
                        return <li className="sug-video" key={index}>
                            <Link href='/video'
                                  as={Helper.replaceUrl(result.link)+'.html'}
                            >
                                <a>
                                    <ImageSSR src={result.image_path}
                                         fallbackSrc={DEFAULT_IMG}
                                         width="65" height="38"/>
                                    <div className="div-sug">
                                        <p dangerouslySetInnerHTML={{__html: Helper.highlightWords(result.name, query)}}/>
                                        <br/>
                                        <span
                                            className="sug-video-span">{Helper.formatNumber(result.isView)} lượt xem</span>
                                    </div>
                                </a>

                            </Link>
                        </li>;
                    }
                });
                return videoResult;
            }

            // -- channel --
            case 'channel': {
                let channelResult = results.map((result, index) => {
                    if (index < 5) {
                        return <li className="sug-audio" key={index}>
                            <Link href="/channel"
                                  as={Helper.replaceUrl(result.link)+'.html'}
                            >
                                <a>
                                    <ImageSSR src={result.url_avatar}
                                              fallbackSrc={DEFAULT_IMG}
                                              width="38" height="38"/>
                                    <p dangerouslySetInnerHTML={{__html: Helper.highlightWords(result.name, query)}}/>
                                    <br/>
                                    <span
                                        className="sug-video-span">{Helper.formatNumber(result.numfollow)} người theo dõi</span>
                                </a>
                            </Link>
                        </li>;
                    }
                });
                return channelResult;
            }

            // -- film --
            case 'film': {
                let filmResult = results.map((result, index) => {
                    if (index < 5) {
                        return <li className="sug-album" key={index}>
                            <Link href="/video"
                                  as={Helper.replaceUrl(result.link)+'.html'}
                            >
                                <a>
                                    <ImageSSR src={result.filmGroup.groupImage}
                                              fallbackSrc={DEFAULT_IMG}
                                              width="38" height="38"/>
                                    <p dangerouslySetInnerHTML={{__html: Helper.highlightWords(result.filmGroup.groupName, query)}}/>

                                    <span className="fim-span-view">
                                    {Helper.formatNumber(result.isView)} lượt xem
                                </span>
                                    <span className=" fim-span-eps">
                                    {result.filmGroup.currentVideo}
                                </span>
                                </a>

                            </Link>
                        </li>;
                    }
                });
                return filmResult;
            }
            default: {
                //statements;
                break;
            }
        }


    }
}


export default HeaderMochaSuggestion;

