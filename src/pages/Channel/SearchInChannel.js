import React from 'react';
import VideoSearchInChannel from "../../components/Video/VideoDetail/VideoSearchInChannel";

class SearchInChannel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            videoHot: ''
        };
    }

    render() {
        var query = this.props.query;
        var video = this.props.videos;

        if (video.length > 0) {
            return (

                <div className="cate-videoAll box-shadow-cm" style={{marginTop: '25px'}}>
                    <div className="breadcrum-clip">
                        <p style={{fontSize: '17px'}}>Kết quả tìm kiếm</p>
                    </div>
                    <div className="box-list-video">
                        {this.showVideo(video)}
                    </div>
                </div>
            );
        } else {
            return (
                <div className="body-phim-center body-phim-center-tabSearchCn ofi">
                    <div className="body-phim-right body-phim-right-tabSearchCn">
                        <div className="bpr-video bpr-video-tabVideoCn box-shadow-cm">
                            <div className="breadcrum-clip">
                                <p style={{fontWeight: 'normal'}}>Không tìm thấy kết quả với "{query}"</p>
                            </div>
                            <div className="box-list-video">
                            </div>
                        </div>
                    </div>
                    <div className="clear"/>
                </div>
            );
        }
    }

    showVideo(videos) {
        var result = null;
        if (videos) {
            if (videos.length > 0) {
                result = videos.map((video, index) => {
                    return <VideoSearchInChannel key={index} video={video} query={this.props.query}/>
                });
            }
        }
        return result;
    }
}

export default SearchInChannel;
