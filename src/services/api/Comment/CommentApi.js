import ApiCaller from "../../../utils/ApiCaller";
import * as Config from '../../../config/Config';
import Helper from "../../../utils/helpers/Helper";
import {API_GET_COMMENT} from "../../../config/Config";


function getCommentVideo(videoId, link, row = '') {
    let endpoint = API_GET_COMMENT + '?videoId=' + videoId + '&url=' + encodeURIComponent(link)  + '&row_start=' + encodeURIComponent(row) + '&num=20' + '&token=' + Helper.getTokenParamForUrl();
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}


function getCommentReply(videoId, url, row = '') {
    let endpoint = API_GET_COMMENT + '?videoId=' + videoId + '&url=' + encodeURIComponent(url)  + '&row_start=' + encodeURIComponent(row) + '&num=20' + '&token=' + Helper.getTokenParamForUrl();
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}

function postComment(data, secinf) {
    let token = Helper.getToken();
    let timeStamp = new Date().getTime();
    let dataAppend = JSON.stringify(data);
    let dataPost = {
        secinf: secinf,
        security: Helper.encryptStr(Helper.md5Str(token + timeStamp + secinf + dataAppend)),
        timestamp: timeStamp,
        data: dataAppend,
        token: token,
        platform: 'WEB'
    };

    return ApiCaller.callApiMocha(Config.API_PUSH_ACTION, 'POST', dataPost);
}

function likeComment(data, secinf) {
    let token = Helper.getToken();
    let timeStamp = new Date().getTime();
    let dataAppend = JSON.stringify(data);
    let dataPost = {
        secinf: secinf,
        security: Helper.encryptStr(Helper.md5Str(token + timeStamp + secinf + dataAppend)),
        timestamp: timeStamp,
        data: dataAppend,
        token: token,
        platform: 'WEB'
    };

    return ApiCaller.callApiMocha(Config.API_PUSH_ACTION, 'POST', dataPost);
}


export default {
    getCommentVideo,
    getCommentReply,
    postComment,
    likeComment
};
