import ApiCaller from "../../../utils/ApiCaller";
import Helper from "../../../utils/helpers/Helper";
import * as Config from '../../../config/Config';


function createVideoByLinkYoutube(linkYoutube, categoryId) {
    let dataPost = {
        linkYoutube: linkYoutube,
        categoryId: categoryId,
        token: Helper.getToken()
    };

    return ApiCaller.callApiMocha(Config.API_CREATE_VIDEO_BY_YOUTUBE, 'POST', dataPost);
}

function createVideoUploadByUser(title, desc, path, cateId) {
    let dataPost = {
        videoTitle: title,
        videoDesc: desc,
        imagelead: 'imagelead',
        itemvideo: path,
        categoryId: cateId,
        token: Helper.getToken()
    };

    return ApiCaller.callApiMocha(Config.API_CREATE_VIDEO_BY_USER,'POST', dataPost);
}

export default {
    createVideoByLinkYoutube,
    createVideoUploadByUser
};
