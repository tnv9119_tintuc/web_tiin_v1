import ApiCaller from "../../../utils/ApiCaller";

const format = require('string-format');
import * as Config from '../../../config/Config';
import Helper from "../../../utils/helpers/Helper";
import HelperServer from "../../../utils/helpers/HelperServer";


class CategoryApi {
    static getData() {
        return ApiCaller.callApiJava('/ws/common/getCategoryList', 'GET', null);
    }

    static getCategoryUpload() {
        let endpoint = Config.API_GET_CATEGORY_UPLOAD + '?token=' + Helper.getTokenParamForUrl();
        return ApiCaller.callApiMocha(endpoint, 'GET', null);
    }

    static getVideoHot(idCate) {
        const params = {
            categoryid: idCate,
            limit: '5',
            token: Helper.getTokenParamForUrl()
        };
        const endPoint = format('?categoryid={0}&limit={1}&token={2}',
            params.categoryid, params.limit, params.token);
        const url = format('{}{}', Config.API_TOP_VIDEO_OF_CATE, endPoint);
        return ApiCaller.callApiMocha(url, 'GET', null);
    }

    static getVideoHotServer(idCate,cookie) {
        const params = {
            categoryid: idCate,
            limit: '5',
            token: HelperServer.getTokenParamForServer(cookie)
        };
        const endPoint = format('?categoryid={0}&limit={1}&token={2}',
            params.categoryid, params.limit, params.token);
        const url = format('{}{}', Config.API_TOP_VIDEO_OF_CATE, endPoint);
        return ApiCaller.callApiMocha(url, 'GET', null);
    }

    static getChannelHot(idCate) {
        const params = {
            categoryid: idCate,
            limit: '5',
            token: Helper.getTokenParamForUrl()
        };
        const endPoint = format('?categoryid={0}&limit={1}&token={2}',
            params.categoryid, params.limit, params.token);
        const url = format('{}{}', Config.API_TOP_CHANNEL_OF_CATE, endPoint);
        return ApiCaller.callApiMocha(url, 'GET', null);
    }

    static getChannelHotServer(idCate,cookie) {
        const params = {
            categoryid: idCate,
            limit: '5',
            token: HelperServer.getTokenParamForServer(cookie)
        };
        const endPoint = format('?categoryid={0}&limit={1}&token={2}',
            params.categoryid, params.limit, params.token);
        const url = format('{}{}', Config.API_TOP_CHANNEL_OF_CATE, endPoint);
        return ApiCaller.callApiMocha(url, 'GET', null);
    }

    static getFilmHot() {
        const params = {
            limit: '5',
            token: Helper.getTokenParamForUrl()
        };
        const endPoint = format('?limit={0}&token={1}',
            params.limit, params.token);
        const url = format('{}{}', Config.API_TOP_VIDEO_OF_FILM, endPoint);
        return ApiCaller.callApiMocha(url, 'GET', null);
    }

    static getFilmHotServer(cookie) {
        const params = {
            limit: '5',
            token: HelperServer.getTokenParamForServer(cookie)
        };
        const endPoint = format('?limit={0}&token={1}',
            params.limit, params.token);
        const url = format('{}{}', Config.API_TOP_VIDEO_OF_FILM, endPoint);
        return ApiCaller.callApiMocha(url, 'GET', null);
    }

    static getVideoAll(idCate, offset = 0, lastIdStr) {
        const params = {
            categoryid: idCate,
            limit: 20,
            offset: offset,
            lastIdStr: lastIdStr,
            token: Helper.getTokenParamForUrl()
        };
        const endPoint = format('?categoryid={0}&limit={1}&offset={2}&lastIdStr={3}&token={4}',
            params.categoryid, params.limit, params.offset, params.lastIdStr, params.token);
        const url = format('{}{}', Config.API_LIST_VIDEO_BY_CATE, endPoint);
        return ApiCaller.callApiMocha(url, 'GET', null);
    }
    static getVideoAllServer(idCate, offset = 0, lastIdStr,cookie) {
        const params = {
            categoryid: idCate,
            limit: 20,
            offset: offset,
            lastIdStr: lastIdStr,
            token: HelperServer.getTokenParamForServer(cookie)
        };
        const endPoint = format('?categoryid={0}&limit={1}&offset={2}&lastIdStr={3}&token={4}',
            params.categoryid, params.limit, params.offset, params.lastIdStr, params.token);
        const url = format('{}{}', Config.API_LIST_VIDEO_BY_CATE, endPoint);
        return ApiCaller.callApiMocha(url, 'GET', null);
    }


    static   getVideoAllFilm(idCate, offset = 0) {
        const params = {
            limit: 20,
            offset: offset,
            lastIdStr: '',
            token: Helper.getTokenParamForUrl()
        };
        const endPoint = format('?limit={0}&offset={1}&lastIdStr={2}&token={3}',
            params.limit, params.offset, params.lastIdStr, params.token);
        const url = format('{}{}', Config.API_LIST_FILM_GROUPS, endPoint);
        return   ApiCaller.callApiMocha(url, 'GET', null);
    }

    static   getVideoAllFilmServer( offset = 0,cookie) {
        const params = {
            limit: 20,
            offset: offset,
            lastIdStr: '',
            token: HelperServer.getTokenParamForServer(cookie)
        };
        const endPoint = format('?limit={0}&offset={1}&lastIdStr={2}&token={3}',
            params.limit, params.offset, params.lastIdStr, params.token);
        const url = format('{}{}', Config.API_LIST_FILM_GROUPS, endPoint);
        return   ApiCaller.callApiMocha(url, 'GET', null);
    }

    static getVideoBanner() {
        return ApiCaller.callApiMocha(Config.API_TOP_VIDEO_OF_HOME + '?categoryid=1001&token=' + Helper.getTokenParamForUrl(), 'GET', null);
    }

    static getVideoBannerServer(cookie) {
        return ApiCaller.callApiMocha(Config.API_TOP_VIDEO_OF_HOME + '?categoryid=1001&token=' + HelperServer.getTokenParamForServer(cookie), 'GET', null);
    }

    static getListHiddenCategory(limit, offset) {
        return ApiCaller.callApiMocha(Config.API_LIST_HIDDEN_CATEGORY + '?limit='+ limit +'&offset='+ offset +'&token=' + Helper.getTokenParamForUrl(), 'GET', null);
    }

}

export default CategoryApi;
