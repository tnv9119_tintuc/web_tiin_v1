import {API_CHANGEPASS, API_LOGIN, DECRYPT_KEY, EXPIRE_TIME, USER_INFO} from "../../config/Config";
import axios from 'axios';
import Helper from "../../utils/helpers/Helper";
import  {Router} from '../../routes';

import qs from 'qs';


function login(username, password) {
    let timeStamp = new Date().getTime();
    username = "84" + username;
    password = Helper.md5Str(password);

    var bodyFormData = new FormData();
    bodyFormData.set("username", username);
    bodyFormData.set("password", password);
    bodyFormData.set("timestamp", timeStamp);

    return axios.post(API_LOGIN,
        bodyFormData,
        {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }
    ).then(function (response) {

        // handle success
        let data = null;
        if (response.status === 200) {
            data = response.data;

            if (data) {
                if (data.code === 200) {
                    let timeExp = new Date().getTime() + 1800000;//30ph
                    let expires = new Date(timeExp);
                    Helper.setCookie(EXPIRE_TIME, timeExp, expires);

                    Helper.setUserInfo(data.dataEnc);
                } else {
                    Helper.removeCookie(USER_INFO);

                    //let error = (data && data.error.message);
                    let error;
                    if (data.code === 404) {
                        error = 'Account chưa được khởi tạo.';
                    } else if (data.code === 401) {
                        error = 'Xác thực không thành công.';
                    } else if (data.code === 505) {
                        error = 'Có lỗi xảy ra.';
                    } else {
                        error = 'Đăng nhập không thành công.';
                    }
                    return Promise.reject(error);
                }
            } else {
                let error = 'Đăng nhập không thành công.';
                return Promise.reject(error);
            }
        }
        return data;
    });
}

function logout() {
    // remove user from local storage to log user out
    Helper.removeCookie(USER_INFO);
    Helper.removeCookie(EXPIRE_TIME);

    let location = {
        pathname: '/'
    };
    Router.pushRoute('/login');
}

function changePassword(pass, newPass) {

    let userInfo = Helper.getUserInfo();
    if (userInfo) {

        let token = userInfo.token;
        let dataPost = {
            oldpassword: Helper.md5Str(pass),
            newpassword: Helper.encryptStr(newPass),
            token: token
        };

        return axios.post(API_CHANGEPASS,
            qs.stringify(dataPost),
            {
                headers: {
                    'content-type': 'application/x-www-form-urlencoded'
                }
            }
        ).then((
            {data}) => {
                Helper.checkTokenExpired(data);
                Helper.renewToken(data);

                // handle success
                if (data.code === 200) {
                    let dataDec = Helper.decryptData(data.dataEnc);
                    if (Helper.checkObjExist(dataDec, "token")) {
                        userInfo.token = dataDec.token;
                        Helper.setUserInfo(Helper.encryptData(userInfo));
                    }
                    return true;
                } else if (data.code === 421) {
                    let error = "Mật khẩu cũ không đúng.";
                    return Promise.reject(error);
                } else {
                    let error = 'Thao tác không thành công.';
                    return Promise.reject(error);
                }
            }
        );
    }

}


export default {
    login,
    logout,
    changePassword
};