import React, {Component} from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import CommentApi from "../../services/api/Comment/CommentApi";
import Helper from "../../utils/helpers/Helper";
import ModalLogin from "../Modal/ModalLogin";
import ModalPartial from "../Modal/ModalPartial";

import {DOMAIN_IMAGE_STATIC} from "../../config/Config";
const default_error = DOMAIN_IMAGE_STATIC + "default_error.png";

class Comment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            msisdn: '',
            page: 1,
            comments: [],
            listSubCommentShow: [],
            replyCommentShowId: 0,
            commentRowId: '',
            hasMoreItems: true,

            comment: '',

            isShow: false,
            isShowLogin: false
        };

        this.handleComment = this.handleComment.bind(this);
        this.toggle = this.toggle.bind(this);
        this.showHideReplyComment = this.showHideReplyComment.bind(this);
        this.handleReplyComment = this.handleReplyComment.bind(this);
        this.loadSubComment = this.loadSubComment.bind(this);
        this.handleLike = this.handleLike.bind(this);
        this.handleLikeSubComment = this.handleLikeSubComment.bind(this);
    }
    componentDidMount() {
        let userInfo = Helper.getUserInfo();
        let user = null;
        let msisdn = null;
        if (userInfo) {
            user = userInfo.channelInfo ? userInfo.channelInfo : null;
            msisdn = userInfo.username ? userInfo.username : null;
            if (msisdn && msisdn.substring(0, 2) == '84') {
                msisdn = "0" + msisdn.substring(2, msisdn.length);
            }
        }
        this.setState(({
            user: user,
            msisdn: msisdn
        }))
    }


    handleComment(e) {
        e.preventDefault();
        const {comment, comments} = this.state;
        var _self = this;

        let isLoggedIn = Helper.checkUserLoggedIn();
        if (!isLoggedIn) {
            this.setState({
                isShowLogin: true
            });
            return;
        }

        // stop here if form is invalid
        if (comment.trim().length === 0) {
            this.setState({
                isShow: true,
                messageComment: "Bạn cần nhập nội dung bình luận. Không được để trống."

            });
            return;
        } else if (comment.length < 2 || comment.length > 1000) {
            this.setState({
                isShow: true,
                messageComment: "Nội dung bình luận yêu cầu bắt buộc phải lớn hơn 2 ký tự và nhỏ hơn 1000 ký tự."

            });
            return;
        }

        let data = this.props.data;
        let dataComment = {
            "actionType": "COMMENT",
            "itemId": String(data.id),
            "url": data.link,
            "itemName": data.name,
            "imgUrl": data.image_path,
            "mediaUrl": data.original_path,
            "status": comment,

            "commentId": "",
            "channelId": "",
            "channelName": "",
            "channelAvatar": ""
        };
        if (Helper.checkArrNotEmpty(data, 'channels') && data.channels[0]) {
            dataComment.channelId = data.channels[0].id ? String(data.channels[0].id) : '';
            dataComment.channelName = data.channels[0].name ? data.channels[0].name : '';
            dataComment.channelAvatar = data.channels[0].url_avatar ? data.channels[0].url_avatar : '';
        }
        CommentApi.postComment(dataComment, data.secinf).then(
            ({data}) => {

                Helper.checkTokenExpired(data);
                Helper.renewToken(data);

                if (data.code === 200) {
                    let divNumComment = document.getElementById('ms-comment');
                    if (divNumComment && !isNaN(divNumComment.innerHTML)) {
                        let num = parseInt(divNumComment.innerHTML) + 1;
                        divNumComment.innerHTML = num;
                        divNumComment.setAttribute("title", num + ' Comments');
                    }

                    this.addComment(comment, data.data.base64RowID);
                }

            });
    }

    handleLike(id, is_like, text) {
        let userLoggedIn = Helper.checkUserLoggedIn();
        if (!userLoggedIn) {
            this.setState({
                isShowLogin: true
            });
            return;
        }

        let data = this.props.data;
        let _self = this;
        let isLike = is_like || this.state['statusLike_' + id];

        let dataComment = {
            "actionType": isLike ? "UNLIKECOMMENT" : "LIKECOMMENT",
            "itemId": String(data.id),
            "url": data.link,
            "itemName": data.name,
            "imgUrl": data.image_path,
            "mediaUrl": data.original_path,
            "status": text,

            "commentId": String(id),
            "channelId": "",
            "channelName": "",
            "channelAvatar": ""
        };
        if (Helper.checkArrNotEmpty(data, 'channels') && data.channels[0]) {
            dataComment.channelId = data.channels[0].id ? String(data.channels[0].id) : '';
            dataComment.channelName = data.channels[0].name ? data.channels[0].name : '';
            dataComment.channelAvatar = data.channels[0].url_avatar ? data.channels[0].url_avatar : '';
        }

        CommentApi.likeComment(dataComment, data.secinf).then(
            ({data}) => {
                Helper.checkTokenExpired(data);
                Helper.renewToken(data);

                if (data.code === 200) {
                    _self.setState({
                        ['statusLike_' + id]: !isLike
                    });
                }
            }).catch(error => {
            }
        );

    }

    handleLikeSubComment(id, is_like, text) {
        let userLoggedIn = Helper.checkUserLoggedIn();
        if (!userLoggedIn) {
            this.setState({
                isShowLogin: true
            });
            return;
        }

        let data = this.props.data;
        let _self = this;
        let isLike = is_like || this.state['statusLike_' + id];
        let dataComment = {
            "actionType": isLike ? "UNLIKECOMMENT" : "LIKECOMMENT",
            "itemId": String(data.id),
            "url": data.link,
            "itemName": data.name,
            "imgUrl": data.image_path,
            "mediaUrl": data.original_path,
            "status": text,

            "commentId": String(id),
            "channelId": "",
            "channelName": "",
            "channelAvatar": ""
        };
        if (Helper.checkArrNotEmpty(data, 'channels') && data.channels[0]) {
            dataComment.channelId = data.channels[0].id ? String(data.channels[0].id) : '';
            dataComment.channelName = data.channels[0].name ? data.channels[0].name : '';
            dataComment.channelAvatar = data.channels[0].url_avatar ? data.channels[0].url_avatar : '';
        }

        CommentApi.likeComment(dataComment, data.secinf).then(
            ({data}) => {
                Helper.checkTokenExpired(data);
                Helper.renewToken(data);

                if (data.code === 200) {
                    _self.setState({
                        ['statusLike_' + id]: !isLike
                    });
                }
            }).catch(error => {
            }
        );

    }

    showHideReplyComment(id) {
        this.setState({
            replyCommentShowId: (id === this.state.replyCommentShowId) ? 0 : id
        }, () => {
            let note = document.getElementById("input-cm-sub-" + id);
            if (note) {
                note.focus();
            }
        });
    }

    loadSubComment(id) {
        let _self = this;
        let videoId = this.props.data.id;
        let startRowId = this.state['startRow_' + id] ? this.state['startRow_' + id] : this.getLastCommentReply(id);
        var comments = this.state.comments;

        CommentApi.getCommentReply(videoId, id, startRowId).then(
            ({data}) => {
                Helper.renewToken(data);

                if (Helper.checkObjExist(data, 'dataEnc')) {
                    let dataDec = Helper.decryptData(data.dataEnc);

                    if (Helper.checkArrNotEmpty(dataDec, 'listComment')) {
                        let lstComment = dataDec.listComment;

                        for (var i = 0; i < comments.length; i++) {
                            if (comments[i].base64RowID == id) {
                                let lengthLstComment = lstComment.length;
                                for (var j = 0; j < lengthLstComment; j++) {
                                    comments[i].lstSubComment.push(lstComment[j]);
                                }

                                this.setState({
                                    ['startRow_' + id]: lstComment[lstComment.length - 1].base64RowID,
                                    ['numberSub_' + id]: _self.state['numberSub_' + id] ? (_self.state['numberSub_' + id] + lengthLstComment) : lengthLstComment,
                                    comments: comments
                                });
                                return;
                            }
                        }
                    }
                }
            });

    }

    getLastCommentReply(id) {
        let comments = this.state.comments;
        for (var i = 0; i < comments.length; i++) {
            if (comments[i].base64RowID === id) {
                let lstSubComment = comments[i].lstSubComment;
                if (lstSubComment && lstSubComment.length > 0) {
                    return lstSubComment[lstSubComment.length - 1].base64RowID;
                }
            }
        }
    }

    handleReplyComment(e) {
        e.preventDefault();

        let _self = this;
        let userLoggedIn = Helper.checkUserLoggedIn();
        if (!userLoggedIn) {
            this.setState({
                isShowLogin: true
            });
            return;
        }

        let commentReply = this.state['commentReply_' + this.state.replyCommentShowId];
        if (!commentReply || commentReply.trim().length === 0) {
            this.setState({
                isShow: true,
                messageComment: "Bạn cần nhập nội dung bình luận. Không được để trống."
            });
            return;
        }

        if (commentReply) {
            // stop here if form is invalid
            if (commentReply.length < 2 || commentReply.length > 1000) {
                this.setState({
                    isShow: true,
                    messageComment: "Nội dung bình luận yêu cầu bắt buộc phải lớn hơn 2 ký tự và nhỏ hơn 1000 ký tự."
                });
                return;
            }

            let dataProps = this.props.data;
            let dataComment = {
                "actionType": "COMMENT",
                "itemId": String(dataProps.id),
                "url": dataProps.link,
                "itemName": dataProps.name,
                "imgUrl": dataProps.image_path,
                "mediaUrl": dataProps.original_path,
                "status": commentReply,

                "commentId": this.state.replyCommentShowId,
                "channelId": "",
                "channelName": "",
                "channelAvatar": ""
            };
            if (Helper.checkArrNotEmpty(dataProps, 'channels') && dataProps.channels[0]) {
                dataComment.channelId = dataProps.channels[0].id ? String(dataProps.channels[0].id) : '';
                dataComment.channelName = dataProps.channels[0].name ? dataProps.channels[0].name : '';
                dataComment.channelAvatar = dataProps.channels[0].url_avatar ? dataProps.channels[0].url_avatar : '';
            }
            CommentApi.postComment(dataComment, dataProps.secinf).then(
                ({data}) => {
                    Helper.checkTokenExpired(data);
                    Helper.renewToken(data);

                    if (data.code === 200) {
                        this.addComment(commentReply, data.data.base64RowID, this.state.replyCommentShowId);
                    }
                }).catch(error => {
                }
            );
        }

    }

    toggle() {
        //this.textInput.current.focus();
        this.setState({
            isShow: false,
            isShowLogin: false
        });
    }

    addComment(text, rowId = '', toRowId = "") {
        let {user, msisdn, comments} = this.state;
        let comment = {
            base64RowID: rowId,
            stamp: new Date().getTime(),
            status: text,
            number_comment: 0,
            number_like: 0,
            is_like: 0
        };

        if (msisdn) {
            comment.userInfo = {
                msisdn: Helper.encryptStr(msisdn),
                name: Helper.showphoneNumber(msisdn)
            };
        } else {
            comment.userInfo = {
                msisdn: '',
                name: ''
            };
        }

        if (toRowId === '') {
            comments.unshift(comment);

            this.setState({
                comment: '',
                comments: comments,
            });
            let txtComment = document.getElementById("txtComment");
            if (txtComment) {
                txtComment.focus();
            }
        } else {
            //comment.base64RowID = rowId;
            for (var i = 0; i < comments.length; i++) {
                if (comments[i].base64RowID === toRowId) {
                    if (!Helper.checkArrNotEmpty(comments[i], 'lstSubComment')) {
                        comments[i].lstSubComment = [];
                    }
                    comments[i].lstSubComment.unshift(comment);
                    comments[i].number_comment = parseInt(comments[i].number_comment) + 1;

                    this.setState({
                        ['commentReply_' + toRowId]: '',
                        comments: comments
                    });

                    let note = document.getElementById("input-cm-sub-" + toRowId);
                    if (note) {
                        note.focus();
                    }
                    return;
                }
            }
            return null;
        }

    }

    loadItems() {
        let page = this.state.page;
        let data = this.props.data;
        let videoId = data.id;
        let link = data.link;
        let self = this;

        this.setState({
            hasMoreItems: false
        }, () => {
            CommentApi.getCommentVideo(videoId, link, self.state.commentRowId, page).then(
                ({data}) => {
                    Helper.renewToken(data);

                    if (Helper.checkObjExist(data, 'dataEnc')) {
                        let dataDec = Helper.decryptData(data.dataEnc);

                        if (Helper.checkArrNotEmpty(dataDec, 'listComment')) {
                            var comments = self.state.comments;
                            dataDec.listComment.map((comment) => {
                                comments.push(comment);
                            });

                            //let hasMore = (comments.length / dataDec.listComment.length) === page;
                            let hasMore = true;
                            self.setState({
                                page: page + 1,
                                commentRowId: comments.length > 0 ? comments[comments.length - 1].base64RowID : '',
                                comments: comments,
                                hasMoreItems: hasMore
                            });
                        }

                    }
                },
                (error) => {
                    this.setState({
                        error
                    });
                });
        });


    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.data !== prevProps.data) {
            this.setState({
                page: 1,
                comments: [],
                listSubCommentShow: [],
                replyCommentShowId: 0,
                commentRowId: '',
                hasMoreItems: true,
                comment: '',
                isShow: false,
                isShowLogin: false
            });
        }
    }


    render() {
        const {user, msisdn, comments, comment, isShow, isShowLogin, messageComment, replyCommentShowId} = this.state;
        var dataState = this.state;

        var items = [];
        comments.map((comment, i) => {
            let subItem = [];
            if (Helper.checkArrNotEmpty(comment, 'lstSubComment')) {

                comment.lstSubComment.map((subComment, j) => {

                    if(Helper.checkObjExist(subComment, 'userInfo') && subComment.userInfo) {
                        subItem.push(
                            <div key={'subComment_' + j}>
                                <div className="cmv-row-sub">
                                    <a className="cmv-row-avatar">
                                        <img src={Helper.getAvatar(Helper.decryptStr(subComment.userInfo.msisdn))}
                                             onError={(e) => {
                                                 e.target.onerror = null;
                                                 e.target.src = default_error
                                             }}
                                        />
                                    </a>
                                    <div className="cmv-row-right">
                                        <p className="cmv-name-like">
                                            <a className="cmv-name">
                                                <b>{subComment.userInfo.name}:</b>&nbsp;&nbsp;
                                                {subComment.status}
                                            </a>
                                        </p>
                                        <p className="cmv-time-answer">
                                            <a className={"cmv-like" + (subComment.is_like || dataState['statusLike_' + subComment.base64RowID] ? " active" : "")}
                                               onClick={() => this.handleLikeSubComment(subComment.base64RowID, subComment.is_like, subComment.status)}>
                                                {!subComment.is_like && dataState['statusLike_' + subComment.base64RowID] ?
                                                    Helper.formatNumber(subComment.number_like + 1) :
                                                    Helper.formatNumber(subComment.number_like)
                                                }
                                            </a>&nbsp;&nbsp;•&nbsp;&nbsp;
                                            <span className="cmv-time-sp">
                                            {Helper.getTimeAgo(subComment.stamp)}
                                        </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        );
                    }

                });
            }


            let showLoadMore = false;
            let numberShow = 0;
            if (comment.number_comment > 3) {
                if ((typeof dataState['numberSub_' + comment.base64RowID] === "undefined")) {
                    showLoadMore = true;
                    numberShow = comment.number_comment - 3;
                } else if (dataState['numberSub_' + comment.base64RowID]) {
                    if ((comment.number_comment - dataState['numberSub_' + comment.base64RowID]) > 3) {
                        showLoadMore = true;
                        numberShow = comment.number_comment - dataState['numberSub_' + comment.base64RowID] - 3;
                    } else {
                        showLoadMore = false;
                        numberShow = 0;
                    }
                }
            }

            if (Helper.checkObjExist(comment, 'userInfo') && comment.userInfo) {
                items.push(
                    <div key={'comment_' + i}>
                        <div className="cmv-row">
                            <a className="cmv-row-avatar">
                                <img src={Helper.getAvatar(Helper.decryptStr(comment.userInfo.msisdn))}
                                     onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = default_error
                                     }}/>
                            </a>
                            <div className="cmv-row-right">
                                <p className="cmv-name-like">
                                    <a className="cmv-name">
                                        <b>{comment.userInfo.name}:</b>&nbsp;&nbsp;&nbsp;{comment.status}
                                    </a>
                                </p>
                                <p className="cmv-time-answer">
                                    <a className={"cmv-like" + (comment.is_like || dataState['statusLike_' + comment.base64RowID] ? " active" : "")}
                                       onClick={() => this.handleLike(comment.base64RowID, comment.is_like, comment.status)}>

                                        {!comment.is_like && dataState['statusLike_' + comment.base64RowID] ?
                                            Helper.formatNumber(comment.number_like + 1) :
                                            Helper.formatNumber(comment.number_like)
                                        }
                                    </a> &nbsp;&nbsp;•&nbsp;&nbsp;
                                    <span className="cmv-time-sp">
                                    {Helper.getTimeAgo(comment.stamp)}
                                </span> &nbsp;&nbsp;•&nbsp;&nbsp;
                                    <a className="cmv-answer-lnk"
                                       onClick={() => this.showHideReplyComment(comment.base64RowID)}>
                                        Trả lời
                                    </a>
                                </p>
                            </div>
                        </div>

                        <form className="cmv-form reply" onSubmit={this.handleReplyComment}
                              style={replyCommentShowId === comment.base64RowID ? {display: 'block'} : {display: 'none'}}>
                            <a className="cmv-avatar">
                                <img src={msisdn ? Helper.getAvatar(msisdn) : default_error}
                                     onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = default_error
                                     }}/>
                            </a>
                            <p className="cmv-form-input">
                            <textarea className="input-cm-sub"
                                      id={"input-cm-sub-" + comment.base64RowID}
                                      placeholder="Nhập nội dung bình luận"
                                      maxLength={1000} value={dataState['commentReply_' + comment.base64RowID]}
                                      onChange={e => this.setState({['commentReply_' + comment.base64RowID]: e.target.value})}
                            />
                            </p>
                            <button className="cmv-form-send"/>
                        </form>

                        {subItem}

                        {showLoadMore ?
                            <a className="cm-sub-loadmore" onClick={() => this.loadSubComment(comment.base64RowID)}>
                                Xem thêm {numberShow} câu trả lời
                            </a> : ''
                        }
                    </div>
                );
            }

        });


        return (

            <div id="comment_mocha" className="comment-mocha-video line-cm">

                <h3 className="cmv-h3">BÌNH LUẬN</h3>
                <div className="cmv-wrap" id="box-comment">
                    <form className="cmv-form" onSubmit={this.handleComment}>
                        <a className="cmv-avatar">
                            <img src={msisdn ? Helper.getAvatar(msisdn) : default_error}
                                 onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = default_error
                                 }}
                            />
                        </a>
                        <p className="cmv-form-input">
                            <textarea id="txtComment" className="input-cm"
                                      placeholder="Nhập nội dung bình luận" maxLength={1000}
                                      value={comment}
                                      onChange={e => this.setState({comment: e.target.value})}
                            />
                        </p>
                        <button className="cmv-form-send" id="btSubmit"/>
                    </form>

                    <div id="lstComment">
                        <InfiniteScroll
                            pageStart={1}
                            loadMore={this.loadItems.bind(this)}
                            hasMore={this.state.hasMoreItems}>
                            {items}
                        </InfiniteScroll>
                    </div>


                </div>

                <ModalLogin isOpen={isShowLogin}
                            body={'Quý khách chưa đăng nhập! Để hoàn thành thao tác, mời quý khách bấm OK để đăng nhập.'}
                            toggle={this.toggle}/>

                <ModalPartial isOpen={isShow}
                              body={messageComment}
                              toggle={this.toggle}/>

            </div>

        );

    }

}

export default Comment;
