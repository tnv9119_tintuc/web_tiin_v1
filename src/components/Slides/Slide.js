import React, {Component} from "react";
import Slider from "react-slick";
//import {Link} from '../../routes';
import Link from 'next/link';
import Helper from '../../utils/helpers/Helper';
import {DEFAULT_IMG} from "../../config/Config";

var dragging = false;
export default class Slide extends Component {
    constructor(props) {
        super(props);
        dragging = false;
    }

    componentWillUnmount() {
        dragging = false;
    }

    render() {
        var slides = this.props.slides;
        if (slides && slides.length > 0) {

            const PrevButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <div></div>
            );

            const NextButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <div></div>
            );

            const settings = {
                customPaging: function (i) {
                    return (
                        <div className=''>
                            <img style={{height: '76px', width: '217px'}}
                                 src={slides[i].image_path ? slides[i].image_path : DEFAULT_IMG}
                                 onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = DEFAULT_IMG
                                 }}
                            />
                        </div>
                    );
                },
                dots: true,
                dotsClass: "slick-dots slick-thumb",
                infinite: true,
                speed: 500,
                autoplay: true,
                autoplaySpeed: 5000,
                pauseOnHover: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                adaptiveHeight: true,
                prevArrow: <PrevButton/>,
                nextArrow: <NextButton/>,
                beforeChange: () => dragging = true,
                afterChange: () => dragging = false
            };

            return (
                <div className="slide_header">
                    <Slider {...settings}>
                        {
                            slides.map((slide, index) => {
                                if (slide && index < 5)
                                    return (
                                        <div key={index}>
                                            <Link href='/video'
                                                  as={slide.link ? Helper.replaceUrl(slide.link) + ".html?src=home" : '/'}

                                            >
                                                <a alt={slide.name}
                                                   title={slide.name}
                                                   onClick={(e) => {
                                                       dragging && e.preventDefault()
                                                   }}>
                                                    <img style={{width: '100%'}} src={slide.image_path ? slide.image_path : DEFAULT_IMG}
                                                         onError={(e) => {
                                                             e.target.onerror = null;
                                                             e.target.src = DEFAULT_IMG
                                                         }}
                                                    />
                                                </a>

                                            </Link>
                                        </div>
                                    );
                            })
                        }
                    </Slider>
                    <div className="box-div"/>
                </div>
            )
        } else {
            return '';
        }
    }
}
