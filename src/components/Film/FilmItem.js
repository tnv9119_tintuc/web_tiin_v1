import React from 'react';
import Helper from "../../utils/helpers/Helper";
//import {Link} from "../../routes";
import Link from 'next/link';
import {DEFAULT_IMG} from "../../config/Config";

class FilmItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const video = this.props.video;
        var boxFilmHome = this.props.type;

        if (video) {
            return (
                <div className="av-top-small swiper-slide swiper-slide">
                    <Link href={Helper.replaceUrl(video.link)}
                          as={Helper.replaceUrl(video.link)+'.html'}
                    >
                        <a className="ats-lnk-img" title={video.filmGroup.groupName}>
                            <img alt={video.filmGroup.groupName} title={video.filmGroup.groupName} className="ats-img"
                                 src={video.filmGroup.groupImage}
                                 onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = DEFAULT_IMG
                                 }}
                                 style={{width: boxFilmHome && boxFilmHome === 'boxFilmHome' ? '222px' : '133px'}}
                            />
                            {video.filmGroup.currentVideo ?
                                <span className="sp-number-film subSp">
                                            {video.filmGroup.currentVideo}</span>
                                : ''
                            }
                        </a>

                    </Link>
                    <div className="ats-info">
                        <h3 className="ats-info-h3">
                            <Link href='/' >
                                <a className="ats-info-lnk"
                                   title={video.filmGroup.groupName}>
                                    {video.filmGroup.groupName}
                                    <span className="sp-chanel-follow">
                                                {Helper.formatNumber(video.isView)} lượt xem</span>
                                </a>

                            </Link>
                        </h3>
                    </div>
                </div>
            );
        } else {
            return null;
        }

    }
}

export default FilmItem;
