import React from 'react';
import ReactDOM from "react-dom";
import VideoItem from './VideoItem';
import InfiniteScroll from 'react-infinite-scroller';
import Helper from "../../../utils/helpers/Helper";
import CateApi from "../../../services/api/Category/CategoryApi";
import ChannelApi from "../../../services/api/Channel/ChannelApi";
import ErrorBoundary from "../../ErrorBoundary";
import VideoItemFilm from "../../Video/partials/VideoItemFilm";
import LoadingScreen from "../../Global/Loading/LoadingScreen";

 class VideoBoxAll extends React.Component {
    constructor(props) {
        super(props);

        this._nodes = new Map();
        this.state = {
            isFirstOnTop: false,

            page: 0,
            refreshing: false,
            hasMoreItems: true,
            items: [],
            isLoaded: false,
            pageSize: 20,
            lastIdStr: '',
            pageMore: 1

        };

        this.loadItems = this.loadItems.bind(this);
        this.handleScroll = this.handleScroll.bind(this);

        if (!this.props.isChannel) {
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);

        if (window.pageYOffset === 0 && !this.state.isFirstOnTop) {
            this.setState({
                isFirstOnTop: true
            });
        }
        this.loadItems();
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll() {
        if (window.pageYOffset === 0 && !this.state.isFirstOnTop) {
            this.setState({
                isFirstOnTop: true
            });
        }
    }

    loadItems() {
        if (this.state.refreshing) {
            return;
        }

        const {pageSize, page, lastIdStr, items} = this.state;
        const {isCate, idSearch, isChannel, isMyChannel, isFilm} = this.props;
        this.setState({refreshing: true}, () => {
            if (isCate == 1) {
                let pageMore = this.state.pageMore;
                CateApi.getVideoAll(idSearch, page, lastIdStr).then(
                    (result) => {
                        Helper.renewToken(result.data);

                        let len = items.length;
                        if (Helper.checkArrNotEmpty(result, 'data.data.listVideo')) {
                            let res = result.data.data.listVideo;
                            let hasMore = res.length == 20;
                            // let hasMore = (len / res.length * pageMore === (pageMore - 1));
                            this.setState({
                                    lastIdStr: result.data.data.lastIdStr,
                                    pageMore: pageMore + 1,
                                    page: page + pageSize,
                                    isLoaded: true,
                                    hasMoreItems: hasMore,
                                    items: items.concat(res)
                                }, () => {
                                    this.scrollToTop(len);
                                }
                            );
                        } else {
                            this.setState({
                                hasMoreItems: false
                            });
                        }
                    }
                );
            } else if (isChannel == 1) {
                if (isMyChannel == 1) {
                    ChannelApi.getVideoChannel(idSearch, page, lastIdStr).then(
                        (result) => {
                            Helper.renewToken(result.data);

                            let len = items.length;
                            if (Helper.checkArrNotEmpty(result, 'data.data.listVideo')) {
                                let res = result.data.data.listVideo;
                                let hasMore = res.length === 20;
                                this.setState({
                                        lastIdStr: result.data.data.lastIdStr,
                                        page: page + pageSize,
                                        isLoaded: true,
                                        hasMoreItems: hasMore,
                                        items: items.concat(res)
                                    }, () => {
                                        this.scrollToTop(len);
                                    }
                                );
                            } else {
                                this.setState({
                                    isLoaded: true,
                                    hasMoreItems: false
                                });
                            }
                        }
                    );
                } else {
                    ChannelApi.getVideoChannel(idSearch, page, lastIdStr).then(
                        (result) => {
                            Helper.renewToken(result.data);

                            let len = items.length;
                            if (Helper.checkArrNotEmpty(result, 'data.data.listVideo')) {
                                let res = result.data.data.listVideo;
                                let hasMore = res.length === 20;
                                this.setState({
                                        lastIdStr: result.data.data.lastIdStr,
                                        page: page + pageSize,
                                        isLoaded: true,
                                        hasMoreItems: hasMore,
                                        items: items.concat(res)
                                    }, () => {
                                        this.scrollToTop(len);
                                    }
                                );
                            } else {
                                this.setState({
                                    isLoaded: true,
                                    hasMoreItems: false
                                });
                            }
                        }
                    );
                }

            } else if (isFilm == 1) {
                CateApi.getVideoAllFilm(idSearch, page).then(
                    (result) => {
                        Helper.renewToken(result.data);

                        let len = items.length;
                        if (Helper.checkArrNotEmpty(result, 'data.data.listVideo')) {
                            let res = result.data.data.listVideo;
                            let hasMore = res.length === 20;
                            this.setState({
                                    page: page + pageSize,
                                    isLoaded: true,
                                    hasMoreItems: hasMore,
                                    items: items.concat(res)
                                }, () => {
                                    this.scrollToTop(len);
                                }
                            );
                        } else {
                            this.setState({
                                hasMoreItems: false
                            });
                        }
                    }
                );
            }
        });
    }

    scrollToTop(length) {

        var _self = this;
        if (length === 0) {
            this.setState({
                refreshing: false
            });
        } else {
            const node = this._nodes.get(length - 1);
            if (node) {
                ReactDOM.findDOMNode(node).scrollIntoView({block: 'end', behavior: 'smooth'});

                setTimeout(function () {
                    _self.setState({
                        refreshing: false
                    });
                }, 500);
            }
        }
    }

    showPage() {
        if (this.props.isCate == 1) {
            return (
                <div className="cate-videoAll box-shadow-cm">
                    <h3 className="bpr-video-h3">{this.props.title}</h3>
                    <InfiniteScroll className="bpr-video-wrap bpr-video-wrap-tabVideoCn"
                                    pageStart={0}
                                    loadMore={this.loadItems}
                                    hasMore={this.state.hasMoreItems}
                    >
                        {this.showVideos(this.state.items)}
                    </InfiniteScroll>
                </div>
            );
        } else if (this.props.isFilm == 1) {
            return (
                <div id="lstSubPhim">
                    <div className="body-phim-center">
                        <div className="body-phim-right">
                            <div className="bpr-new box-shadow-cm">
                                <h3 className="bpr-new-h3">{this.props.title}</h3>
                                <InfiniteScroll className="bpr-new-wrap"
                                                pageStart={0}
                                                loadMore={this.loadItems}
                                                hasMore={this.state.hasMoreItems}
                                >
                                    {this.showVideos(this.state.items)}
                                </InfiniteScroll>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else if (this.props.isChannel == 1) {
            return (
                <div>
                    <div className="body-phim-center body-phim-center-tabVideoCn">
                        <div className="body-phim-right body-phim-right-tabVideoCn">
                            <div className="bpr-video bpr-video-tabVideoCn allVideo box-shadow-cm">
                                <h3 className="bpr-video-h3">
                                    TẤT CẢ VIDEO
                                </h3>
                                <InfiniteScroll className="bpr-video-wrap bpr-video-wrap-tabVideoCn"
                                                pageStart={0}
                                                loadMore={this.loadItems}
                                                hasMore={this.state.hasMoreItems}
                                >
                                    {this.showVideos(this.state.items)}
                                </InfiniteScroll>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }

    render() {
        const {isFirstOnTop, isLoaded} = this.state;
        if (!this.props.isChannel && (!isFirstOnTop || !isLoaded)) {
            return <LoadingScreen/>;
        } else {
            return (
                <div>
                    {this.showPage()}
                </div>
            );
        }
    }

    showVideos(videos) {
        var result = <div></div>;
        if (videos && videos.length > 0) {
            videos = Helper.checkIsChannelNotShowVideo(videos);
            result = videos.map((video, index) => {
                if (this.props.isFilm == 1) {
                    return <ErrorBoundary key={index}>
                        <VideoItemFilm video={video} ref={(element) => this._nodes.set(index, element)}/>
                    </ErrorBoundary>
                } else if (this.props.isCate == 1) {
                    return <ErrorBoundary key={index}>
                        <VideoItem video={video} ref={(element) => this._nodes.set(index, element)}/>
                    </ErrorBoundary>
                } else if (this.props.isChannel == 1) {
                    return <ErrorBoundary key={index}>
                        <VideoItem video={video} isVideoOfChannel={true}
                                   ref={(element) => this._nodes.set(index, element)}/>
                    </ErrorBoundary>
                }
            });
        }

        return result;
    }
}

export  default  VideoBoxAll;