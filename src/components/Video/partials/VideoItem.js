import React from 'react';
//import {Link} from '../../../routes';
import Link from 'next/link';
import Helper from "../../../utils/helpers/Helper";
import {DEFAULT_IMG} from "../../../config/Config";
import {withRouter} from 'next/router';

class VideoItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pathname: this.props.router.asPath
        };
    }

    genInfoChannel(video, isTabFilmOfChannel, isVideoOfChannel) {
        if (!isTabFilmOfChannel && Helper.checkArrNotEmpty(video, 'channels') && video.channels[0]) {
            return (
                <div className="playnow-home-chanel-new">
                    {isVideoOfChannel ? '' :
                        <Link href="/channel"
                              as={Helper.replaceUrl(video.channels[0].link) + '.html'}
                        >
                            <a className="playnow-chanel-link-new"
                               draggable={false}>
                                <img alt={video.channels[0].name}
                                     src={video.channels[0].url_avatar ? video.channels[0].url_avatar : DEFAULT_IMG}
                                     onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = DEFAULT_IMG
                                     }}
                                     style={{display: 'inline'}}/>
                            </a>

                        </Link>
                    }
                    <div className="play-chanel-info-new">
                        <p className="play-chanel-view-new">
                            {Helper.formatNumber(video.isView)} lượt xem • {Helper.getTimeAgo(video.DatePublish)}
                        </p>
                        {isVideoOfChannel ? '' :
                            <h3 className="play-channel-h3-new">
                                <Link href="/channel"
                                      as={Helper.replaceUrl(video.channels[0].link) + '.html'}
                                >
                                    <a>
                                        {video.channels[0].name}
                                    </a>

                                </Link>
                            </h3>
                        }
                    </div>
                    <a className="chanel-dot-more"/>
                </div>
            );
        }
        return null;
    }

    genNameVideo(video, isTabFilmOfChannel, isVideoOfChannel) {
        if (!isTabFilmOfChannel) {
            return (
                <h3 className="playnow-home-h3-new" title={video.name}>
                    {video.name}
                </h3>
            );
        }
        return null;
    }

    render() {
        const {pathname} = this.state;
        const {video, isVideoHotOfCate, isVideoOfChannel, isTabFilmOfChannel} = this.props;

        if (video) {

            let src = '';

            if (isVideoOfChannel && Helper.checkArrNotEmpty(video, 'channels')) {
                src = "?src=cn" + video.channels[0].id;
            } else if (pathname === '/') {
                src = "?src=home";
            } else if (pathname.includes('-cg1001')) {
                src = "?src=hot";
            } else if (pathname.includes('-cg7')) {
                src = "?src=sieuhai";
            }

            return (
                <div className={isVideoHotOfCate ? "" : "av-top-small swiper-slide"}>
                    <div className="playnow-home-item-new">
                        <Link href="/video"
                              as={Helper.replaceUrl(video.link) + '.html' + src}
                        >
                            <a className="playnow-home-link-new"
                               draggable={false}>
                                <img className="animated lazy"
                                     alt={video.name}
                                     title={video.name}
                                     data-original={video.image_path}
                                     src={video.image_path ? video.image_path : DEFAULT_IMG}
                                     onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = DEFAULT_IMG
                                     }}
                                     style={{display: 'block'}}
                                     draggable={false}
                                />
                                {isTabFilmOfChannel ?
                                    <span className="sp-number-film sp-number-filmGrp">{video.epsNum}</span>
                                    : ""
                                }
                                <span className="icon-play-video"/>

                                {this.genNameVideo(video, isTabFilmOfChannel, isVideoOfChannel)}
                            </a>

                        </Link>

                        {this.genInfoChannel(video, isTabFilmOfChannel, isVideoOfChannel)}
                    </div>
                </div>

            );
        } else {
            return <div>Loading...</div>;
        }

    }
}

export default withRouter(VideoItem);
