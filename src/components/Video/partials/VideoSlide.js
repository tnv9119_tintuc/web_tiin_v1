import React from "react";
import Slider from "react-slick";

//import {Link} from "../../../routes";
import Link from 'next/link';
import Helper from "../../../utils/helpers/Helper";
import {DEFAULT_IMG} from "../../../config/Config";

var dragging = false;

class VideoSlide extends React.Component {
    constructor(props) {
        super(props);
        var numberItem = this.props.numberItem;
        var numberSlide = numberItem ? numberItem : 4;
        this.state = {
            numberSlide: numberSlide
        };
    }

    render() {
        var {videos, type} = this.props;
        var {numberSlide} = this.state;

        if (videos && videos.length > 0) {
            let setLength = videos.length;
            if (videos.length >= 10) {
                setLength = 10;
            }

            const PrevButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-back"
                        aria-hidden="true"
                        aria-disabled={currentSlide === 0}
                        type="button"
                        style={{
                            display: currentSlide === 0 ? 'none' : 'block',
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            const NextButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-next"
                        aria-hidden="true"
                        aria-disabled={currentSlide >= setLength - this.state.numberSlide}
                        type="button"
                        style={{
                            display: (currentSlide >= setLength - this.state.numberSlide) ? 'none' : 'block',
                            right: "-15px",
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );


            //swipeToSlide
            const settings = {
                className: "center",
                arrows: true,
                infinite: false,
                centerPadding: "50px",
                slidesToShow: numberSlide,
                swipeToSlide: true,
                prevArrow: <PrevButton/>,
                nextArrow: <NextButton/>,
                beforeChange: () => dragging = true,
                afterChange: () => dragging = false
            };

            return (
                <div className="owl-slider-av">
                    <Slider {...settings}>
                        {this.showVideos(videos, type)}
                    </Slider>
                </div>
            );
        } else {
            return null;
        }
    }

    showVideos(videos, type) {
        return videos.map((video, index) => {
            if (video && index < videos.length && index < 10) {

                return <div className="av-top-small swiper-slide" key={index}>
                    <div className="playnow-home-item-new">
                        <Link href ="/video"
                              as ={Helper.replaceUrl(video.link) + ".html?src=suggest"}
                        >
                            <a className="playnow-home-link-new"
                               onClick={(e) => {
                                   dragging && e.preventDefault();
                               }}
                               draggable={false}>
                                <img className="animated lazy"
                                     alt={video.name}
                                     title={video.name}
                                     data-original={video.image_path}
                                     src={video.image_path ? video.image_path : DEFAULT_IMG}
                                     onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = DEFAULT_IMG
                                     }}
                                     style={{display: 'block'}}/>

                                <span className="icon-play-video"/>
                                <h3 className="playnow-home-h3-new" title={video.name}>
                                    {video.name}
                                </h3>
                            </a>

                        </Link>

                        {/*{this.genInfoChannel(video, isTabFilmOfChannel, isVideoOfChannel)}*/}

                        {(function () {
                            if (Helper.checkArrNotEmpty(video, "channels") && video.channels[0]) {
                                switch (type) {
                                    case 'full':
                                        return <div className="playnow-home-chanel-new">

                                            <Link href="/channel"
                                                  as = {encodeURIComponent(video.channels[0].name) + '-cn' + video.channels[0].id + '.html'}
                                            >
                                                <a className="playnow-chanel-link-new"
                                                   onClick={(e) => {
                                                       dragging && e.preventDefault();
                                                   }}
                                                   draggable={false}>
                                                    <img alt={video.channels[0].name} className="lazy"
                                                         src={video.channels[0].url_avatar ? video.channels[0].url_avatar : DEFAULT_IMG}
                                                         onError={(e) => {
                                                             e.target.onerror = null;
                                                             e.target.src = DEFAULT_IMG
                                                         }}
                                                         style={{display: 'inline'}}/>
                                                </a>

                                            </Link>

                                            <div className="play-chanel-info-new">
                                                <p className="play-chanel-view-new">
                                                    {Helper.formatNumber(video.isView)} lượt xem
                                                    • {Helper.getTimeAgo(video.DatePublish)}
                                                </p>

                                                <h3 className="play-channel-h3-new">
                                                    <Link href="/channel"
                                                          as={encodeURIComponent(video.channels[0].name) + '-cn' + video.channels[0].id + '.html'}
                                                    >
                                                        <a  onClick={(e) => {
                                                            dragging && e.preventDefault();
                                                        }}
                                                            draggable={false}>
                                                            {video.channels[0].name}
                                                        </a>

                                                    </Link>
                                                </h3>

                                            </div>
                                            <a className="chanel-dot-more"/>
                                        </div>;

                                    case 'full_no_chanel':
                                        return <div className="playnow-home-chanel-new">
                                            <div className="play-chanel-info-new">
                                                <p className="play-chanel-view-new">
                                                    {Helper.formatNumber(video.isView)} lượt xem
                                                    • {Helper.getTimeAgo(video.DatePublish)}
                                                </p>
                                            </div>
                                            <a className="chanel-dot-more"/>
                                        </div>;

                                    case 'collapse':
                                        return null;
                                    default:
                                        return null;
                                }
                            }
                        })()}

                    </div>
                </div>;
            }
        });
    }
}

export default VideoSlide;