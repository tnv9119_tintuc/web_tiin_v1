import React from 'react';
import VideoItem from './VideoItem';
import {Tabs, Tab, TabPanel, TabList} from "react-tabs";
//import {Link} from '../../../routes';
import Link from 'next/link';
import CateApi from "../../../services/api/Category/CategoryApi";
import Helper from "../../../utils/helpers/Helper";

class VideoBox4Video extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            linkto: '/the-thao-cg3',
            selectedIndex: 0,
            videoNews: [],
            lastIdStr: ''
        };
    }

    componentDidMount() {
        if (this.props.others) {
            CateApi.getVideoAll(3, 0, this.state.lastIdStr).then(
                ({data}) => {
                    Helper.renewToken(data);

                    this.setState({
                        videoNews: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                    });
                }
            );
        }
    }

    handleSelect = (index) => {
        let linkto = '';
        let cateId = 0;
        switch (index) {
            case 0:
                cateId = 3;
                linkto = '/the-thao-cg3';
                break;
            case 1:
                cateId = 1010;
                linkto = '/song-cg1010';
                break;
            case 2:
                cateId = 2;
                linkto = '/sao-cg2';
                break;
            case 3:
                cateId = 1020;
                linkto = '/dep-cg1020';
                break;
            default:
                cateId = 0;

                return cateId;
        }
        if (cateId > 0) {
            CateApi.getVideoAll(cateId, 0, this.state.lastIdStr).then(
                ({data}) => {
                    Helper.renewToken(data);

                    this.setState({
                        videoNews: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : [],
                        selectedIndex: index,
                        linkto: linkto
                    });
                }
            );
        }
    }

    render() {
        var {videos, title, others, path} = this.props;

        const {videoNews, linkto} = this.state;

        if (others) {
            return (
                <div className="mocha-video-hot mocha-video-other  mocha-video-box">
                    <div className="wrap-bpk">
                        <h3 className="phim-keeng-h3">
                            <Link href="/more" as={path + '.html'}>
                                {title}
                            </Link>
                        </h3>
                        <Tabs
                            selectedIndex={this.selectedIndex}
                            onSelect={this.handleSelect}
                        >
                            <div className="mocha-video-tab">
                                <TabList>
                                    <Tab tabfor="one">
                                        <a>Thể thao</a>
                                    </Tab>
                                    <Tab tabfor="two">
                                        <a>Sống</a>
                                    </Tab>
                                    <Tab tabfor="three">
                                        <a>Sao</a>
                                    </Tab>
                                    <Tab tabfor="four">
                                        <a>Đẹp</a>
                                    </Tab>
                                </TabList>
                            </div>

                            <div className="wrap-playnow-home">
                                <TabPanel>
                                    {this.showVideos(videoNews)}
                                </TabPanel>
                            </div>
                            <TabPanel>
                                <div className="wrap-playnow-home">
                                    {this.showVideos(videoNews)}
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <div className="wrap-playnow-home">
                                    {this.showVideos(videoNews)}
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <div className="wrap-playnow-home">
                                    {this.showVideos(videoNews)}
                                </div>
                            </TabPanel>
                        </Tabs>
                        <Link href="/category" as={linkto + '.html'}>
                            <a className="p-more-video">
                                Xem tất cả
                            </a>

                        </Link>
                    </div>
                </div>

            );
        } else {
            return (
                <div className="mocha-video-hot mocha-video-box">
                    <div className="wrap-bpk">
                        <h3 className="phim-keeng-h3">
                            <Link href="/category" as={path + '.html'}>
                                <a>
                                    {title}
                                </a>

                            </Link>
                        </h3>
                        <div className="wrap-playnow-home">
                            {this.showVideos(videos)}
                        </div>
                    </div>
                </div>
            );
        }
    }

    showVideos(videos) {
        var result = '';
        
        if (videos) {
            videos = Helper.checkIsChannelNotShowVideo(videos);
            if (videos.length > 0) {
                let idx = 0;
                result = videos.map((video, index) => {
                    if (this.props.type) {
                        if (!Helper.inArr(this.props.array, video.id)) {
                            if (idx < 8) {
                                idx = idx + 1;
                                return <VideoItem key={index} video={video}/>
                            }
                        }
                    } else {
                        if (index < 8) {
                            return <VideoItem key={index} video={video}/>
                        }
                    }
                });
            }
        }
        return result;
    }
}

export default VideoBox4Video;