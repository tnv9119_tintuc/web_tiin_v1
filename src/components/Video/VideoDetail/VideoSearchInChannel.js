import React from 'react';
//import {Link} from '../../../routes';
import Link from "next/link";
import Helper from "../../../utils/helpers/Helper";
import {ImageSSR} from "../../Global/ImageSSR/ImageSSR";
import {DEFAULT_IMG} from "../../../config/Config";
 class VideoSearchInChannel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoaded: true
        };
    }

    render() {
        const {error, isLoaded} = this.state;
        var video = this.props.video;
        var query = this.props.query;
        if (error) {
            return 'Error..';
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="chanel-video-small">
                    <Link  href="/video"
                           as={Helper.replaceUrl(video.link)+'.html'}
                    >
                        <a className="cvs-lnk-img">
                            <ImageSSR title="" className="lazy img-responsive vi-img"
                                 src={video.image_path}
                                fallbackSrc={DEFAULT_IMG}
                            />
                        </a>
                    </Link>
                    <div className="cvs-info">
                        <h3 className="cvs-info-h2">
                            <Link href="/video"
                                  as={Helper.replaceUrl(video.link)}
                            >
                                <a title="Nghề bắt cào cào | Ân Long An">
                                    {video.name}
                                </a>
                            </Link>
                        </h3>
                        <p className="play-chanel-view-new">
                            {video.isView} lượt xem
                        </p>
                    </div>
                </div>
            );
        }

    }

}

export  default  VideoSearchInChannel;
