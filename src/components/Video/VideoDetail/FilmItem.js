import React from 'react';
//import {Link} from "../../../routes";
import Link from 'next/link';
import Helper from "../../../utils/helpers/Helper";
import {DEFAULT_IMG} from "../../../config/Config";

class FilmItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let video = this.props.video;

        if (video) {
            return (

                <div className="av-top-small-dV swiper-slide swiper-slide">
                    <Link href="/video"
                          as={Helper.replaceUrl(video.link)+'.html'}
                    >
                        <a className="ats-lnk-img" title={video.filmGroup.groupName}>
                            <img alt={video.filmGroup.groupName}
                                 title={video.filmGroup.groupName} className="ats-img"
                                 src={video.filmGroup.groupImage ? video.filmGroup.groupImage : DEFAULT_IMG}
                                 onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = DEFAULT_IMG
                                 }}/>
                            {video.filmGroup.currentVideo ?
                                <span className="sp-number-film">{video.filmGroup.currentVideo}</span>
                                : ''
                            }
                        </a>

                    </Link>
                    <div className="ats-info">
                        <h3 className="ats-info-h3-dV">
                            <Link href="/video"
                                  as ={Helper.replaceUrl(video.link)+'.html'}
                            >
                                <a className="ats-info-lnk">
                                    {video.filmGroup.groupName}
                                    <span
                                        className="sp-chanel-follow">{Helper.formatNumber(video.isView)} lượt xem</span>
                                </a>

                            </Link>
                        </h3>
                    </div>
                </div>

            );
        } else {
            return <div>Loading...</div>;
        }

    }
}

export default FilmItem;
