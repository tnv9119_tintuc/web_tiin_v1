import React, {Component} from 'react';
import FilmSlide from "../partials/FilmSlide";

 class FilmRelated extends React. Component {
    constructor(props) {
        super(props);
    }

    render() {

        let filmsRelated = this.props.films;

        if (filmsRelated) {

            return (
                <div>

                    <div className="playnow-auto line-eps">
                        <b>PHIM ĐỀ XUẤT</b>
                    </div>
                    <div className="Goupsfilm-related">
                    <FilmSlide film={filmsRelated} numberItem={5}/>
                    </div>
                </div>
            );
        } else {
            return null;
        }

    }

}
export  default  FilmRelated;
