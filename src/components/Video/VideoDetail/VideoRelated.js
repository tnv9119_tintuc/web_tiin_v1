import React, {Component} from 'react';
import VideoSlide from "../partials/VideoSlide";


 class VideoRelated extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let videoRelated = this.props.videos;

        if (videoRelated.length > 0) {
            return <VideoSlide videos={videoRelated} type="full" numberItem={4}/>;
        } else {
            return null;
        }

    }

}
export  default  VideoRelated;
