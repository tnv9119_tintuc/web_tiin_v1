import React from 'react';
import {DEFAULT_AVATAR, DEFAULT_IMG} from "../../../config/Config";
//import {Link} from '../../../routes';
import Link from 'next/link';
import Helper from "../../../utils/helpers/Helper";
import {ImageSSR} from "../../Global/ImageSSR/ImageSSR";

class CateChannelHot extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var video = this.props.video;
        var type = this.props.type;
        if (video) {
            return (
                <div className="bpl-trailer bpl-trailer-box">
                    <h3 className="bpl-trailer-h3">
                        {type === 'trailer' ? "TRAILER PHIM" : "MỌT PHIM"}
                        <Link href="/channel"
                              as={type === 'trailer' ? "/trailer-phim-cn1476.html" : "/mot-phim-cn1478.html"}
                        >
                            <a id="phim1_All">
                                Xem tất cả
                            </a>

                        </Link>
                    </h3>
                    <div className="av-top-small swiper-slide">
                        {this.show(video)}
                    </div>
                </div>
            );
        }
    }

    show(videos) {
        var type = this.props.type;
        var result = null;
        if (videos) {
            if (videos.length > 0) {
                result = videos.map((video, index) => {
                    if (index < 2) {
                        return (
                            <div className="playnow-home-item-new" key={index}>
                                <Link href="/video"
                                      as={Helper.replaceUrl(video.link) + ".html?src=phim"}
                                >
                                    <a className="playnow-home-link-new">
                                        <ImageSSR className="animated"
                                             src={video.image_path ? video.image_path : DEFAULT_IMG}
                                             fallbackSrc={DEFAULT_IMG}
                                        />
                                        <span className="icon-play-video"/>
                                        <h3 className="playnow-home-h3-new">
                                            {video.name}
                                        </h3>
                                    </a>

                                </Link>


                                {(Helper.checkArrNotEmpty(video, 'channels') && video.channels[0]) ?
                                    <div className="playnow-home-chanel-new">
                                        <Link  href="/channel"
                                               as={Helper.replaceUrl(video.channels[0].link) +'.html'}
                                        >
                                            <a title={video.channels[0].name} className="playnow-chanel-link-new">
                                                <ImageSSR alt={video.channels[0].name}
                                                          src={video.channels[0].url_avatar ? video.channels[0].url_avatar : DEFAULT_AVATAR}
                                                          fallbackSrc={DEFAULT_AVATAR}
                                                />
                                            </a>

                                        </Link>
                                        <div className="play-chanel-info-new">
                                            <p className="play-chanel-view-new">
                                                {Helper.formatNumber(video.isView)}
                                                lượt xem •
                                                {video.created_at ? Helper.getTimeAgo(video.created_at) : ''}
                                            </p>
                                            <h3 className="play-channel-h3-new">
                                                <Link  href="/channel"
                                                       as={Helper.replaceUrl(video.channels[0].link)+'.html'}
                                                >
                                                    <a title={video.channels[0].name}>
                                                        {video.channels[0].name}
                                                    </a>

                                                </Link>
                                            </h3>
                                        </div>
                                        <a className="chanel-dot-more"/>
                                    </div>
                                    : ''
                                }

                            </div>
                        );
                    }
                });
            }
        }
        return result;
    }
}

export  default CateChannelHot;
