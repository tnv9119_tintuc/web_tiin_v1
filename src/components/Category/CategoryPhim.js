import React, {Component} from 'react';
import CateInfo from './Partial/CateInfo';
import CatePhimHot from './Phim/CatePhimHot';
import CateChannelHot from './Phim/CateChannelHot';
import ListPhim from './Phim/ListPhim';
import CateApi from "../../services/api/Category/CategoryApi";
import Helper from "../../utils/helpers/Helper";
import VideoBoxAll from "../Video/partials/VideoBoxAll";
import ChannelApi from "../../services/api/Channel/ChannelApi";
import LoadingScreen from "../Global/Loading/LoadingScreen";

class CategoryPhim extends Component {
    constructor(props) {
        super(props);

        this.state = {
            videoFilm: [],
            filmHot: [],
            isShowAll: false,
            cateInfo: [],
            videoTrailerPhim: [],
            videoMotPhim: [],
            lastIdStr: ''
        };

        this.showAll = this.showAll.bind(this);
    }

    showAll() {
        this.setState({
            isShowAll: true
        });
    }

    componentDidMount() {
        let isScroll = false;
        window.onscroll = function() {
            isScroll = true;
            if(window.pageYOffset === 0) {
                isScroll = false;
            }
        };

        if (!isScroll) {
            this.fetchData();
        }
    }

    fetchData() {
        let lastIdStr = this.state.lastIdStr;
        CateApi.getFilmHot().then(
            (result) => {
                this.setState({
                    filmHot: Helper.checkObjExist(result, 'data.data.listVideo') ? result.data.data.listVideo : []
                });
            }
        );

        CateApi.getVideoAllFilm(10).then(
            (result) => {
                this.setState({
                    videoFilm: Helper.checkObjExist(result, 'data.data.listVideo') ? result.data.data.listVideo : [],
                    cateInfo: Helper.checkObjExist(result, 'data.data.cateInfo') ? result.data.data.cateInfo : []
                });
            }
        );
        ChannelApi.getVideoChannel(1476, 0, lastIdStr).then(
            (result) => {
                this.setState({
                    videoTrailerPhim: Helper.checkObjExist(result, 'data.data.listVideo') ? result.data.data.listVideo : []
                });
            }
        );
        ChannelApi.getVideoChannel(1478, 0, lastIdStr).then(
            (result) => {
                this.setState({
                    videoMotPhim: Helper.checkObjExist(result, 'data.data.listVideo') ? result.data.data.listVideo : []
                });
            }
        );
    }

    render() {
        const {videoFilm, filmHot, cateInfo, videoTrailerPhim, videoMotPhim} = this.state;
        var {isShowAll} = this.state;
        if (videoFilm.length > 0) {
            if (isShowAll) {
                return (
                    <div>
                        <CateInfo cateInfo={this.state.cateInfo}/>
                        <VideoBoxAll title="PHIM MỚI CẬP NHẬT" idSearch={this.props.idSearch} isFilm="1"/>
                    </div>
                );
            } else {
                return (
                    <div>
                        <CateInfo cateInfo={this.state.cateInfo}/>
                        <div id="lstMainFilm">
                            <div className="body-phim-center">
                                <div className="body-phim-left">
                                    <CatePhimHot filmHot={filmHot}/>
                                    <CateChannelHot video={videoTrailerPhim} type={'trailer'}/>
                                    <CateChannelHot video={videoMotPhim}/>
                                </div>
                                <div className="body-phim-right">
                                    <ListPhim videoFilm={videoFilm} showAllOnclick={this.showAll}/>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
        } else {
            return <LoadingScreen/>;
        }
    }
}

export default CategoryPhim;
