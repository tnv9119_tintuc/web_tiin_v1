import React, {Component} from 'react';
import ChannelItem from './ChannelItem';
import Helper from "../../../utils/helpers/Helper";

export class ChannelHotList extends Component {

    render() {
        var lstchannelHot = this.props.channelHot;

        return (
            <div className="bpl-trailer box-shadow-cm">
                <h3 className="bpl-trailer-h3">
                    KÊNH NỔI BẬT
                </h3>
                {this.showVideos(lstchannelHot)}
            </div>
        );
    }

    showVideos(lstchannelHot){
        Helper.checkIsChannelNotShow(lstchannelHot);
        var result = null;
        if(lstchannelHot) {
            if (lstchannelHot.length > 0) {
                result = lstchannelHot.map((channelHot, index) => {
                    if (channelHot != null){
                        return <ChannelItem key={index} channelHot={channelHot}/>
                    }
                });
            }
        }
        return result;
    }
}

export default ChannelHotList;
