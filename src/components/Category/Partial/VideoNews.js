import React, {Component} from 'react';
import VideoItem from '../../Video/partials/VideoItem';

class VideoNew extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var videos = this.props.video;
        var isVideoOfChannel = this.props.isVideoOfChannel;
        return (
            <div className="body-phim-right">
                <div className={isVideoOfChannel ? "bpr-video box-shadow-cm pdbt-cn" : "bpr-new-cate box-shadow-cm"}>
                    <h3 className="bpr-new-h3-cate">
                        VIDEO HOT</h3>
                    <div className={isVideoOfChannel ? "bpr-video-wrap" : "bpr-new-wrap bpr-new-wrap-cate"}>
                        {this.showVideos(videos, isVideoOfChannel)}
                    </div>
                    <a onClick={this.props.showAllOnclick} className="p-more-video">Xem tất cả </a>
                </div>
            </div>
        );
    }

    showVideos(videos, isVideoOfChannel) {
        var result = null;
        if (videos) {
            if (videos.length > 0) {
                result = videos.map((video, index) => {
                    if (index < 20) {
                        if ((index + 1) % 2 == 0) {
                            return (
                                <div key={index}>
                                    <VideoItem key={index} video={video} isVideoOfChannel={isVideoOfChannel}/>
                                    <div className="clear"></div>
                                </div>
                            )
                        } else {
                            return (
                                <VideoItem key={index} video={video} isVideoOfChannel={isVideoOfChannel}/>
                            )
                        }
                    }
                });
            }
        }
        return result;
    }
}

export default VideoNew;
