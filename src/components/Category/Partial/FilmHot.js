import React from 'react';
import {DEFAULT_IMG} from "../../../config/Config";
//import {Link} from '../../../routes';
import  Link from 'next/link';
import Helper from "../../../utils/helpers/Helper";

class FilmHot extends React.Component {

    render() {
        var filmHot = this.props.filmHot;

        return (
            <div className="av-top-small swiper-slide">
                <Link href="/video"  as = {Helper.replaceUrl(filmHot.link) + ".html?src=phim"} >
                    <a title={filmHot.name} className="ats-lnk-img">

                        <img alt={filmHot.name}
                             title={filmHot.name} className="ats-img"
                             src={filmHot.filmGroup.groupImage ? filmHot.filmGroup.groupImage : DEFAULT_IMG}
                             onError={(e) => {
                                 e.target.onerror = null;
                                 e.target.src = DEFAULT_IMG
                             }}
                        />
                        {filmHot.filmGroup.currentVideo ?
                            <span className="sp-number-film subSp-filmHot">{filmHot.filmGroup.currentVideo}</span>
                            : ''
                        }
                    </a>

                </Link>
                <div className="ats-info">
                    <h3 className="ats-info-h3">
                        <Link href="/video" as={Helper.replaceUrl(filmHot.link) + ".html?src=phim"} >
                            <a title={filmHot.name} className="ats-info-lnk">
                                {filmHot.name}<span className="sp-chanel-follow">{Helper.formatNumber(filmHot.isView)} lượt xem</span>
                            </a>

                        </Link>
                    </h3>
                </div>
            </div>
        );
    }
}

export default FilmHot;
