import {Component} from 'react';
import { withRouter } from 'next/router';

class ScrollToTop extends Component {
    componentDidUpdate(prevProps) {
        if (this.props.router.asPath !== prevProps.router.asPath) {
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }
    }

    render() {
        return this.props.children;

    }
}

export default withRouter(ScrollToTop);

