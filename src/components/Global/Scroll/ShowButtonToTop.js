import {Component} from 'react';
import ScrollUpButton from 'react-scroll-up-button';
import {DOMAIN_IMAGE_STATIC} from "../../../config/Config";
const icon_gototop = DOMAIN_IMAGE_STATIC + 'icon_gototop.png';

class ShowButtonToTop extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollUpButton
                ContainerClassName="up_button_container"
                TransitionClassName="up_button_transition"
            >
                <a>
                    <img src={icon_gototop} alt="goto top" style={{paddingBottom: '15px'}}/>
                </a>
            </ScrollUpButton>
        );
    }
}

export default ShowButtonToTop;

