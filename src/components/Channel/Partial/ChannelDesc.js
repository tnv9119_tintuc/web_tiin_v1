import React from 'react';
import Helper from "../../../utils/helpers/Helper";

import {DOMAIN_IMAGE_STATIC} from "../../../config/Config";
const icon_info_01 = DOMAIN_IMAGE_STATIC + 'mychannel/icon_info_01.png';
const icon_info_02 = DOMAIN_IMAGE_STATIC + 'mychannel/icon_info_02.png';
const icon_info_03 = DOMAIN_IMAGE_STATIC + 'mychannel/icon_info_03.png';

class ChannelDesc extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var videos = this.props.video;
        var infoChannel = this.props.infoChannel;
        var desc, datecreate, numVideo;

        desc = (
            <div className="bpl-info-row">
                <span className="info-row-left">
                    <img src={icon_info_01}/>
                </span>
                <p className="info-row-right">
                    <span className="fontbold">Mô tả: </span>
                    {infoChannel.description ? infoChannel.description : ''}
                </p>
            </div>
        );


        datecreate = (
            <div className="bpl-info-row">
                            <span className="info-row-left">
                                <img src={icon_info_02}/>
                            </span>
                <p className="info-row-right">
                    <span className="fontbold">Ngày thành lập: </span>
                    {infoChannel.createdDateTime ? Helper.formatDate(infoChannel.createdDateTime) : ''}
                </p>
            </div>
        );


        numVideo = (
            <div className="bpl-info-row">
                            <span className="info-row-left">
                                <img src={icon_info_03}/>
                            </span>
                <p className="info-row-right">
                    <span className="fontbold">Tổng số video: </span>
                    {infoChannel.numVideo ? infoChannel.numVideo : 0}
                </p>
            </div>
        );


        return (
            <div className="bpl-info box-shadow-cm">
                <h3 className="bpl-info-h3">
                    GIỚI THIỆU</h3>
                {desc}
                {datecreate}
                {numVideo}
            </div>
        );
    }
}

export default ChannelDesc;
