import React  from 'react';
import ChannelDesc from './Partial/ChannelDesc';
import VideoNew from "../Category/Partial/VideoNews";
import VideoHotList from "../Category/Partial/VideoHotList";

class ChannelTabHome extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var video = this.props.video;
        var videoHot = this.props.videoHot;
        var infoChannel = this.props.infoChannel;

        return (
            <div>
                <div id="lstHome">
                    <div className="body-phim-center">
                        <div className="body-phim-left">
                            <ChannelDesc infoChannel={infoChannel}/>
                            <VideoHotList videoHot={videoHot} isChannel="1"/>
                        </div>
                        <VideoNew video={video} isVideoOfChannel={true} showAllOnclick={this.props.showAllOnclick}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default ChannelTabHome;
