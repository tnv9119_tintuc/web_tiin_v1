import React, {Component} from 'react';
import Helper from "../../../utils/helpers/Helper";

import axios from 'axios';
import * as Config from "../../../config/Config";
import {Button, Modal, ModalBody, ModalFooter} from 'reactstrap';
import Link from 'next/link';
import qs from "qs";
import {DEFAULT_IMG} from "../../../config/Config";
import {ImageSSR} from "../../Global/ImageSSR/ImageSSR";
import ChannelApi from "../../../services/api/Channel/ChannelApi";

import {DOMAIN_IMAGE_STATIC} from "../../../config/Config";
const comment_03 = DOMAIN_IMAGE_STATIC + 'comment_03.png';
const comment_04 = DOMAIN_IMAGE_STATIC + 'comment_04.png';
const comment_06 = DOMAIN_IMAGE_STATIC + 'comment_06.png';

class EditVideo extends React.Component {
    constructor(props) {
        super(props);
        let video = this.props.video;

        this.state = {
            messError: '',
            modal: false,
            enabledSave: false,

            file: '',
            imagePreviewUrl: '',
            videoTitle: video.name ? video.name : '',
            videoDesc: video.description ? video.description : '',
            videoImage: video.image_path ? video.image_path : ''
        };

        this._handleImageChange = this._handleImageChange.bind(this);
        this._handleImageClick = this._handleImageClick.bind(this);
        this._handleInputName = this._handleInputName.bind(this);
        this._handleInputDesc = this._handleInputDesc.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggle = this.toggle.bind(this);

    }

    toggle() {
        this.setState({
            modal: false,
            enabledSave: false
        });
    }

    _handleInputName(e) {
        const videoNameNew = e.target.value;
        if (videoNameNew.length > 0) {
            this.setState({
                videoTitle: e.target.value,
                enabledSave: true,
                messError: ''
            });
        } else {
            this.setState({
                enabledSave: false,
                //messError: 'Tiêu đề video không được để trống'
            });
        }
    }

    _handleInputDesc(e) {
        this.setState({
            videoDesc: e.target.value,
            enabledSave: true
        });
    }

    _handleImageClick() {
        this.inputImage.click();
    }

    _handleImageChange(e) {

        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        if (file.type.includes('image/')) {
            reader.onloadend = () => {
                this.setState({
                    file: file,
                    imagePreviewUrl: reader.result,
                    messError: ''
                });
            };
            reader.readAsDataURL(file);
            let formData = new FormData();
            formData.set('fileName', file.name);
            formData.append('uploadFile', file);
            axios({
                method: 'POST',
                url: Config.MV_SERVICE_UPLOAD + Config.API_UPLOAD_IMAGE,
                data: formData,
                headers: {'Content-type': 'multipart/form-data'}
            }).then(({data}) => {
                Helper.renewToken(data);

                if (data.code == 200){
                    this.setState({
                        videoImage: data.path,
                        enabledSave: (this.state.videoTitle.length > 0)
                    });
                } else {
                    this.setState({
                        videoImage: '',
                        messError: 'Ảnh đại diện video không đúng định dạng.',
                        enabledSave: false,
                        file: '',
                        imagePreviewUrl: '',
                    });
                }
            }).catch(function (result) {

            });
        } else {
            this.setState({
                file: '',
                imagePreviewUrl: '',
                messError: 'Ảnh đại diện video không đúng định dạng.',
                enabledSave: false
            });
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        const video = this.props.video;
        const {videoTitle, videoDesc, videoImage, enabledSave} = this.state;

        if (enabledSave) {
            let dataPost = {
                videoId: video.id,
                videoTitle: videoTitle,
                videoDesc: videoDesc,
                videoImage: videoImage,
                token: Helper.getToken()
            };

            axios({
                method: 'POST',
                url: Config.API_UPDATE_VIDEO,
                data: qs.stringify(dataPost),
                headers: {'content-type': 'application/x-www-form-urlencoded'}
            }).then(({data}) => {
                Helper.checkTokenExpired(data);
                Helper.renewToken(data);

                if (data.code === 200) {
                    ChannelApi.deleteCache(video.channels[0].id).then(
                        ({data}) => {
                        }
                    );
                    this.setState({
                        modal: true
                    });
                }
            }).catch(function (result) {});
        }
    }

    render() {
        const video = this.props.video;
        const {videoTitle, videoDesc, enabledSave, modal, messError} = this.state;

        let {imagePreviewUrl} = this.state;
        let imagePreview = null;
        if (imagePreviewUrl) {
            imagePreview = (
                <ImageSSR src={imagePreviewUrl ? imagePreviewUrl : DEFAULT_IMG} onClick={this._handleImageClick}
                     fallbackSrc={DEFAULT_IMG}
                />
            );
        } else {
            imagePreview = (
                <ImageSSR src={video.image_path ? video.image_path : DEFAULT_IMG} onClick={this._handleImageClick}
                     fallbackSrc={DEFAULT_IMG}
                />
            );
        }

        return (
            <div>
                <form>
                    <div className="blr-wrap box-shadow-cm">
                        <h2 className="blr-wrap-h2">
                            QUẢN LÝ VIDEO
                        </h2>
                        <p className="qlv-title">
                            {video.name}
                        </p>
                        <div className="qlv-info">
                            <div className="qlv-info-left">
                                <video width="400" height="225" preload="auto" controls
                                       poster={video.image_path}
                                       className="video-js vjs-default-skin" id="content_video">
                                    <source type="video/mp4" src={video.original_path}/>
                                </video>
                            </div>
                            <div className="qlv-info-right">
                                <h3 className="qlv-info-h3">
                                    THÔNG TIN VỀ VIDEO</h3>
                                <p className="qlv-info-p">
                                    <span className="qlv-sp-left">Thời gian xuất bản:</span>
                                    <span className="qlv-sp-right">
							            {Helper.getDate(video.DatePublish)}
                                    </span>
                                </p>
                                <p className="qlv-info-p">
                                    <span className="qlv-sp-left">Lượt thích:</span> <span
                                    className="qlv-sp-right">{video.total_like}<img src={comment_03}/></span>
                                </p>
                                <p className="qlv-info-p">
                                    <span className="qlv-sp-left">Lượt bình luận:</span> <span
                                    className="qlv-sp-right">{video.total_comment}<img src={comment_04}/></span>
                                </p>
                                <p className="qlv-info-p">
                                    <span className="qlv-sp-left">Lượt xem:</span>
                                    <span className="qlv-sp-right">{Helper.formatNumber(video.isView)}
                                        <img src={comment_06}/>
                                    </span>
                                </p>
                                <p className="qlv-info-p">
                                    <span className="qlv-sp-left">Link video:</span> <span className="qlv-sp-right">
                                        <Link href="/video" as={Helper.replaceUrl(video.link) +'.html'}>
                                            <a>
                                                {video.link}
                                            </a>
                                        </Link>
                                </span>
                                </p>
                            </div>
                        </div>

                        <h3 className="wc-edit-h3">
                            CHỈNH SỬA VIDEO</h3>
                        <div className="blr-form blr-form-video">
                            <div className="blr-form-left">
                                <h3 className="bfl-h3">
                                    Hình đại diện video</h3>
                                <p title="Thay đổi ảnh đại diện" className="bfl-p">
                                    {imagePreview}
                                    <input type="file" name="imageFile" className="hidden"
                                           onChange={(e) => {
                                               this._handleImageChange(e)
                                           }}
                                           ref={(input) => this.inputImage = input}
                                    />
                                    <a className="avatar-edit" onClick={this._handleImageClick}/>
                                </p>
                            </div>
                            <div className="mc-form-right">
                                <p className="bfr-p-text">
                                    Tiêu đề video</p>
                                <p className="bfr-p-input">
                                    <input placeholder="Tiêu đề video"
                                           defaultValue={videoTitle ? videoTitle : video.name} maxLength="500"
                                           onChange={(event) => {
                                               this._handleInputName(event)
                                           }}
                                    />
                                    <br/>
                                </p>
                                <p className="bfr-p-text">
                                    Mô tả video</p>
                                <p className="bfr-p-input">
                                    <textarea placeholder="Nhập mô tả" maxLength={4000}
                                              defaultValue={videoDesc ? videoDesc : video.description}
                                              onChange={event => {
                                                  this._handleInputDesc(event)
                                              }}
                                    />
                                </p>
                                <span style={{color: 'Red'}}>{messError}</span>
                                <p className="bfr-p-btn">
                                    <a className={'chanel-form-btn' + (enabledSave ? ' active' : '')}
                                       onClick={this.handleSubmit}>LƯU</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>

                <Modal isOpen={modal} toggle={this.toggle} style={{width: '340px', top: '15%'}}>
                    <ModalBody>
                        Chỉnh sửa video thành công.
                    </ModalBody>
                    <ModalFooter className="popup-create-btn" style={{height: '10px', paddingTop: '25px'}}>
                        <div>
                            <p className="pcb-continue" style={{margin: '0px 10px 0px 0px', color: '#5e48ce'}} style={{color: '#5e48ce'}} onClick={((e) => this.handleClick(video.id))}>Đóng</p>
                        </div>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

    handleClick = (id) => {
        this.setState({
            modal: false
        });
        this.props.callBack(id);
    }
}

export default EditVideo;
