import React, {Component} from 'react';
import Helper from "../../../utils/helpers/Helper";
import BoxPhimGrp from "../Partial/BoxPhimGrp";
import {DEFAULT_AVATAR} from "../../../config/Config";
//import {Link} from "../../../routes";
import  Link from 'next/link';
import {ImageSSR} from "../../Global/ImageSSR/ImageSSR";

class MyAccount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data:'',
            value:'',
            display:'',
            valueSearch: ''
        };

    }
    handleChange=(e)=> {
        const {value} = e.target;
        this.setState({
            value: value
        });
    }
    handleInputChange=(e)=> {
        const {value} = e.target;
        this.setState({
            valueSearch: value
        });
    }

    handleInputKeyPress=(e)=> {
        if (e.key === 'Enter') {
            if (this.state.value == ''){
                alert('Bạn vui lòng nhập tên video cần tìm doanh thu');
            } else {
                this.setState({
                    display: 'none'
                });
            }
        }
    }
    render() {
        var value = this.state.value;
        var {channelRevenue, infoChannel, VideoRevenue, point} = this.props;
        var content;
        if (channelRevenue){
            if (value == '' || value == 1){
                content = channelRevenue[0];
            } else if (value == 2) {
                content = channelRevenue[1];
            }else if (value == 3){
                content = channelRevenue[2];
            }else {
                content = channelRevenue[3];
            }
        }
        if (content){
            return (
                <div className="blr-wrap box-shadow-cm">
                    <h2 className="blr-wrap-h2">
                        THỐNG KÊ DOANH THU
                    </h2>
                    <div className="wc-tab-user">
                        <div className="tab-user-left">
                            <ImageSSR src={infoChannel.url_avatar}
                                     fallbackSrc={DEFAULT_AVATAR}/>
                            <div className="tab-user-info">
                                <h3>
                                    {Helper.checkObjExist(infoChannel, 'name') ? infoChannel.name : ''}</h3>
                                <p>Ngày tham gia: {Helper.formatDate(infoChannel.createdDateTime)} - {infoChannel.numVideo} Video</p>
                            </div>
                        </div>
                        <div className="tab-user-right">
                            <p className="user-p-doanhthu">
                                <span>Tổng doanh thu tạm tính hiện tại</span><br/>
                                {point} Vnd</p>
                        </div>
                    </div>
                    <div className="wc-tab-doanhthu">
                        <span className="sp-doanhthu-left">DOANH THU</span> <span className="sp-doanhthu-right"
                                                                                  id="id_sp-doanhthu-right">
            <select id="ddTimeSpoint" value={this.state.value}
                    onChange={this.handleChange}>
              <option value={1}>Tuần này</option>
              <option value={2}>Tuần trước</option>
              <option value={3}>Tháng này</option>
              <option value={4}>Tháng trước</option>
            </select></span>
                    </div>
                    <div className="wc-tab-user">
                        <div className="tab-user-left">
                            <p className="user-p-doanhthu">
                                <span id="span_doanhthu">Doanh thu dự tính tháng này</span><br/>
                                <span id="sp_dateSpoint">{content.rangeTime}</span>
                            </p>
                        </div>
                        <div className="tab-user-right">
                            <span className="sp-spoint" id="sp_resultSpoint">{content.spoint}</span>
                        </div>
                    </div>
                    <div className="wc-tab-search">
                        <h3 className="tab-search-h3">
                            CHI TIẾT DOANH THU</h3>
                        <p className="tab-search-p" style={{background: '#7663d5 none repeat scroll 0 0'}}>
                            <input id="search_point" type="text" placeholder="Tìm kiếm doanh thu tạm tính theo video"
                                   className="search-txt searchInput"
                                   onChange={this.handleInputChange}
                                   onKeyPress={(e) => this.handleInputKeyPress(e)}
                                   value={this.state.valueSearch}/>
                            <a id="button_search" className="search-btn">
                            </a>
                        </p>
                    </div>
                    <div className="wc-tab-table" id="table_spoint">
                        <table>
                            <tbody>
                            <tr>
                                <th width="50%">
                                    Link video
                                </th>
                                <th width="15%">
                                    Loại Video
                                </th>
                                <th width="15%">
                                    Lượt xem 30s
                                </th>
                                <th width="15%">
                                    Doanh thu
                                </th>
                            </tr>
                            {this.show(VideoRevenue)}
                            </tbody>
                        </table>
                    </div>
                </div>
            );
        }else {
            return <div></div>
        }
    }
    showVideos(lstvideoFilm) {
        let result = null;
        if (lstvideoFilm) {
            result = lstvideoFilm.map((videoFilm, index) => {
                return <BoxPhimGrp key={index} videoFilm={videoFilm}/>
            });
        }
        return result;
    }
    show(videos){
        let style = this.state.display;
        let result = null;
        let point = this.props.point;
        if (videos) {
            result = videos.map((video, index) => {
                let percent = video.pointRevenue / point * 100;
                return  <tr key={index} style={{display:style}}>
                    <td>
                        <Link href="/video" as ={Helper.replaceUrl(video.link)+'.html'}>
                            <a>
                                {video.name}
                            </a>
                        </Link></td>
                    <td>Tổng hợp</td>
                    <td>{video.viewGr30s}</td>
                    <td>{video.pointRevenue} ({Math.round(parseFloat(percent) * 1000)/1000}%)
                    </td>
                </tr>
            });
        }
        return result;

    }
}

export default MyAccount;
