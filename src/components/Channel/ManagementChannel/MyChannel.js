import React from 'react';
import Helper from "../../../utils/helpers/Helper";
import VideoItem from './Partial/VideoItem';
import ChannelApi from "../../../services/api/Channel/ChannelApi";
import { withRouter } from 'next/router';
import {DEFAULT_IMG} from "../../../config/Config";


 class MyChannel extends React.Component {
    constructor(props) {
        super(props);

        let identify = this.props.router.query?this.props.router.query.identify : '';
        this.state = {
            identify: identify,
            videoMyChannel: []
        };
    }
    componentDidMount() {
        let formData = new FormData();
        formData.set('username', 'sieuhai');
        formData.set('password', 'sieuhai');
        formData.set('msisdn', '0335202868');
        formData.set('request_id', 'sieuhai');

        ChannelApi.getVideoMyChannel(this.state.identify, 0, '').then(
            ({data}) => {
                Helper.checkTokenExpired(data);
                Helper.renewToken(data);

                this.setState({
                    videoMyChannel: Helper.checkObjExist(data, 'data.listVideo') ? data.data.listVideo : []
                });
            }
        );
    }

    render() {
        const infoChannel = this.props.infoChannel;
        const videoNew = this.props.videoNew;
        var videoMyChannel = this.state.videoMyChannel;
        const editVideo = this.props.editVideo;
        return (
            <div>
                <div className="chanel-home-top box-shadow-cm">
                    <div className="cht-left">
                        <a className="cht-left-avatar">
                            <img alt={infoChannel.name} src={infoChannel.url_avatar}
                                 onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = DEFAULT_IMG
                                 }}/></a>
                        <div className="cht-left-info">
                            <h3 className="cht-left-h3">
                                {infoChannel.name}</h3>
                            <p className="chanel-edit" id="p_EditChannel">
                                {infoChannel.status && infoChannel.status == 2 ?
                                    <a title="Chỉnh sửa thông tin kênh" onClick={this.props.editChannel}>Chỉnh sửa</a>
                                    :
                                    <a className="waited" title="Đang chờ duyệt">Đang chờ duyệt</a>
                                }
                            </p>
                        </div>
                    </div>
                    <p className="cht-right">
                        {/*<span><strong>3.675</strong><br/>Lượt xem </span>*/}
                        <span><strong>{infoChannel.numfollow}</strong><br/>Đăng ký </span>
                        <span><strong>{infoChannel.mochaSpoint}</strong><br/>Spoint</span>
                    </p>
                </div>
                <div className="wrap-chanel-bottom">
                    <div className="wcb-left box-shadow-cm">
                        <h3 className="wcb-left-h3">VIDEO</h3>
                        {this.showVideos(videoMyChannel, editVideo)}
                        <p className="more-all-video"><a onClick={this.props.showTabVideo}>Xem tất cả &gt;</a></p>
                    </div>
                    <div className="wcb-right">
                        <div className="wcb-right-box box-shadow-cm">
                            <h3 className="wcb-right-title">
                                THÔNG TIN KÊNH
                            </h3>
                            <p className="wcb-right-p">
                                <span className="fontbold">Tên kênh: </span>
                                {infoChannel.name}
                            </p>
                            <p className="wcb-right-p">
                                <span className="fontbold">Mô tả: </span>
                                {infoChannel.description}
                            </p>
                            <p className="chanel-edit" id="p_EditChannel_Sub">
                                {infoChannel.status && infoChannel.status == 2 ?
                                    <a title="Chỉnh sửa thông tin kênh" onClick={this.props.editChannel}>Chỉnh sửa</a>
                                    :
                                    <a className="waited" title="Đang chờ duyệt">Đang chờ duyệt</a>
                                }
                            </p>
                        </div>
                        <div className="wcb-right-box box-shadow-cm">
                            <h3 className="wcb-right-title">
                                LỊCH SỬ KÊNH
                            </h3>
                            {infoChannel.createdDateTime ?
                                <p className="wcb-right-p">
                                    <span className="fontbold">Ngày thành lập: </span>
                                    {Helper.formatDate(infoChannel.createdDateTime)}
                                </p> : ''
                            }
                            <p className="wcb-right-p">
                                <span className="fontbold">Tổng số video: </span>
                                {infoChannel.numVideo}
                            </p>
                        </div>
                    </div>
                    <div className="clear"/>
                </div>
            </div>
        );
    }

    showVideos(videos, editVideo) {
        let result = null;
        if (videos) {
            result = videos.map((item, index) => {
                if (index < 4) {
                    return <VideoItem key={index} video={item} editVideo={editVideo} callBack={this.props.callBack} isVideoEdited={this.props.isVideoEdited}/>;

                }
            });
        }
        return result;
    }
}

export default withRouter( MyChannel);
