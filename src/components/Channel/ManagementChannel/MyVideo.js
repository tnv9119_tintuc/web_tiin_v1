import React from 'react';
import ChannelApi from "../../../services/api/Channel/ChannelApi";
import ErrorBoundary from "../../ErrorBoundary";
import VideoItem from "./Partial/VideoItem";
import Helper from "../../../utils/helpers/Helper";

class MyVideo extends React.Component {

    constructor(props) {
        super(props);
        let rows = 20;

        this.state = {
            isError: null,
            isLoaded: false,
            rows: rows,
            total: this.props.videoNew.length,
            results: [],
            index: 1,
            lastIdStr: ''
        };
    }

    componentDidMount() {
        this.fetchData(this.state.rows, this.state.index);
    }

    handleNext =() => {
        if (this.state.rows * this.state.index <= this.state.total) {
            let index = this.state.index + 1;
            this.fetchData(this.state.rows, index);
        }
    }

    handlePrev =()=> {
        if (this.state.index > 1) {
            let index = this.state.index - 1;
            this.fetchData(this.state.rows, index);
        }
    }

    fetchData(rows, index) {
        let start = (index - 1) * rows;
        let lastIdStr = this.state.lastIdStr;
        this.setState({
            index: index,
            start: start
        });
        ChannelApi.getVideoMyChannel(this.props.idSearch, start, lastIdStr).then(
            (response) => {
                if(response && response.data) {
                    this.setState({
                        lastIdStr: response.data.data.lastIdStr,
                        isLoaded: true,
                        results: Helper.checkObjExist(response.data, 'data.listVideo') ? response.data.data.listVideo : []
                    });
                }
            },
            (error) => {
                this.setState({
                    isError: true
                });
            });

    }


    render() {
        const {isError, isLoaded, index, rows, total, results} = this.state;
        let pagination = (
            <div className="pagination clear">
                <div className="tn-pagging">
                    <div className="pagging" id="idPagging" align="center">
                        <a style={{border: '1px solid rgba(0, 0, 0, 0.1)'}}
                           className="lnk-back"
                           onClick={this.handlePrev}>
                            &lt; Trang trước
                        </a>&nbsp;
                        <a style={{border: '1px solid rgba(0, 0, 0, 0.1)'}}>
                            {"Trang " + index + "/" + parseInt(total / rows + 1)}
                        </a>&nbsp;
                        <a style={{border: '1px solid rgba(0, 0, 0, 0.1)'}}
                           className="lnk-next"
                           onClick={this.handleNext}>
                            Trang sau &gt;
                        </a>
                    </div>
                </div>
            </div>);

        if (isError) {
            return (<div>Error...</div>);
        } else if (isLoaded) {

            return (
                <ErrorBoundary>
                    <div>
                        <div className="blr-wrap box-shadow-cm">
                            <h2 className="blr-wrap-h2">
                                QUẢN LÝ VIDEO</h2>
                            {this.showVideo(results)}
                        </div>
                        {pagination}
                    </div>
                </ErrorBoundary>
            );
        } else {
            return <div></div>;
        }
    }

    showVideo(results) {

        var result = "";
        if (results.length > 0) {
            result = results.map((result, index) => {
                return <ErrorBoundary><VideoItem video={result} idVideo={this.props.idVideo} key={index}
                                                 callBack={this.props.callBack}
                                                 editVideo={this.props.editVideo}/></ErrorBoundary>
            });
        }
        return result;
    }
}

export default MyVideo;
