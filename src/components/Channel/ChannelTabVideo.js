import React, {Component} from 'react';
import VideoItem from "../Video/partials/VideoItem";

class ChannelTabVideo extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(){
        window.scrollTop(0,0);
    }
    render() {
        var videos = this.props.video;
        return (
            <div>
                <div className="body-phim-center body-phim-center-tabVideoCn">
                    <div className="body-phim-right body-phim-right-tabVideoCn">
                        <div className="bpr-video bpr-video-tabVideoCn allVideo box-shadow-cm">
                            <h3 className="bpr-video-h3">
                                TẤT CẢ VIDEO
                            </h3>
                            <div className="bpr-video-wrap bpr-video-wrap-tabVideoCn">
                                {this.showVideos(videos,1)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    showVideos(videos,isVideoOfChannel){
        var result = null;
        if(videos){
            if(videos.length > 0){
                result = videos.map((video, index)=>{
                    return (
                        <VideoItem key={index} video={video} isVideoOfChannel={isVideoOfChannel}/>
                    )
                });
            }
        }
        return result;
    }
}

export default ChannelTabVideo;
