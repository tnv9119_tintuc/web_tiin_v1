import React, {Component} from 'react';
import BoxPhimGrp from './Partial/BoxPhimGrp';
import ChannelApi from "../../services/api/Channel/ChannelApi";
import Helper from "../../utils/helpers/Helper";
import InfiniteScroll from 'react-infinite-scroller';


class ChannelTabPhim extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
            page: 0,
            refreshing: false,
            hasMoreItems: true,
            items: [],
            isLoaded: false,
            pageSize: 20
        };

        this.loadItems = this.loadItems.bind(this);
    }

    componentDidMount() {
        this.loadItems();
    }

    loadItems() {
        const pageSize = this.state.pageSize;
        let page = this.state.page;

        ChannelApi.getFilmGrp(this.props.idSearch, page).then(
            (result) => {
                Helper.renewToken(result.data);

                let items = this.state.items;
                let len = items.length;
                if (Helper.checkArrNotEmpty(result, 'data.data.listVideo')) {
                    let res = result.data.data.listVideo;
                    let hasMore = ((len / res.length) * pageSize === (page - pageSize));
                    this.setState({
                            page: page + pageSize,
                            isLoaded: true,
                            hasMoreItems: hasMore,
                            items: items.concat(res)
                        }
                    );
                } else {
                    this.setState({
                        hasMoreItems: false
                    });
                }
            }
        );
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if(items.length > 0){
            return (
                <div>
                    <InfiniteScroll className="body-phim-center ofi"
                                    pageStart={0}
                                    loadMore={this.loadItems}
                                    hasMore={this.state.hasMoreItems}
                    >
                        {this.showVideos(items)}
                    </InfiniteScroll>
                </div>
            );
        }else{
            return(
                <div style={{textAlign: 'center'}}>Loading...</div>
            );
        }

    }

    showVideos(lstvideoFilm) {
        let result = null;
        if (lstvideoFilm) {
            result = lstvideoFilm.map((videoFilm, index) => {
                return <BoxPhimGrp key={index} videoFilm={videoFilm}/>
            });
        }
        return result;
    }

}

export default ChannelTabPhim;
