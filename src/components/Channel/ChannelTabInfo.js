import React, {Component} from 'react';
import ChannelDesc from "./Partial/ChannelDesc";
import BoxChannelFollow from '../Channel/Partial/BoxChannelFollow';
import ChannelApi from "../../services/api/Channel/ChannelApi";
import Helper from '../../utils/helpers/Helper';

class ChannelTabInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            channelFollow: [],
            channelRelated: []
        };
    }

    componentDidMount() {
        let infoChannel = this.props.infoChannel;
        let channelName = infoChannel.name;
        let channelId = infoChannel.id;
        ChannelApi.getChannelFollowOfChannel(this.props.idSearch, 20).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    channelFollow: Helper.checkObjExist(data, 'data.listChannel') ? data.data.listChannel : []
                });
            }
        );

        ChannelApi.getChannelRelated(0, channelId, channelName).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    channelRelated: Helper.checkObjExist(data, 'result') ? data.result : []
                });
            }
        );
    }

    render() {
        const {channelFollow, channelRelated} = this.state;
        var infoChannel = this.props.infoChannel;

        return (
            <div>
                <div id="lstHome">
                    <div className="body-phim-center">
                        <div className="body-phim-left">
                            <ChannelDesc infoChannel={infoChannel}/>
                        </div>
                        <div className="body-phim-right">
                            <BoxChannelFollow title={"KÊNH ĐƯỢC THEO DÕI"} channels={Helper.checkIsChannelNotShow(channelFollow)}/>

                            <BoxChannelFollow title={"KÊNH LIÊN QUAN"} channels={Helper.checkIsChannelNotShow(channelRelated)}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ChannelTabInfo;
