import React, {Component} from 'react';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
//import 'bootstrap/dist/css/bootstrap.css';


class ModalPartial extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {callOk, type} = this.props;
        return (
            <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} style={{width: '340px', top: '15%'}}>
                <ModalBody>
                    {this.props.body}
                </ModalBody>
                <ModalFooter className="popup-create-btn" style={{height: '10px'}}>
                    <div>
                        <p className="pcb-continue" onClick={type === 'editChannel' ? callOk : this.props.toggle}
                           style={{margin: '0px 10px 0px 0px'}}>OK
                        </p>
                    </div>
                </ModalFooter>
            </Modal>
        );
    }
}

export default ModalPartial;
