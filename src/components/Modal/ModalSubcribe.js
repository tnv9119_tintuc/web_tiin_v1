import React, {Component} from 'react';
import { Modal,  ModalBody, ModalFooter} from 'reactstrap';
//import 'bootstrap/dist/css/bootstrap.css';


class ModalSubcribe extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal isOpen={this.props.isOpen} style={{width: '340px', top: '15%'}} toggle={this.props.toggle} >
                {/*<ModalHeader toggle={this.toggle}></ModalHeader>*/}
                <ModalBody>
                    {this.props.body}
                </ModalBody>
                <ModalFooter className="popup-create-btn" style={{height: '10px'}}>
                    <div>
                        <p className="pcb-continue" onClick={this.props.callbackUnfollow}
                           style={{margin: '10px 10px 0px 0px'}}>OK
                        </p>
                        <p onClick={this.props.toggle} className="pcb-continue">
                            Hủy
                        </p>
                    </div>
                </ModalFooter>
            </Modal>
        );
    }
}

export default ModalSubcribe;
