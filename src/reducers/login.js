import { combineReducers } from 'redux';
import {
    SET_USER,
    SET_ERROR,
} from '../actions/index';

var initialState = [];

function  getLogin(state = [], action) {
    switch (action.type) {
        case SET_USER:
            return Object.assign({}, state, {
                data: action.data
            });
        case SET_ERROR:
            return [
                ...state,
                {
                    data: action.data
                }
            ]
        default:
            return state;
    }
}

const login = combineReducers({
    getLogin
})

export default login;
