const express = require('express');
const next = require('next');

// Import middleware.
const routes = require('./src/routes');

// Setup app.
const app = next({dev: 'production' !== process.env.NODE_ENV});

const handler = routes.getRequestHandler(app, ({req, res, route, query}) => {
    app.render(req, res, route.page, query)
});

app.prepare()
    .then(() => {
        // Create server.
        const server = express();

        server.get('/hot.html', (req, res) => {
            app.render(req, res, '/hot');
        });

        server.get('/login.html', (req, res) => {
            app.render(req, res, '/login');
        });

        server.get('/uploadvideo.html', (req, res) => {
            app.render(req, res, '/uploadvideo');
        });

        server.get('/search.html', (req, res) => {
            app.render(req, res, '/search', {key: req.query.key});
        });

        server.get('/profile.html', (req, res) => {
            app.render(req, res, '/profile');
        });

        server.get('/more.html', (req, res) => {
            app.render(req, res, '/more');
        });

        server.get('/contact.html', (req, res) => {
            app.render(req, res, '/contact');
        });

        server.get('/faq.html', (req, res) => {
            app.render(req, res, '/faq');
        });

        server.get('/gioithieuapp.html', (req, res) => {
            app.render(req, res, '/gioithieuapp');
        });

        server.get('/:slug' + '-cg' + ':id' + '.html', (req, res) => {
            app.render(req, res, '/category', {slug: req.params.slug, id: req.params.id});
        });

        server.get('/:slug' + '-cn' + ':identify' + '.html', (req, res) => {
            app.render(req, res, '/channel', {slug: req.params.slug, identify: req.params.identify});
        });

        server.get('/:slug' + '-v' + ':id([0-9]+)' + '.html', (req, res) => {
            app.render(req, res, '/video');
        });

        server.get('/:slug' + '-t' + ':id([0-9]+)' + '.html', (req, res) => {
            app.render(req, res, '/tvideo');
        });

        server.get('/phim.html', (req, res) => {
            app.render(req, res, '/phim');
        });

        server.get('/guide.html', (req, res) => {
            app.render(req, res, '/guide');
        });

        server.get('/provision.html', (req, res) => {
            app.render(req, res, '/provision');
        });

        server.get('/download.html', (req, res) => {
            app.render(req, res, '/download');
        });

        server.get('/createchannel.html', (req, res) => {
            app.render(req, res, '/createchannel');
        });

        server.get('/game8-3.html',(req,res)=>{
            app.render(req,res,'/game8-3');
        });

        server.get('/function.html',(req,res)=>{
            app.render(req,res,'/function');
        });

        server.get('/thelegame8-3.html',(req,res)=>{
            app.render(req,res,'/thelegame8-3');
        });

        server.get('/gametet.html',(req,res)=>{
            app.render(req,res,'/gametet');
        });

        server.get('/hdchuyenngu.html',(req,res)=>{
            app.render(req,res,'/hdchuyenngu');
        });

        server.get('/hdupload.html',(req,res)=>{
            app.render(req,res,'/hdupload');
        });

        server.get('/ipage.html',(req,res)=>{
            app.render(req,res,'/ipage');
        });

        server.get('/quayso19.html',(req,res)=>{
            app.render(req,res,'/quayso19');
        });

        server.get('/thelegametet.html',(req,res)=>{
            app.render(req,res,'/thelegametet');
        });

        server.get('/thelesharevideo10k.html',(req,res)=>{
            app.render(req,res,'/thelesharevideo10k');
        });

        server.get('/theletopdiem8-3.html',(req,res)=>{
            app.render(req,res,'/theletopdiem8-3');
        });

        server.get('/theleupload.html',(req,res)=>{
            app.render(req,res,'/theleupload');
        });

        server.get('/thuthachdata.html',(req,res)=>{
            app.render(req,res, '/thuthachdata');
        });
        // Use our handler for requests.
        server.use(handler);

        // Don't remove. Important for the server to work. Default route.
        server.get('*', (req, res) => {
            return handler(req, res);
        });

        // Get current port.
        const port = process.env.PORT || 3000;

        // Error check.
        server.listen(port, err => {
            if (err) {
                throw err;
            }
            // Where we starting, yo!
            console.log(`> Ready on port ${port}...`);
        });
    });

