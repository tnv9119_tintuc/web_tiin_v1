
const withCSS = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');
const withImages = require('next-images');
const withFonts = require('next-fonts');



module.exports = withCSS(withSass(withImages(withFonts({
        onDemandEntries: {
            // period (in ms) where the server will keep pages in the buffer
            maxInactiveAge: 1000 * 60 * 60,
            // number of pages that should be kept simultaneously without being disposed
            pagesBufferLength: 5,
        },
        webpack(config, options) {
            // Further custom configuration here
            return config
        },
        // exportPathMap: async function() {
        //     //var videoPath ='/:slug' + '-v' + ':id([0-9]+)'+'.html';
        //     //var tVideoPath ='/:slug' + '-t' + ':id([0-9]+)'+'.html';
        //     const paths = {
        //         // '/': { page: '/' },
        //         // '/hot.html': {page: '/hot'},
        //         // '/login.html': {page: '/login'},
        //         // //'/uploadvideo.html': {page: '/uploadvideo'},
        //         // '/search.html': {page: '/search'},
        //         // '/profile.html': {page: '/profile'},
        //         // '/more.html': {page: '/more'},
        //         // '/contact.html': {page: '/contact'},
        //         // '/faq.html': {page: '/faq'},
        //         // '/gioithieuapp.html': {page: '/gioithieuapp'},
        //         // '/phim.html': {page: '/phim'},
        //         // //'/createchannel.html': {page: '/createchannel'},
        //         // '/guide.html': {page: '/guide'},
        //         // '/provision.html': {page: '/provision'},
        //         // '/download.html': {page: '/download'},
        //
        //     };
        //     //paths[videoPath]={ page: '/video' };
        //     //paths[tVideoPath]={ page: '/tvideo' };
        //
        //     return paths;
        // }

    }
))));


//const withPlugins = require("next-compose-plugins");
//module.exports = withPlugins([withCSS, withSass, withImages,withFonts]);