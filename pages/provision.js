import React, {Component} from "react";
//import '../../../styles/GioiThieuApp.scss';
import Link from 'next/link';

import {DOMAIN_IMAGE_STATIC} from "../src/config/Config.js";
const logo_mocha = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_mocha.png";
const provision = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/provision.jpg";
const logo_viettel = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_viettel.png";
const logo_congthuong = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_congthuong.png";


export class Provision extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id="wrapper">
                <style dangerouslySetInnerHTML={{__html: "\n #header { z-index: 9; width: 100%; background: #f7f7f7; position: static;}\n #body {min-height: 0; height: 100%; overflow: hidden; width: 100%; background: #e0d2f9 !important ; } \n .mocha-guide {padding: 15px 0px; background: none;} \n #footer { background: #f5f5f5 none repeat scroll 0 0; padding: 25px 0px 25px 0px; margin-top: 0;}"}}/>
                <div id="header">
                    <div className="header-top">
                        <Link href="/" >
                            <a style={{float: 'left', marginTop: '5px'}}>
                                <img height={35} src={logo_mocha}/>
                            </a>

                        </Link>
                        <Link  href="/contact"
                               as="/contact.html"
                        >
                            <a className="top-menu">LIÊN HỆ</a>
                        </Link>
                        <Link  href="/faq"
                               as="/faq.html"
                        >
                            <a className="top-menu">HỎI &amp; ĐÁP</a>
                        </Link>
                        <Link href="/gioithieuapp"
                              as="/gioithieuapp.html" >
                            <a className="top-menu">TÍNH NĂNG</a>
                        </Link>
                    </div>
                </div>
                <div id="body">
                    <div style={{width: '100%', textAlign: 'center', position: 'relative', zIndex: 999, padding: '20px 0' }}>
                        <img src={provision} style={{borderRadius: '8px'}}/>
                    </div>
                </div>
                <div id="footer">
                    <div className="mocha-footer">
                        <Link  href="/">
                            <a className="logo-mocha"></a>
                        </Link>
                        <p className="provision-intro">
                            Cơ quan chủ quản: Viettel Telecom - Chăm sóc khách hàng 18008098 (Miễn phí)
                            <br/>
                            <Link  href="/provision"
                                   as="/provision.html"
                            >
                                <a className="provision">Điều khoản sử dụng</a>
                            </Link>
                            |
                            <Link  href="/guide"
                                   as="/guide.html"
                            >
                                <a className="provision">
                                    Hướng dẫn sử dụng
                                </a>

                            </Link>
                        </p>
                        <a href="#" className="logo-viettel">
                            <img src={logo_viettel}/>
                        </a>
                        <a href="http://online.gov.vn/HomePage.aspx" className="logo-viettel logo-congthuong">
                            <img src={logo_congthuong}/>
                        </a>
                    </div>
                </div>
            </div>

        );
    }

}

export default Provision;
