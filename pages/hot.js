import React from 'react';
import Slide from "../src/components/Slides/Slide";
import ErrorBoundary from "../src/components/ErrorBoundary";

import Helper from "../src/utils/helpers/Helper";

import VideoBox4Video from "../src/components/Video/partials/VideoBox4Video";
import ChannelSubscribe from "../src/components/Home/ChannelSubscribe";
import ChannelApi from "../src/services/api/Channel/ChannelApi";
import CateApi from "../src/services/api/Category/CategoryApi";
import LoadingScreen from "../src/components/Global/Loading/LoadingScreen";
import Phim from "../src/components/Slides/SlideFilmHome/Phim";
import VideoNew from "../src/components/Slides/SlideVideoHome/VideoNew";

class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false,
            videoBanner: [],
            videoNew: [],
            videoHot: [],
            videoFilm: [],
            videoHai: [],
            videoTheThao: [],
            videoSong: [],
            videoSao: [],
            videoDep: [],
            videoChannelSubcribe: [],
            channelSubcribe: [],
            arrShow: [
                {title: "BANNER", path: ""},            //0
                {title: "MỚI CẬP NHẬT", path: "/moi-cap-nhat-cg1002"},      //1
                {title: "VIDEO HOT", path: "/hot-cg1001"},         //2
                {title: "KÊNH THEO DÕI", path: ""},     //3
                {title: "PHIM", path: "/phim"},              //4
                {title: "HÀI", path: "/hai-cg7"},               //5
                {title: "KHÁC", path: "/more"}               //6
            ],
            lastIdStr: ''
        };
    }
    static async getInitialProps({req,res}) {
        // if(res)
        // {
        //     res.redirect('/hot');
        // }
    }


      componentDidMount() {
        let lastIdStr = this.state.lastIdStr;
        CateApi.getVideoAll(1002, 0, lastIdStr).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    isLoaded: true,
                    videoNew: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            }
        );
        CateApi.getVideoAll(1001, 0, lastIdStr).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    videoHot: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            }
        );

        CateApi.getVideoAll(7, 0, lastIdStr).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    videoHai: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            }
        );

        CateApi.getVideoAll(3, 0, lastIdStr).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    videoTheThao: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            }
        );

        CateApi.getVideoAllFilm(10).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    videoFilm: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            }
        );

         CateApi.getVideoBanner(1001).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    videoBanner: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            }
        );

        if (Helper.checkUserLoggedIn()) {
            ChannelApi.getVideoNewPublish(lastIdStr).then(
                ({data}) => {
                    Helper.checkTokenExpired(data);
                    Helper.renewToken(data);

                    if (Helper.checkArrNotEmpty(data, 'data.listVideo')) {
                        this.setState({
                            videoChannelSubcribe: data.data.listVideo
                        });
                    }

                }
            );

            ChannelApi.getChannelUserFollow().then(
                ({data}) => {
                    Helper.checkTokenExpired(data);
                    Helper.renewToken(data);

                    if (Helper.checkArrNotEmpty(data, 'data.listChannel')) {
                        this.setState({
                            channelSubcribe: data.data.listChannel
                        });
                    }
                }
            );
        }

    }

    render() {

        const {channelSubcribe, videoChannelSubcribe, videoNew, isLoaded, videoFilm, videoBanner, videoHot, videoHai} = this.state;
        if (!isLoaded) {
            return <LoadingScreen/>;
        } else {
            return (
                <div>
                    <ErrorBoundary>
                        <div id="body">
                            {this.showHome(channelSubcribe, videoChannelSubcribe, videoNew, videoFilm, videoBanner, videoHot, videoHai)}
                            <div className="clear"/>
                        </div>
                    </ErrorBoundary>
                </div>
            );
        }
    }

    showHome(channelSubcribe, videoChannelSubcribe, videoNew, videoFilm, videoBanner, videoHot, videoHai) {
        var result = null;
        result = this.state.arrShow.map((item, index) => {
            switch (index) {
                case 0: {
                    return <Slide key={index} slides={videoBanner}/>;
                }
                case 1: {
                    return <VideoNew key={index} title={item.title} videos={Helper.checkIsChannelNotShowVideo(videoNew)}
                                     path={item.path}/>;
                }
                case 2: {
                    return <VideoBox4Video key={index} title={item.title} videos={Helper.checkIsChannelNotShowVideo(videoHot)} path={item.path}/>;
                }
                case 3: {
                    if (Helper.checkUserLoggedIn()) {
                        return <ChannelSubscribe key={index} title={item.title} videos={Helper.checkIsChannelNotShowVideo(videoChannelSubcribe)}
                                                 channels={Helper.checkIsChannelNotShow(channelSubcribe)}/>;
                    }
                    break;
                }
                case 4: {
                    return <Phim key={index} title={item.title} videos={Helper.checkIsChannelNotShowVideo(videoFilm)} path={item.path}/>;
                }
                case 5: {
                    return <VideoBox4Video key={index} title={item.title} videos={Helper.checkIsChannelNotShowVideo(videoHai)} path={item.path}/>;
                }
                case 6: {
                    return <VideoBox4Video key={index} title={item.title}
                                           others={true}
                                           path={item.path}/>;
                }
                default: {
                    break;
                }
            }
        });
        return result;
    }
}

export default Home;
