import React, {Component} from 'react';
import Link from "next/link";
import Loading from "../src/components/Global/Loading/LoadingScreen";
import {DOMAIN_IMAGE_STATIC} from "../src/config/Config";

 class TheLeGameTet extends Component {
    constructor(props) {
        super(props);
    }
    render() {
            return (
                <div>
                    <head>
                        <meta charset="utf-8" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                            <title>Thể lệ game</title>

                            <link href="http://live84.keeng.net/playnow/images/static/web_static/tet/css/Style.css" rel="stylesheet" type="text/css" />
                    </head>
                    <body class="content-thele-game" style={{padding:'unset'}}>
                    <div class="header">
                        <p>THỂ LỆ GAME</p>
                        <p>"HEO VÀNG MAY MẮN"</p>
                    </div>
                    <div class="box-content-1" style={{paddingTop:'90px'}}>
                        <p>
                            <i>
                                Như một niềm vui mà Viettel mang đến cho mỗi khách hàng thân yêu trong dịp Tết Kỷ Hợi 2019,
                                tham gia chơi Game "Heo vàng may mắn" trên các ứng dụng dịch vụ của Viettel, bạn có cơ hội
                                trúng rất nhiều phần quà hấp dẫn bao gồm iPhone XS Max 256 GB, Apple Watch, Loa Blue-tooth,
                                quả bóng có chữ ký tuyển thủ Việt Nam và hàng triệu phần quà đáng yêu khác.
                            </i>
                        </p>
                        <div class="box-the-le">
                            <p class="no-indent"><b>1. Thời gian diễn ra chương trình</b></p>
                            <p>- Thời gian dự kiến chương trình từ ngày 28/01/2019 đến 05/03/2019</p>
                        </div>
                        <div class="box-the-le">
                            <p class="no-indent"><b>2. Đối tượng tham gia</b></p>
                            <p>
                                - Tất cả khách hàng là thuê bao di động trả trước, trả sau Viettel đang hoạt động 02 chiều
                                tại thời điểm tham gia chơi và thời điểm nhận quà
                            </p>
                            <p>- Chương trình không áp dụng cho thuê bao Dcom, Homephone</p>
                        </div>
                        <div class="box-the-le">
                            <p class="no-indent"><b>3. Cơ cấu quà tặng</b></p>
                            <p>
                                <table class="tbl-giai-thuong">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Quà tặng</th>
                                        <th>Số lượng</th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td>1</td>
                                        <td>Quà tặng hiện vật</td>
                                        <td>10.230</td>
                                    </tr>
                                    <tr>
                                        <td>1.1</td>
                                        <td>Vé xem phim CGV</td>
                                        <td>10.000</td>
                                    </tr>
                                    <tr>
                                        <td>1.2</td>
                                        <td>Apple Watch Series 4 GPS (44mm)</td>
                                        <td>19</td>
                                    </tr>
                                    <tr>
                                        <td>1.3</td>
                                        <td>Iphone XS MAX 256GB</td>
                                        <td>19</td>
                                    </tr>
                                    <tr>
                                        <td>1.4</td>
                                        <td>Loa di động BLUETOOTH® XB01 EXTRA BASS™</td>
                                        <td>190</td>
                                    </tr>
                                    <tr>
                                        <td>1.5</td>
                                        <td>Quả bóng có chữ kí tuyển thủ Việt Nam</td>
                                        <td>1</td>
                                    </tr>
                                    <tr>
                                        <td>1.6</td>
                                        <td>Giải đặc biệt (Apple watch + Iphone XS MAX 256 GB) </td>
                                        <td>1</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td><b><i>Quà tặng Viễn thông</i></b></td>
                                        <td><b>3.200.000</b></td>
                                    </tr>
                                    <tr>
                                        <td>2.1</td>
                                        <td>6 SMS nội mạng</td>
                                        <td>1.000.000</td>
                                    </tr>
                                    <tr>
                                        <td>2.2</td>
                                        <td>19 SMS nội mạng</td>
                                        <td>100.000</td>
                                    </tr>
                                    <tr>
                                        <td>2.3</td>
                                        <td>19 phút gọi nội mạng</td>
                                        <td>100.000</td>
                                    </tr>
                                    <tr>
                                        <td>2.4</td>
                                        <td>1GB DATA</td>
                                        <td>1.000.000</td>
                                    </tr>
                                    <tr>
                                        <td>2.5</td>
                                        <td>1 tuần sử dụng DV MyClip</td>
                                        <td>250.000</td>
                                    </tr>
                                    <tr>
                                        <td>2.6</td>
                                        <td>1 tuần sử dụng DV 5Dmax</td>
                                        <td>250.000</td>
                                    </tr>
                                    <tr>
                                        <td>2.7</td>
                                        <td>1 tuần sử dụng DV MoCha</td>
                                        <td>250.000</td>
                                    </tr>
                                    <tr>
                                        <td>2.8</td>
                                        <td>1 tuần sử dụng DV Keeng</td>
                                        <td>250.000</td>
                                    </tr>
                                </table>
                            </p>
                        </div>
                        <div class="box-the-le">
                            <p class="no-indent"><b>4. Nội dung chương trình</b></p>
                            <p>
                                <i class="fas fa-arrows-alt"></i> Trên các ứng dụng/Wap/Web của các trang, dịch vụ của Viettel bao gồm My Viettel, Viettel Portal, Shop Viettel, Mocha, Keeng, MyClip, 5Dmax, bạn sẽ thấy banner của game hoặc khi vào menu của từng ứng dụng/wap/ web này bạn sẽ thấy mục game “Heo vàng may mắn” và có thể click vào mục này để tham gia chơi.
                            </p>
                            <div>
                                <p><b><i><i class="fas fa-arrows-alt"></i> Các bước tham gia chơi Game như sau:</i></b></p>
                                <p>+ Bước 1: Truy cập App/Wap/web của các trang, dịch vụ sau đều có thể tham gia: My Viettel, Viettel Portal, Shop Viettel, Mocha, Keeng, MyClip, 5Dmax.</p>
                                <p>+ Bước 2: Đăng nhập vào game.</p>
                                <p>+ Bước 3: Bắt đầu chơi: Khi bạn nhấn chơi, giao diện sẽ cho phép khách hàng bắt các con heo (vàng, bạc, kim cương), các chướng ngại vật hoặc không bắt được gì.</p>
                                <p>+ Khi bắt được heo, bạn sẽ đập heo để trúng các phần quà. Thông báo quà tặng sẽ hiện ở popup trên giao diện game và SMS gửi về điện thoại.</p>
                            </div>
                            <div>
                                <p><b><i><i class="fas fa-arrows-alt"></i> Quy định về lượt chơi:</i></b></p>
                                <p>- Mỗi ngày, Viettel tặng bạn 3 lượt chơi miễn phí sử dụng đến hết 24h ngày hôm đó, không cộng dồn vào ngày kế tiếp trong trường hợp không sử dụng hết.</p>
                                <p>- Để thêm lượt chơi, bạn có thể hoàn thành các nhiệm vụ ở mục “thêm lượt”. Mỗi nhiệm vụ tương ứng với số lượt chơi được cộng thêm được quy định rõ. Lượt chơi được thêm khi hoàn thành nhiệm vụ được sử dụng đến hết thời gian diễn ra chương trình.</p>
                            </div>
                            <div>
                                <p><b><i><i class="fas fa-arrows-alt"></i> Quy định về thời hạn sử dụng quà tặng:</i></b></p>
                                <p>- Quà tặng là SMS nội mạng: Thời hạn sử dụng 7 ngày kể từ thời điểm trúng quà.</p>
                                <p>- Quà tặng là phút gọi nội mạng: Thời hạn sử dụng 7 ngày kể từ thời điểm trúng quà.</p>
                                <p>- Quà tặng là 1GB Data: Thời hạn sử dụng 30 ngày kể từ thời điểm trúng quà.</p>
                                <p>- Quà tặng là Video Xuân: Một tuần sử dụng miễn phí dịch vụ Mocha/Keeng/MyClip/ 5Dmax.</p>
                                <p>- Quà tặng là voucher vé xem phim CGV: Sử dụng đến 24h ngày 30/06/2019.</p>
                            </div>
                            <div>
                                <p><b><i><i class="fas fa-arrows-alt"></i> Thông báo trúng quà:</i></b></p>
                                <p>- Bạn sẽ nhận được thông báo nhận được quà tặng trên game và tin nhắn thông báo, hướng dẫn cách thức nhận quà trong vòng 24 giờ, kể từ thời điểm bạn trúng quà khi chơi Game.</p>
                                <p class="indent-2">+ Quà tặng là SMS, phút gọi nội mạng, data, voucher vé xem phim: Viettel thông báo qua popup trên Game và qua tin nhắn SMS.</p>
                                <p class="indent-2">+ Quà tặng hiện vật iPhone Xs Max, Loa Bluetooth, Apple Watch, quả bóng có chữ kí tuyển thủ Việt Nam: Viettel thông báo qua popup trên Game và qua tin nhắn SMS. Nhân viên của Viettel sẽ liên hệ với bạn để làm các thủ tục nhận quà. Bạn có trách nhiệm cung cấp các thông tin, giấy tờ cá nhân (chứng minh thư, hộ khẩu), nộp thuế theo quy định của Nhà nước để nhận quà. Danh sách khách hàng nhận được quà là iPhone Xs Max và Apple Watch sẽ được công bố trên website vietteltelecom.vn.</p>
                            </div>
                            <div>
                                <p><b><i><i class="fas fa-arrows-alt"></i> Thời gian địa điểm, cách thức và thủ tục trao quà</i></b></p>
                                <p>- Quà tặng là Video Xuân, tin nhắn nội mạng, phút gọi nội mạng, data: hệ thống sẽ tự động cộng tài khoản/giảm trừ cước sử dụng cho thuê bao của bạn.</p>
                                <p class="indent-2">+ Thuê bao di động trả trước: cộng trực tiếp vào tài khoản của thuê bao.</p>
                                <p class="indent-2">+ Thuê bao di động trả sau: Giảm trừ vào hóa đơn cước tháng 2/2019 và tháng 03/2019 tương ứng với thời điểm nhận được quà.</p>
                                <p>- Quà tặng là voucher vé xem phim: Viettel gửi mã voucher qua SMS đến số thuê bao trúng quà.</p>
                                <p>- Quà tặng là Iphone XS Max, Loa, Đồng hồ, quả bóng có chữ kí tuyển thủ Việt Nam: Sẽ được trao cho bạn trong vòng 45 ngày kể từ khi kết thúc chương trình.</p>
                                <p>- Tổng công ty Viễn thông Viettel liên hệ với khách hàng nhận được quà, hướng dẫn khách hàng các thủ tục nhận quà và tổ chức trao quà. </p>
                                <p class="indent-2">+ Thủ tục cần có để nhận quà: Người nhận được quà là hiện vật phải là chính chủ thuê bao: Khi nhận quà chủ thuê bao phải xuất trình CMND/Hộ chiếu còn hiệu lực theo quy định (CMND không quá 15 năm kể từ ngày được cấp, hộ chiếu phải còn hiệu lực trong vòng 06 tháng tính đến thời điểm nhận quà hoặc các giấy tờ khác thay thế nhưng phải có ảnh và có xác nhận của chính quyền địa phương trong vòng 06 tháng tính đến thời điểm nhận quà). Nếu ủy quyền cho người khác nhận quà thì người đi nhận phải có CMND bản photo và bản gốc của chính chủ trúng quà, CMND bản photo và bản gốc của người được ủy quyền, giấy ủy quyền đi nhận giải quà của người trúng quà. Trường hợp người trúng quà đặc biệt hiện vật không phải là chính chủ thuê bao: Khi nhận, người nhận được quà phải xuất trình CMND, cung cấp ít nhất 05 số điện thoại thường xuyên liên lạc trong vòng 06 tháng gần nhất.</p>
                                <p class="indent-2">+ Quà tặng sẽ được trao tại địa chỉ của khách hàng hoặc tại các cửa hàng Viettel tỉnh/TP.</p>
                                <p class="indent-2">+ Trong trường hợp quá 30 ngày kể từ ngày kết thúc chương trình mà Tổng Công ty Viễn thông Viettel không thể liên lạc được với khách hàng hoặc khách hàng không liên hệ với Tổng Công ty Viễn thông Viettel để làm thủ tục nhận quà thì quà tặng sẽ không được trao.</p>
                            </div>
                            <div>
                                <p><b><i><i class="fas fa-arrows-alt"></i> Các quy định khác:</i></b></p>
                                <p>- Khách hàng chơi game “Heo vàng may mắn” vẫn được hưởng quyền lợi từ các chương trình khuyến mại khác của Tổng Công ty Viễn thông Viettel (nếu đủ điều kiện).</p>
                                <p>- Tổng Công ty Viễn thông Viettel hoàn toàn chịu trách nhiệm trong việc quản lý tính chính xác của bằng chứng xác định trúng quà và đưa bằng chứng xác định trúng quà vào lưu thông trong chương trình khuyến mại. Trường hợp bằng chứng xác định trúng quà của Tổng Công ty phát hành có sai sót gây hiểu nhầm cho khách hàng trong việc trúng quà, Tổng Công ty Viễn thông Viettel có trách nhiệm trao các quà này cho khách hàng trúng quà. Việc tổ chức chương trình khuyến mại phải đảm bảo tính khách quan và công khai.</p>
                                <p>- Tổng Công ty Viễn thông Viettel được sử dụng tên và hình ảnh của khách hàng trúng quà cho mục đích quảng cáo thương mại.</p>
                                <p>- Trong trường hợp xảy ra tranh chấp liên quan đến chương trình khuyến mại này, Tổng Công ty Viễn thông Viettel có trách nhiệm trực tiếp giải quyết, nếu không thỏa thuận được tranh chấp sẽ được xử lý theo quy định của pháp luật.</p>
                                <p>- Nếu người chơi có dấu hiệu gian lận hoặc quấy rối, Ban Tổ chức có thể đưa vào danh sách đen không được tiếp tục chơi game “Heo vàng may mắn” trên các ứng dụng dịch vụ của Viettel.</p>
                                <p>- Quyết định của Ban Tổ chức (Viettel) là quyết định cuối cùng. Chi tiết liên hệ 198 (Miễn phí).</p>
                            </div>
                        </div>
                    </div>
                    </body>
                </div>
            );
    }
}

export default TheLeGameTet;
