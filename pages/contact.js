import React, {Component} from "react";
import Link from 'next/link';
import {DOMAIN_IMAGE_STATIC} from "../src/config/Config.js";

const logo_mocha = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_mocha.png";
const logo_viettel = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_viettel.png";
const logo_congthuong = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_congthuong.png";
const contact = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/contact.png";
const icon_email = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/icon-email.png";
const icon_face = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/icon-face.png";
const icon_mocha = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/icon-mocha.png";

export class Contact extends Component {

    render() {
        return (
            <div id="wrapper">
                <style dangerouslySetInnerHTML={{__html: "\n  #footer { margin-top: 0;}"}}/>
                <div className="header">
                    <div className="header-top">
                        <Link href="/" >
                            <a style={{float: 'left', marginTop: '5px'}}>
                                <img height="35" src={logo_mocha}/>
                            </a>
                        </Link>
                        <Link  href="/contact" as="/contact.html" >
                            <a className="top-menu active">LIÊN HỆ</a>
                        </Link>
                        <Link  href="/faq" as="/faq.html">
                            <a className="top-menu">HỎI & ĐÁP</a>
                        </Link>
                        <Link  href="/gioithieuapp" as="/gioithieuapp.html">
                            <a className="top-menu">TÍNH NĂNG</a>
                        </Link>
                    </div>
                </div>
                <div id="" style={{background: '#e0d2f9 none repeat scroll 0 0', paddingBottom: '20px'}}>
                    {/*<div className="mocha-guide mocha-faq" style={{paddingTop:'7px'}}>*/}
                    <p className="notice-mocha"
                       style={{
                           width: '100%',
                           color: '#FFF',
                           textAlign: 'center',
                           zIndex: 9999,
                           fontSize: '14px',
                           fontFamily: 'arial',
                           padding: '7px 0px',
                           top: '45px'
                       }}>
                        <a href="http://mocha.com.vn/callout.html" style={{color: 'rgb(254, 0, 0)'}}>THÔNG BÁO: Hiện
                            nay,
                            khách hàng dùng các gói cước Sinh viên của Viettel được ưu đãi MIỄN PHÍ GỌI NỘI MẠNG khi
                            dùng Mocha. Bấm để xem thêm! </a></p>
                    <div className="mocha-contact">
                        <div className="mocha-contact-letft">
                            <p className="mocha-guide-p mocha-contact-p">Liên hệ</p>
                            <p className="mocha-contact-p1">Viettel trân trọng cảm ơn mọi ý kiến phản hồi, đóng góp
                                của Quý khách giúp Mocha ngày càng hoàn thiện hơn.</p>
                            <p className="mocha-contact-p1">Để được hỗ trợ sử dụng sản phẩm hoặc gửi ý kiến đóng
                                góp, Quý khách có thể liên hệ trực tiếp qua các kênh sau:</p>
                        </div>
                        <div className="mocha-contact-right">
                            <img src={contact}/>
                        </div>
                        <div className="mocha-contact-list">
                            <div className="mocha-contact-item">
                                <div className="mocha-contact-item-left">
                                    <img src={icon_email}/>
                                </div>
                                <div className="mocha-contact-item-right">
                                    <p className="mocha-contact-item-p"> Góp ý & hỗ trợ </p>
                                    <a className="mocha-contact-item-a"
                                       href="mailto:mocha@viettel.com.vn">mocha@viettel.com.vn</a>
                                </div>
                            </div>
                            <div className="mocha-contact-item">
                                <div className="mocha-contact-item-left">
                                    <img src={icon_face}/>
                                </div>
                                <div className="mocha-contact-item-right">
                                    <p className="mocha-contact-item-p"> Facebook Mocha </p>
                                    <a className="mocha-contact-item-a"
                                       href="https://www.facebook.com/mocha.com.vn">https://www.facebook.com/mocha.com.vn </a>
                                </div>
                            </div>
                            <div className="mocha-contact-item">
                                <div className="mocha-contact-item-left">
                                    <img src={icon_mocha}/>
                                </div>
                                <div className="mocha-contact-item-right">
                                    <p className="mocha-contact-item-p"> Đường dây nóng </p>
                                    <p className="mocha-contact-item-p1">18008098 (Miễn phí)</p>
                                </div>
                            </div>
                        </div>
                        <div className="mocha-input-msisdn">
                        </div>
                    </div>

                    {/*</div>*/}
                </div>

                <div id="footer">
                    <div className="mocha-footer">
                        <Link href="/" >
                            <a className="logo-mocha" ></a>
                        </Link>
                        <p className="provision-intro">
                            Cơ quan chủ quản: Viettel Telecom - Chăm sóc khách hàng 18008098 (Miễn phí)
                            <br/>
                            <Link  href="/provision" as="/provision.html">
                                <a className="provision">Điều khoản sử dụng</a>
                            </Link>
                            |
                            <Link  href="/guide" as="/guide.html">
                                <a className="provision">Hướng dẫn sử dụng</a>
                            </Link>
                        </p>
                        <a href="#" className="logo-viettel">
                            <img src={logo_viettel}/>
                        </a>
                        <a href="http://online.gov.vn/HomePage.aspx" className="logo-viettel logo-congthuong">
                            <img src={logo_congthuong}/>
                        </a>
                    </div>
                </div>

            </div>

        );
    }

}

export default Contact;
