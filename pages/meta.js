import React  from 'react';
import Head from 'next/head';
import {DOMAIN_IMAGE_STATIC} from "../src/config/Config.js";
const clip_logo = DOMAIN_IMAGE_STATIC + "clip_logo1.png";

class MetaSeo extends React.Component {
    render() {
        return (
            <Head>
                <link rel="canonical" href="http://mocha.com.vn/" />
                <meta name="description" content="Cập nhật trending, phim ngắn hot nhất, video hài đặc sắc cùng các thông tin nóng nhất về giải trí, thể thao, âm nhạc, xã hội Việt Nam - Quốc Tế..." />

                {/* META FOR FACEBOOK */}
                <meta property="og:site_name" content="Mocha video - kho nội dung clip cực Hot" />
                <meta property="og:url" itemprop="url" content="http://mocha.com.vn/" />
                <meta property="og:image" itemprop="thumbnailUrl"
                      content={clip_logo} />
                <meta content="Mocha video - Mạng xã hội chia sẻ video" itemprop="headline" property="og:title" />
                <meta
                    content="Cập nhật trending, phim ngắn hot nhất, video hài đặc sắc cùng các thông tin nóng nhất về giải trí, thể thao, âm nhạc, xã hội Việt Nam - Quốc Tế..."
                    itemprop="description" property="og:description" />
                {/* END META FOR FACEBOOK */}

                {/* Twitter Card */}
                <meta name="twitter:url" content="http://mocha.com.vn/" />
                <meta name="twitter:title" content="Mocha video - kho nội dung clip cực Hot" />
                <meta name="twitter:description"
                      content="Cập nhật trending, phim ngắn hot nhất, video hài đặc sắc cùng các thông tin nóng nhất về giải trí, thể thao, âm nhạc, xã hội Việt Nam - Quốc Tế..." />
                <meta name="twitter:image" content="http://video.mocha.com.vn/images/clip_logo1.png" />
                {/* End Twitter Card */}

            </Head>
        );
    }
}

export default MetaSeo;
