import React from 'react';
import ErrorBoundary from "../src/components/ErrorBoundary";
import Helper from "../src/utils/helpers/Helper";
import CategoryList from "../src/components/Category/CategoryList";
import CateApi from "../src/services/api/Category/CategoryApi";

import LoadingScreen from "../src/components/Global/Loading/LoadingScreen";
import {withRouter} from 'next/router';
import CircularJSON from "circular-json";
//import Error from 'next/error';
import Error from "../src/components/Error";
import NoSSR from 'react-no-ssr';
import Metaseo from "./metaseo";


var _isMounted = false;
class Category extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: false,
            isLoaded: false,
            isOnTop: false,

            videoNew: [],
            channelHot: [],
            videoHot: [],
            idCate: 0,
            cateInfo: null,
            lastIdStr: ''
        };
    }

    static async getInitialProps({req, query, asPath}) {
        var props = {
            error: false,
            isServer: false,
            isLoaded: false,

            videoNew: [],
            channelHot: [],
            videoHot: [],
            idCate: 0,
            cateInfo: null,
            path: asPath
        };

        if (req) {
            _isMounted = true;
            const cookie = req.headers.cookie;
            props.isServer = true;
            props.idCate = parseInt(query.id);
            var lastIdStr = '';
            if (props.idCate) {
                await Promise.all([
                    CateApi.getVideoAllServer(props.idCate, 0, lastIdStr, cookie),
                    CateApi.getVideoHotServer(props.idCate, cookie),
                    CateApi.getChannelHotServer(props.idCate, cookie)
                ]).then((responses) => {
                    if (responses) {
                        props.isLoaded = true;
                        let resVideoAll = JSON.parse(CircularJSON.stringify(responses))[0];
                        let resVideoHot = JSON.parse(CircularJSON.stringify(responses))[1];
                        let resChannelHot = JSON.parse(CircularJSON.stringify(responses))[2];
                        props.videoNew = Helper.checkArrNotEmpty(resVideoAll.data, 'data.listVideo') ? resVideoAll.data.data.listVideo : [];
                        props.cateInfo = resVideoAll.data.data.cateInfo;
                        props.videoHot = Helper.checkObjExist(resVideoHot.data, 'data') ? resVideoHot.data.data : [];
                        props.channelHot = Helper.checkObjExist(resChannelHot.data, 'data.listChannel') ? resChannelHot.data.data.listChannel : [];
                    }
                }).catch((error) => {
                    props.error = error.response ? error.response.status : true;
                });
            }
        }

        return {initialProps: props};
    }

    componentDidMount() {
        if (!this.props.initialProps.isServer) {
            window.addEventListener('scroll', this.handleScroll);
            if (window.pageYOffset === 0 && !this.state.isOnTop) {
                this.setState({
                    isOnTop: true
                });
            }
            let idCate = this.props.router.asPath ? parseInt(Helper.getIdByLink(this.props.router.asPath, '-cg')) : 0;

            this.fetchData(idCate);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (!this.props.initialProps.isServer) {
            if (parseInt(Helper.getIdByLink(this.props.router.asPath, '-cg')) !== parseInt(this.state.idCate)) {
                this.fetchData(parseInt(Helper.getIdByLink(this.props.router.asPath, '-cg')));
            }
        }
    }

    componentWillUnmount() {
        _isMounted = false;
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = () => {
        if (window.pageYOffset === 0 && !this.state.isOnTop) {
            this.setState({
                isOnTop: true
            });
        }
    }

    fetchData = (idCate) => {
        _isMounted = true;

        const {lastIdStr} = this.state;
        var self = this;
        let isTop = (window.pageYOffset === 0);

        if (idCate) {
            this.setState({
                isLoaded: false,
                isOnTop: isTop,
                idCate: idCate
            }, () => {
                try {
                    CateApi.getVideoAll(idCate, 0, lastIdStr).then(
                        (response) => {

                            if (response && response.data) {
                                Helper.renewToken(response.data);
                                if (_isMounted && Helper.checkObjExist(response.data, 'data.cateInfo')) {
                                    self.setState({
                                        isLoaded: true,
                                        videoNew: Helper.checkArrNotEmpty(response.data, 'data.listVideo') ? response.data.data.listVideo : [],
                                        cateInfo: response.data.data.cateInfo
                                    });
                                } else {
                                    this.setState({
                                        error: true
                                    });
                                }
                            }
                        }
                    );

                    CateApi.getVideoHot(idCate).then(
                        (response) => {
                            if (response && response.data) {
                                Helper.renewToken(response.data);
                                if (_isMounted && response.data) {
                                    self.setState({
                                        videoHot: Helper.checkObjExist(response.data, 'data') ? response.data.data : []
                                    });
                                } else {
                                    this.setState({
                                        error: true
                                    });
                                }
                            }
                        }
                    );

                    CateApi.getChannelHot(idCate).then(
                        (response) => {
                            if (response && response.data) {
                                Helper.renewToken(response.data);
                                if (_isMounted && response.data) {
                                    self.setState({
                                        channelHot: Helper.checkObjExist(response.data, 'data.listChannel') ? response.data.data.listChannel : []
                                    });
                                } else {
                                    this.setState({
                                        error: true
                                    });
                                }
                            }
                        }
                    );
                } catch (error) {
                    this.setState({
                        error: error.response ? error.response.status : true
                    })
                }

            });
        }
    }

    render() {

        var initialProps = this.props.initialProps;
        var isServer = initialProps.isServer;
        const isLoaded = isServer ? initialProps.isLoaded : this.state.isLoaded;
        const isOnTop = isServer ? true : this.state.isOnTop;
        const videoNew = isServer ? initialProps.videoNew : this.state.videoNew;
        const idCate = isServer ? initialProps.idCate : this.state.idCate;
        const videoHot = isServer ? initialProps.videoHot : this.state.videoHot;
        const channelHot = isServer ? initialProps.channelHot : this.state.channelHot;
        const cateInfo = isServer ? initialProps.cateInfo : this.state.cateInfo;
        const isError = isServer ? initialProps.error : this.state.error;

        if (isError) {
            return <Error/>
        }
        if (!isLoaded || !isOnTop) {
            return <LoadingScreen/>;
        } else {//Chuyên mục thường
            let meta = {
                title: cateInfo.categoryname,
                image: cateInfo.url_avatar,
                description: cateInfo ? cateInfo.description : '',
                url: '',
                type: 'category',
                path: initialProps.path
            };
            return (
                <>
                    <Metaseo type="category" data={meta}/>
                    <ErrorBoundary>
                        <NoSSR onSSR={<LoadingScreen/>}>
                            <section>
                            </section>
                        </NoSSR>
                        <CategoryList cateInfo={cateInfo} video={Helper.checkIsChannelNotShowVideo(videoNew)}
                                      videoHot={Helper.checkIsChannelNotShowVideo(videoHot)}
                                      channelHot={channelHot}
                                      idSearch={idCate}/>
                    </ErrorBoundary>
                </>
            );

        }
    }
}

export default withRouter(Category);
