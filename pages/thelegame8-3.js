import React, {Component} from 'react';
import Link from "next/link";
import Loading from "../src/components/Global/Loading/LoadingScreen";
import {DOMAIN_IMAGE_STATIC} from "../src/config/Config";

 class Thelegame83 extends Component {
    constructor(props) {
        super(props);
    }
    render() {
            return (
                <div>
                    <head>
                        <meta charSet="utf-8"/>
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                            <title>Thể lệ chương trình quay số</title>
                    </head>
                    <body className="content-thele-game">
                    <div className="header">
                        <img
                            src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/the-le-game.png"
                            title="" alt=""/>
                    </div>
                    <div className="box-content-1" style={{marginTop:'100px'}}>
                        <p>
                            <i>
                                Khi tham gia chơi Game "Trao yêu thương - Trúng kim cương"
                                trên các ứng dụng dịch vụ của Viettel,
                                bạn có cơ hội trúng rất nhiều phần quà giá trị
                                bao gồm bộ trang sức kim cương vàng trắng 14k,
                                bộ trang sức ngọc trai vàng trắng 14k,
                                vé xem phim CGV và hàng triệu phần quà hấp dẫn khác
                            </i>
                        </p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>1. Tên thương nhân thực hiện chương trình khuyến mại</b></p>
                        <p>- Tổng Công Ty Viễn thông Viettel – Chi nhánh Tập đoàn Viễn thông Quân đội.</p>
                        <p>- Số 01, Đường Giang Văn Minh, Quận Ba Đình, Hà Nội.</p>
                        <p>- Điện thoại: 024.62660434.</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>2. Tên chương trình khuyến mại: </b>TRAO YÊU THƯƠNG- TRÚNG KIM
                            CƯƠNG.</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>3. Thời gian khuyến mại</b></p>
                        <p>- Trong 26 ngày từ ngày 06/03/2019 đến 31/03/2019.</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>4. Địa bàn (phạm vi) khuyến mại: </b>Toàn quốc.</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>5. Hình thức khuyến mại: </b>Chương trình khuyến mại may mắn.</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>6. Đối tượng tham gia</b></p>
                        <p>- Toàn bộ khách hàng là thuê bao Viettel di động trả trước, trả sau đang hoạt động 02 chiều
                            tại thời điểm tham gia chơi và thời điểm nhận quà.</p>
                        <p><b>Lưu ý</b>: Chương trình không áp dụng cho thuê bao Dcom, Homephone.</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>7. Cơ cấu quà tặng</b></p>
                        <table className="table" style={{fontSize:'11px', textAlign:'left'}}>
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Quà tặng</th>
                                <th colSpan="2">Đơn vị tính</th>
                                <th>Số lượng</th>
                                <th>Đơn giá</th>
                                <th>Thành Tiền</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td><b>Quà tặng hiện vật</b></td>
                                <td colSpan="2"></td>
                                <td>8.310</td>
                                <td></td>
                                <td>1.148.800.000</td>
                            </tr>
                            <tr>
                                <td>1.1</td>
                                <td>Bộ trang sức kim cương vàng trắng 14k (nhẫn, khuyên tai, vòng cổ)</td>
                                <td colSpan="2">Bộ</td>
                                <td>1</td>
                                <td>209.000.000</td>
                                <td>209.000.000</td>
                            </tr>
                            <tr>
                                <td>1.2</td>
                                <td>Bộ trang sức ngọc trai vàng trắng 14k (nhẫn, khuyên tai, vòng cổ)</td>
                                <td colSpan="2">Bộ</td>
                                <td>3</td>
                                <td>28.600.000</td>
                                <td>85.800.000</td>
                            </tr>
                            <tr>
                                <td rowSpan="2">1.3</td>
                                <td rowSpan="2">Quà tặng 5 người chơi top điểm may mắn</td>
                                <td>Top 1</td>
                                <td>2 chỉ vàng</td>
                                <td>2</td>
                                <td>4.000.000</td>
                                <td>8.000.000</td>
                            </tr>
                            <tr>
                                <td>Top 2-5</td>
                                <td>1 chỉ vàng</td>
                                <td>4</td>
                                <td>4.000.000</td>
                                <td>16.000.000</td>
                            </tr>
                            <tr>
                                <td>1.4</td>
                                <td>Voucher vé xem phim CGV</td>
                                <td colSpan="2">Quà</td>
                                <td>8.300</td>
                                <td>100.000</td>
                                <td>830.000.000</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><b><i>Quà tặng Viễn thông</i></b></td>
                                <td colSpan="2">Quà</td>
                                <td><b>8.291.690</b></td>
                                <td></td>
                                <td>4.597.722.248</td>
                            </tr>
                            <tr>
                                <td>2.1</td>
                                <td>3 phút gọi nội mạng</td>
                                <td colSpan="2">Quà</td>
                                <td>2.000.000</td>
                                <td>163,5</td>
                                <td>327.000.000</td>
                            </tr>
                            <tr>
                                <td>2.2</td>
                                <td>8 SMS nội mạng</td>
                                <td colSpan="2">Quà</td>
                                <td>5.381.690</td>
                                <td>39,2</td>
                                <td>210.962.248</td>
                            </tr>
                            <tr>
                                <td>2.3</td>
                                <td>83MB Data</td>
                                <td colSpan="2">Quà</td>
                                <td>100.000</td>
                                <td>298.8</td>
                                <td>29.880.000</td>
                            </tr>
                            <tr>
                                <td>2.4</td>
                                <td>830MB DATA</td>
                                <td colSpan="2"><b>Quà</b></td>
                                <td>10.000</td>
                                <td>2.988</td>
                                <td>29.880.000</td>
                            </tr>
                            <tr>
                                <td>2.5</td>
                                <td>1 tuần sử dụng DV MyClip</td>
                                <td colSpan="2"></td>
                                <td><b>200.000</b></td>
                                <td>5.000</td>
                                <td>1.000.000.000</td>
                            </tr>
                            <tr>
                                <td>2.6</td>
                                <td>1 tuần sử dụng DV 5Dmax</td>
                                <td colSpan="2"></td>
                                <td><b>200.000</b></td>
                                <td>5.000</td>
                                <td>1.000.000.000</td>
                            </tr>
                            <tr>
                                <td>2.7</td>
                                <td>1 tuần sử dụng DV MoCha</td>
                                <td colSpan="2"></td>
                                <td><b>200.000</b></td>
                                <td>5.000</td>
                                <td>1.000.000.000</td>
                            </tr>
                            <tr>
                                <td>2.8</td>
                                <td>1 tuần sử dụng DV Keeng</td>
                                <td colSpan="2"></td>
                                <td><b>200.000</b></td>
                                <td>5.000</td>
                                <td>1.000.000.000</td>
                            </tr>
                            <tr>
                                <td><b>3</b></td>
                                <td><b>Quà tặng may mắn</b></td>
                                <td colSpan="2"><b>Quà</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>3.1</td>
                                <td>Điểm may mắn</td>
                                <td colSpan="2">Quà</td>
                                <td><b>Không giới hạn</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>TỔNG</td>
                                <td colSpan="2"></td>
                                <td>0</td>
                                <td></td>
                                <td>5.746.522.248</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent">
                            <center><b>Bằng chữ: Năm tỉ, bảy trăm bốn mươi sáu triệu, năm trăm hai mươi hai ngàn, hai
                                trăm bốn mươi tám đồng./.</b></center>
                        </p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b style={{textDecoration:'underline'}}>Ghi chú:</b></p>
                        <p>- Giá trị quà tặng bao gồm thuế VAT.</p>
                        <p>- Quà tặng không có giá trị quy đổi thành tiền mặt.</p>
                        <p>- Tổng doanh thu bán các gói dịch vụ Data, thoại, SMS, dịch vụ Giá trị gia tăng trên các hệ
                            thống dịch vụ triển khai Game ~ 40 tỷ đồng. Tổng giá trị quà tặng/tổng hàng hóa khuyến mại ~
                            14 %.</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>8. Nội dung và thể lệ chi tiết chương trình</b></p>
                        <div className="WordSection1 WordSection">
                            <p className="header-content">
                                <b><i>8.1. Điều kiện, cách thức, thủ tục cụ thể khách hàng phải thực hiện để được tham
                                    gia chương trình khuyến mại: </i></b>
                            </p>
                            <p className="content2">
                                <ul>
                                    <li>
                                        <span className="list-style" style={{fontFamily:'Wingdings'}}>v
                                            <span style={{font:"7.0pt Times New Roman"}}/>
                                        </span>
                                        Điều kiện tham gia chương trình:
                                        <div>Khách hàng di động Viettel truy cập các App/Wap/web của các trang, dịch vụ
                                            sau đều có thể tham gia:</div>
                                        <ul className="content3">
                                            <li>- MyViettel</li>
                                            <li>- Viettel Portal</li>
                                            <li>- ShopViettel</li>
                                            <li>- Mocha</li>
                                            <li>- Mocha</li>
                                            <li>- MyClip</li>
                                            <li>- 5Dmax</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span className="list-style"  style={{fontFamily:'Wingdings'}}>v<span
                                            style={{font:"7.0pt Times New Roman"}}></span></span> Cách thức và thủ tục
                                        tham gia chương trình: Khách hàng thực hiện các bước như sau:
                                        <div><b>Bước 1:</b> Khách hàng vào App/Wap/web các Dịch vụ sau đều có thể tham
                                            gia</div>
                                        <ul className="content3">
                                            <li>- MyViettel</li>
                                            <li>- Viettel Portal</li>
                                            <li>- ShopViettel</li>
                                            <li>- Mocha</li>
                                            <li>- Mocha</li>
                                            <li>- MyClip</li>
                                            <li>- 5Dmax</li>
                                        </ul>
                                        <div><b>Bước 2:</b> Đăng nhập để tham gia chơi</div>
                                        <div><b>Bước 3:</b> Bắt đầu chơi</div>
                                        <div>- Khách hàng ấn “Bắt đầu” để chơi và sẽ nhận được các phần quà ứng với mỗi
                                            lượt chơi, hệ thống sẽ Pop up thông báo quà trên Game chơi và gửi SMS thông
                                            báo. Các loại quà tặng như sau:
                                        </div>
                                        <table className="table" style={{fontSize:'11px', textAlign:'left'}}>
                                            <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th colSpan="2">Quà tặng</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th>1</th>
                                                <th colSpan="2"><b><i>Quà tặng hiện vật</i></b></th>
                                            </tr>
                                            <tr>
                                                <th>1.1</th>
                                                <th colSpan="2">Bộ trang sức kim cương vàng trắng 14k (nhẫn, khuyên tai,
                                                    vòng cổ)
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>1.2</th>
                                                <th colSpan="2">Bộ trang sức ngọc trai vàng trắng 14k (nhẫn, khuyên tai,
                                                    vòng cổ)
                                                </th>
                                            </tr>
                                            <tr>
                                                <th rowSpan="2">1.3</th>
                                                <th rowSpan="2">Quà tặng 5 người chơi đạt nhiều điểm may mắn nhất</th>
                                                <th>Top 1: 02 chỉ vàng</th>
                                            </tr>
                                            <tr>
                                                <th>Top 1: 02 chỉ vàng</th>
                                            </tr>
                                            <tr>
                                                <th>1.4</th>
                                                <th colSpan="2">Voucher vé xem phim CGV</th>
                                            </tr>
                                            <tr>
                                                <th>2</th>
                                                <th colSpan="2"><b><i>Quà tặng Viễn thông</i></b></th>
                                            </tr>
                                            <tr>
                                                <th>2.1</th>
                                                <th colSpan="2">3 phút gọi nội mạng</th>
                                            </tr>
                                            <tr>
                                                <th>2.2</th>
                                                <th colSpan="2">8 SMS nội mạng</th>
                                            </tr>
                                            <tr>
                                                <th>2.3</th>
                                                <th colSpan="2">83 MB data</th>
                                            </tr>
                                            <tr>
                                                <th>2.4</th>
                                                <th colSpan="2">830 MB data</th>
                                            </tr>
                                            <tr>
                                                <th>2.5</th>
                                                <th colSpan="2">1 tuần sử dụng DV MyClip</th>
                                            </tr>
                                            <tr>
                                                <th>2.6</th>
                                                <th colSpan="2">1 tuần sử dụng DV 5Dmax</th>
                                            </tr>
                                            <tr>
                                                <th>2.7</th>
                                                <th colSpan="2">1 tuần sử dụng DV Mocha</th>
                                            </tr>
                                            <tr>
                                                <th>2.8</th>
                                                <th colSpan="2">1 tuần sử dụng DV Keeng</th>
                                            </tr>
                                            <tr>
                                                <th><b>3</b></th>
                                                <th colSpan="2"><b>Quà tặng may mắn</b></th>
                                            </tr>
                                            <tr>
                                                <th>3.1</th>
                                                <th colSpan="2">Điểm may mắn lấy top 5 người chơi điểm cao nhất để trúng
                                                    quà mục 1.3
                                                </th>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div>- Khi khách hàng trúng quà, hệ thống Game hiển thị thông báo trúng quà,
                                            đồng thời hệ thống SMS gửi tin nhắn thông báo quà tặng.
                                        </div>
                                        <div>- Kịch bản tin nhắn dự kiến như sau:</div>
                                        <ul className="content3">
                                            <li>
                                                <b>+ Khách hàng trúng quà “điểm may mắn” -></b> Hệ thống hiển thị nội
                                                dung thông báo cộng điểm để có cơ hội trúng quà cho top người chơi điểm
                                                cao.
                                                <ul className="content4">
                                                    <li>* Nội dung hiển thị: Trúng quà may mắn rồi! Chúc mừng bạn đã
                                                        được cộng thêm 1 điểm may mắn để có cơ hội trúng quà cho người
                                                        chơi điểm cao. Chơi tiếp để tích lũy điểm nhé! Chi tiết xem
                                                        trong mục giải thưởng. Trân trọng.
                                                    </li>
                                                    <li>* Khi khách hàng nhận quà điểm may mắn”: Không gửi SMS cho khách
                                                        hàng.
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <b>+ Khách hàng trúng quà “thêm lượt” -> </b> Hệ thống hiển thị thông
                                                báo
                                                <ul className="content4">
                                                    <li>* Nội dung hiển thị: Chúc mừng quý khách đã nhận được thêm một
                                                        lượt chơi.
                                                    </li>
                                                    <li>* Nội dung SMS: [TB]: Chuc mung Quy khach da duoc tang 1 luot
                                                        choi Game Trao yeu thuong- Trung kim cuong voi hang trieu qua
                                                        tang hap dan. Moi Quy khach tiep tuc tham gia tro choi. Chi tiet
                                                        LH 198 (0d). Tran trong”.
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <b>+ Khách hàng trúng quà “8 SMS”:</b>
                                                <ul className="content4">
                                                    <li>* Nội dung hiển thị trên Game: Chúc mừng Quý khách đã nhận được
                                                        8 SMS nội mạng miễn phí.
                                                    </li>
                                                    <li>* Nội dung SMS: [TB]: Quy khach da duoc tang 8 SMS noi mang mien
                                                        phi trong CT Trao yeu thuong- trung kim cuong. Qua tang duoc su
                                                        dung trong 7 ngay. Chi tiet LH 198 (0d). Tran trong.”
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <b>+ Khách hàng trúng quà “3 phút thoại”:</b>
                                                <ul className="content4">
                                                    <li>* Nội dung hiển thị trên Game: Chúc mừng Quý khách đã nhận được
                                                        quà là 3 phút gọi nội mạng miễn phí.
                                                    </li>
                                                    <li>* Nội dung SMS: [TB]: Quy khach da duoc tang 3 phut goi noi mang
                                                        mien phi trong CT Trao yeu thuong- Trung kim cuong. Qua tang
                                                        duoc su dung trong 7 ngay. Chi tiet LH 198 (0d). Tran trong.”
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <b>- Khách hàng trúng quà “83 MB data”:</b>
                                                <ul className="content4">
                                                    <li>* Nội dung hiển thị trên Game: Chúc mừng Quý khách đã nhận được
                                                        quà là 83MB Data miễn phí.
                                                    </li>
                                                    <li>* Nội dung SMS: [TB]: Quy khach da duoc tang 83 MB Data trong CT
                                                        Trao yeu thuong- Trung kim cuong. Qua tang duoc su dung trong 7
                                                        ngay. Chi tiet LH 198 (0d). Tran trong.”
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <b>- Khách hàng trúng quà “830 MB data”</b>
                                                <ul className="content4">
                                                    <li>* Nội dung hiển thị trên Game: Chúc mừng Quý khách đã nhận được
                                                        quà là 830MB Data miễn phí.
                                                    </li>
                                                    <li>* Nội dung SMS: [TB]: Quy khach da duoc tang 830 MB Data trong
                                                        CT Trao yeu thuong- Trung kim cuong. Qua tang duoc su dung trong
                                                        7 ngay. Chi tiet LH 198 (0d). Tran trong.”
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <b>- Khách hàng trúng quà “Bộ trang sức kim cương vàng trắng 14k”</b>
                                                <ul className="content4">
                                                    <li>* Nội dung hiển thị trên Game: Chúc mừng quý khách đã nhận được
                                                        quà là 01 Bộ trang sức kim cương vàng trắng 14k.
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <i>Nội dung SMS: [TB]: Chuc mung Quy khach da trung qua 01 bo trang suc
                                                    kim cuong vang trang 14k. Qua tang se duoc trao sau khi chuong trinh
                                                    ket thuc. Viettel se lien he truc tiep de huong dan Quy khach thu
                                                    tuc nhan qua. Chi tiet LH 198 (0d). Tran trong.”</i>
                                            </li>
                                            <li>
                                                <b>- Khách hàng trúng quà “Bộ trang sức ngọc trai vàng trắng 14k”</b>
                                                <ul className="content4">
                                                    <li>* Nội dung hiển thị trên Game: Chúc mừng quý khách đã nhận được
                                                        quà là 01 Bộ trang sức ngọc trai vàng trắng 14k.
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <i>Nội dung SMS: [TB]: Chuc mung Quy khach da trung qua 01 bo trang suc
                                                    ngoc trai vang trang 14k. Qua tang se duoc trao sau khi chuong trinh
                                                    ket thuc. Viettel se lien he truc tiep de huong dan Quy khach thu
                                                    tuc nhan qua. Chi tiet LH 198 (0d). Tran trong.”</i>
                                            </li>
                                            <li>
                                                <b>- Khách hàng trúng quà “Ve xem phim”:</b>
                                                <ul className="content4">
                                                    <li>* Nội dung hiển thị trên Game: Chúc mừng quý khách đã nhận được
                                                        quà là Voucher vé xem phim tại rạp CGV.
                                                    </li>
                                                    <li>* Nội dung SMS : [TB]: Chuc mung Quy khach da trung qua ve xem
                                                        phim CGV. ..., su dung den 24h00 ngay 31/07/2019. Quy khach vui
                                                        long mang tin nhan toi bat ky rap chieu phim CGV tren toan quoc
                                                        de doi thanh ve xem phim. Chi tiet LH 198 (0d). Tran trong.”
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <b>- Khách hàng trúng quà “1 tuan Free Keeng/ Mocha/ 5Dmax/ MyClip”:</b>
                                                <ul className="content4">
                                                    <li>* Nội dung hiển thị trên Game: Chúc mừng quý khách đã nhận được
                                                        quà là 1 tuần miễn phí Keeng/ Mocha/ 5Dmax/ MyClip.
                                                    </li>
                                                    <li>* Nội dung SMS: [TB]: Chuc mung Quy khach da trung 1 tuan xem
                                                        video hai, nhac, phim dac sac mien phi tren Keeng/ Mocha/ 5Dmax/
                                                        MyClip. Quy khach vui long truy cap link (link dich vu ...) de
                                                        su dung uu dai. Chi tiet LH 198 (0d). Tran trong”.
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span className="list-style">v<span
                                            style={{font:"7.0pt Times New Roman", fontFamily:'Wingdings'}}></span></span> Quy định về số lượt chơi
                                        <ul className="content3">
                                            <li>- Mỗi khách hàng có các lượt chơi miễn phí/ngày (áp dụng theo ngày –
                                                Người chơi sử dụng lượt chơi miễn phí đến 24h00 cùng ngày, không cộng
                                                dồn vào ngày kế tiếp nếu không sử dụng hết) theo quy định mỗi lần vào
                                                chơi được 3 lượt miễn phí, được cộng thêm 3 lượt nếu người chơi vào lại
                                                game sau >= 1 giờ đồng hồ kể từ lần vào chơi trước.
                                            </li>
                                            <li>
                                                - Quy tắc cộng thêm lượt:
                                                <ul className="content4">
                                                    <li>+ Thuê bao đăng ký tài khoản ứng dụng lần đầu các apps Mocha,
                                                        MyClip, 5Dmax, Keeng, MyViettel trong thời gian diễn ra chương
                                                        trình được thêm 10 lượt chơi.
                                                    </li>
                                                    <li>+ Giao dịch chia sẻ Game: Được cộng 5 lượt chơi trong suốt thời
                                                        gian diễn ra chương trình.
                                                    </li>
                                                    <li>+ Mời bạn bè vào chơi trên Mocha: Người mời được cộng thêm 5
                                                        lượt chơi khi người được mời tham gia chơi lần đầu tiên.
                                                    </li>
                                                    <li>+ Mỗi giao dịch đăng ký mới các dịch vụ được số lượt chơi như
                                                        sau:
                                                    </li>
                                                    <li>- Với các dịch vụ VAS/Media: Thuê bao đăng ký mới Mocha, MyClip,
                                                        5Dmax, Keeng được 10 lượt chơi. Lưu ý: chỉ áp dụng với thuê bao
                                                        đăng ký mới có trừ phí > 0đ.
                                                    </li>
                                                    <li>- Với các dịch vụ viễn thông: số gói cước áp dụng và số lượt
                                                        tương ứng như sau:
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <table className="table" style={{fontSize:'11px', textAlign:'left'}}>
                                            <thead>
                                            <tr>
                                                <td>STT</td>
                                                <td>Loại gói</td>
                                                <td>Chu kỳ</td>
                                                <td>Tên gói</td>
                                                <td>Số lượt</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Data</td>
                                                <td>Tháng</td>
                                                <td>UMAX300</td>
                                                <td>165</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Data</td>
                                                <td>Tháng</td>
                                                <td>MIMAX200</td>
                                                <td>110</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Data</td>
                                                <td>Tháng</td>
                                                <td>MIMAX125</td>
                                                <td>75</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Data</td>
                                                <td>Tháng</td>
                                                <td>MIMAX90</td>
                                                <td>50</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>Data</td>
                                                <td>Tháng</td>
                                                <td>MIMAX70</td>
                                                <td>39</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>Data</td>
                                                <td>Tháng</td>
                                                <td>ECOD50</td>
                                                <td>28</td>
                                            </tr>
                                            <tr>
                                                <td>7</td>
                                                <td>Data</td>
                                                <td>Tháng</td>
                                                <td>TOMD30</td>
                                                <td>17</td>
                                            </tr>
                                            <tr>
                                                <td>8</td>
                                                <td>Data</td>
                                                <td>Tháng</td>
                                                <td>TOMD10</td>
                                                <td>6</td>
                                            </tr>
                                            <tr>
                                                <td>9</td>
                                                <td>Data</td>
                                                <td>Ngày</td>
                                                <td>MT20N</td>
                                                <td>10</td>
                                            </tr>
                                            <tr>
                                                <td>10</td>
                                                <td>Data</td>
                                                <td>Ngày</td>
                                                <td>MI10D</td>
                                                <td>5</td>
                                            </tr>
                                            <tr>
                                                <td>11</td>
                                                <td>Data</td>
                                                <td>Ngày</td>
                                                <td>G3</td>
                                                <td>5</td>
                                            </tr>
                                            <tr>
                                                <td>12</td>
                                                <td>Data</td>
                                                <td>Ngày</td>
                                                <td>MI5D</td>
                                                <td>3</td>
                                            </tr>
                                            <tr>
                                                <td>13</td>
                                                <td>Data</td>
                                                <td>Ngày</td>
                                                <td>MI3K</td>
                                                <td>2</td>
                                            </tr>
                                            <tr>
                                                <td>14</td>
                                                <td>Data</td>
                                                <td>Ngày</td>
                                                <td>MI2K</td>
                                                <td>1</td>
                                            </tr>
                                            <tr>
                                                <td>15</td>
                                                <td>Thoại</td>
                                                <td>Tháng</td>
                                                <td>KM119</td>
                                                <td>60</td>
                                            </tr>
                                            <tr>
                                                <td>16</td>
                                                <td>Thoại</td>
                                                <td>Tháng</td>
                                                <td>KM99</td>
                                                <td>50</td>
                                            </tr>
                                            <tr>
                                                <td>17</td>
                                                <td>Thoại</td>
                                                <td>Tháng</td>
                                                <td>KM69</td>
                                                <td>35</td>
                                            </tr>
                                            <tr>
                                                <td>18</td>
                                                <td>Thoại</td>
                                                <td>Tháng</td>
                                                <td>KM50N</td>
                                                <td>26</td>
                                            </tr>
                                            <tr>
                                                <td>19</td>
                                                <td>Thoại</td>
                                                <td>Tháng</td>
                                                <td>KM49</td>
                                                <td>25</td>
                                            </tr>
                                            <tr>
                                                <td>20</td>
                                                <td>Nạp thẻ</td>
                                                <td>Ngày</td>
                                                <td></td>
                                                <td>10</td>
                                            </tr>
                                            <tr>
                                                <td>21</td>
                                                <td>Nạp thẻ</td>
                                                <td>Ngày</td>
                                                <td></td>
                                                <td>25</td>
                                            </tr>
                                            <tr>
                                                <td>22</td>
                                                <td>Nạp thẻ</td>
                                                <td>Ngày</td>
                                                <td></td>
                                                <td>50</td>
                                            </tr>
                                            <tr>
                                                <td>23</td>
                                                <td>Nạp thẻ</td>
                                                <td>Ngày</td>
                                                <td></td>
                                                <td>100</td>
                                            </tr>
                                            <tr>
                                                <td>24</td>
                                                <td>Nạp thẻ</td>
                                                <td>Ngày</td>
                                                <td></td>
                                                <td>250</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div><b><i>Lưu ý: </i></b></div>
                                        <ul className="content3">
                                            <li>- Chỉ áp dụng với giao dịch đăng ký mới, không áp dụng với giao dịch gia
                                                hạn, áp dụng cho tất cả các kênh đăng ký.
                                            </li>
                                            <li>- Chương trình nạp thẻ cộng lượt chơi áp dụng cho thuê bao trả trước.
                                            </li>
                                            <li>- Trường hợp nạp thẻ hộ thuê bao A nạp cho thuê bao B thì lượt chơi cộng
                                                cho thuê bao B. (thuê bao được nạp thẻ).
                                            </li>
                                            <li>- Hình thức nạp thẻ áp dụng với tất cả các kênh: thẻ cào giấy, anypay,
                                                toup.
                                            </li>
                                            <li>- Số lượt chơi thêm có thời hạn sử dụng từ thời điểm khách hàng nhận
                                                được tin nhắn thông báo cho tới khi kết thúc chương trình.
                                            </li>
                                        </ul>
                                        <div>Nội dung tin trả về: đồng nhất là <b>VT_QUATANG</b>.</div>
                                        <div>
                                            Tin trả về cho giao dịch đăng ký dịch vụ Thoại/DATA/VAS/Media: Chuc mung Quy
                                            khach da duoc tang X luot choi Game Trao yeu thuong- Trung kim cuong. Moi
                                            quy khach truy cap App Myviettel, Mocha, Keeng, MyClip, 5Dmax hoac truy cap
                                            Viettel.vn, Shop.viettel.vn de tham gia tro choi. Chi tiet LH 198 (0d). Tran
                                            trong.
                                        </div>
                                    </li>
                                    <li>Ghi chú: Trường hợp KH hết lượt chơi miễn phí , popup thông báo mời KH quay lại
                                        chơi vào giờ tiếp theo: <b>Quý khách đã hết lượt chơi miễn phí, mời quý khách
                                            tiếp tục chơi vào một giờ sau hoặc thực hiện các nhiệm vụ sau để thêm lượt
                                            chơi.</b></li>
                                    <li>
                                        <span className="list-style"  style={{fontFamily:'Wingdings'}}>v<span
                                            style={{font:"7.0pt Times New Roman"}}></span></span> Quy định về thời hạn sử
                                        dụng quà tặng:
                                        <ul className="content3">
                                            <li>- Quà tặng là SMS nội mạng: Thời hạn sử dụng 7 ngày kể từ thời điểm
                                                trúng quà.
                                            </li>
                                            <li>- Quà tặng là phút gọi nội mạng: Thời hạn sử dụng 7 ngày kể từ thời điểm
                                                trúng quà.
                                            </li>
                                            <li>- Quà tặng là Data: Thời hạn sử dụng 7 ngày kể từ thời điểm trúng quà.
                                            </li>
                                            <li>- Quà tặng là 1 tuần miễn phí Mocha/Keeng/MyClip/ 5Dmax.: Một tuần sử
                                                dụng miễn phí dịch vụ Mocha/Keeng/MyClip/ 5Dmax.
                                            </li>
                                            <li>- Quà tặng là Voucher vé xem phim CGV: Sử dụng đến 24h ngày
                                                31/07/2019.
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>8.2. Thời gian, cách thức phát hành bằng chứng xác định trúng
                            quà</b></p>
                        <p>- Chương trình được thực hiện online trên các sản phẩm, ứng dụng của Viettel đảm bảo công
                            bằng, hệ thống ghi nhận và thông báo trực tiếp trên sản phẩm, ứng dụng mà khách hàng tham
                            gia chơi Game hoặc đồng thời tin nhắn gửi Khách hàng tùy theo từng loại quà (quy định tại
                            8.1). Đối với danh mục quà tặng hiện vật và quà tặng viễn thông nêu ở mục 7 (trừ quà cho
                            người chơi điểm cao nhất mục 1.3) sẽ thông báo trúng quà online trên các sản phẩm, ứng dụng
                            của Viettel, còn đối với quà tặng là điểm may mắn, đến ngày 31/03/2019, Viettel sẽ tiến hành
                            thống kê điểm để trao quà cho người chơi điểm cao nhất nêu ở mục 7.</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>8.3. Quy định về bằng chứng xác định trúng quà</b></p>
                        <p>- Các thuê bao tham dự chương trình hợp lệ chính là số thuê bao di động Viettel.</p>
                        <p>- Tổng số quà tặng Tổng Công ty Viễn Thông Viettel - Chi nhánh Tập đoàn Công nghiệp - Viễn
                            thông Quân đội dự tính trao trong thời gian khuyến mại là: 8.300.000 quà tặng. </p>
                        <p>- Quà tặng được tính là hợp lệ khi có thông báo hiển thị trên giao diện Game và tin nhắn
                            (SMS) thông báo từ Viettel. Nội dung tin nhắn đã nêu rõ ở mục 8.1. Đối với quà tặng dành cho
                            người chơi điểm cao nhất 1.3 nêu ở mục 7 sẽ được xác định thông qua chương trình thống kê
                            điểm may mắn người chơi trúng trong Game. </p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>8.4. Thời gian, địa điểm và cách thức xác định trúng quà</b></p>
                        <p>- Quà tặng được xác định ngay khi Khách hàng thực hiện trúng quà khi chơi Game.</p>
                        <p>- Quà tặng cho người chơi điểm cao nhất nêu ở mục 7 được xác định vào ngày 31/03/2019 khi
                            Viettel tiến hành thống kê điểm may mắn người chơi đã giành được trong Game.</p>
                        <p>- Ngay sau khi khách hàng nhận được quà tặng cho người chơi điểm cao nhất, hệ thống Viettel
                            sẽ thông báo đến trực tiếp đến thuê bao qua tin nhắn.</p>
                        <p>- Hệ thống sẽ ghi nhận log khi miễn phí dịch vụ Vas/Media, cộng SMS nội mạng, phút gọi nội
                            mạng, data cho khách hàng, thông tin gồm: số điện thoại, ngày tháng năm, giờ phút giây,
                            thông tin cộng quà tặng, trạng thái thành công hay không.</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>8.5 Thông báo trúng quà</b></p>
                        <p>- Khách hàng sẽ nhận được thông báo nhận được quà tặng trên Game và tin nhắn thông báo, hướng
                            dẫn cách thức nhận quà trong vòng 24 giờ, kể từ thời điểm khách hàng trúng quà khi chơi
                            Game.</p>
                        <p>+ Quà tặng là SMS, phút gọi nội mạng, data: Viettel thông báo qua popup trên Game.</p>
                        <p>+ Nội dung tin nhắn thông báo trúng quà đã nêu rõ ở mục 8.1.</p>
                        <p>+ Quà tặng là voucher vé xem phim CGV: Viettel thông báo cách thức nhận qua popup trên Game
                            và gửi SMS mã voucher quà tặng cho Khách hàng.</p>
                        <p>+ Quà tặng hiện vật: Bộ trang sức kim cương vàng trắng 14k, bộ trang sức ngọc trai vàng trắng
                            14k: Khách hàng sẽ nhận được popup thông báo trên Game, tin nhắn thông báo trúng quà và
                            hướng dẫn nhận quà. Nhân viên của Viettel sẽ liên hệ với khách hàng trúng quà để làm các thủ
                            tục nhận quà. Khách hàng trúng quà có trách nghiệm cung cấp các thông tin, giấy tờ cá nhân
                            (chứng minh thư, hộ khẩu), nộp thuế theo quy định của Nhà nước để nhận quà. </p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>8.6. Thời gian địa điểm, cách thức và thủ tục trao quà</b></p>
                        <p>- Quà tặng là dịch vụ Vas, Media, SMS, phút gọi nội mạng, data: Hệ thống sẽ tự động cộng tài
                            khoản cho khách hàng.</p>
                        <p>- Đối với quà tặng là voucher vé xem phim: Viettel gửi mã voucher qua SMS đến số thuê bao
                            Khách hàng.</p>
                        <p>- Đối với quà tặng hiện vật: Bộ trang sức kim cương vàng trắng 14k, bộ trang sức ngọc trai
                            vàng trắng 14k sẽ được trao cho khách hàng trong vòng 45 ngày kể từ khi kết thúc chương
                            trình. </p>
                        <p>+ Tổng công ty Viễn thông Viettel liên hệ với khách hàng nhận được quà, hướng dẫn khách hàng
                            các thủ tục nhận quà và tổ chức trao quà. Danh sách khách hàng nhận được quà đặc biệt sẽ
                            được công bố trên website: vietteltelecom.vn.</p>
                        <p>+ Thủ tục cần có để nhận quà: Người nhận được quà là hiện vật phải là chính chủ thuê bao: Khi
                            nhận quà chủ thuê bao phải xuất trình CMND/Hộ chiếu còn hiệu lực theo quy định (CMND không
                            quá 15 năm kể từ ngày được cấp, hộ chiếu phải còn hiệu lực trong vòng 06 tháng tính đến thời
                            điểm nhận quà hoặc các giấy tờ khác thay thế nhưng phải có ảnh và có xác nhận của chính
                            quyền địa phương trong vòng 06 tháng tính đến thời điểm nhận quà). Nếu ủy quyền cho người
                            khác nhận quà thì người đi nhận phải có CMND bản photo và bản gốc của chính chủ trúng quà,
                            CMND bản photo và bản gốc của người được ủy quyền, giấy ủy quyền đi nhận giải quà của người
                            trúng quà. Trường hợp người trúng quà đặc biệt hiện vật không phải là chính chủ thuê bao:
                            Khi nhận, người nhận được quà phải xuất trình CMND, cung cấp ít nhất 05 số điện thoại thường
                            xuyên liên lạc trong vòng 06 tháng gần nhất. </p>
                        <p>+ Quà tặng sẽ được trao tại địa chỉ của khách hàng hoặc tại các cửa hàng Viettel tỉnh/TP.</p>
                        <p>+ Trong trường hợp quá 30 ngày kể từ ngày kết thúc chương trình mà Tổng Công ty Viễn thông
                            Viettel không thể liên lạc được với khách hàng hoặc khách hàng không liên hệ với Tổng Công
                            ty Viễn thông Viettel để làm thủ tục nhận quà thì quà tặng sẽ không được trao.</p>
                        <p>+ Đối với quà tặng Bộ trang sức kim cương vàng trắng 14k, bộ trang sức ngọc trai vàng trắng
                            14k: Khách hàng nhận quà phải nộp thuế thu nhập không thường xuyên theo quy định của pháp
                            luật hiện hành.</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>9. Đầu mối giải đáp thắc mắc cho khách hàng về các vấn đề liên quan
                            đến chương trình khuyến mại (người liên hệ, điện thoại...):</b></p>
                        <p>- Trung tâm Chăm sóc Khách hàng Viettel Telecom.</p>
                        <p>- Số điện thoại: 198 (miễn phí).</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>10. Trách nhiệm thông báo:</b></p>
                        <p>- Tổng Công ty Viễn thông Viettel có trách nhiệm thông báo đầy đủ chi tiết nội dung của thể
                            lệ chương trình, trị giá quà tặng, số lượng quà tặng và đầy đủ thông tin khách hàng nhận
                            được quà tặng trên ít nhất một phương tiện thông tin đại chúng, tại địa điểm tổ chức chương
                            trình.</p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>11. Các qui định khác</b></p>
                        <p>- Khách hàng nhận được quà tặng phải nộp thuế thu nhập không thường xuyên (nếu có) theo quy
                            định của pháp luật hiện hành. Nếu được sự chấp thuận của Khách hàng nhận được quà, Tổng Công
                            ty Viễn thông Viettel sẽ khấu trừ khoản thuế thu nhập không thường xuyên (nếu có) theo quy
                            định của pháp luật trên giá trị quà tặng mà khách hàng đã nhận và thay mặt khách hàng nộp
                            theo quy định.</p>
                        <p>- Khách hàng tham dự chương trình khuyến mại này vẫn được hưởng quyền lợi từ các chương trình
                            khuyến mại khác của Tổng Công ty Viễn thông Viettel (nếu đủ điều kiện).</p>
                        <p>- Tổng Công ty Viễn thông Viettel hoàn toàn chịu trách nhiệm trong việc quản lý tính chính
                            xác của bằng chứng xác định trúng quà và đưa bằng chứng xác định trúng quà vào lưu thông
                            trong chương trình khuyến mại. Trường hợp bằng chứng xác định trúng quà của Tổng Công ty
                            phát hành có sai sót gây hiểu nhầm cho khách hàng trong việc trúng quà, Tổng Công ty Viễn
                            thông Viettel có trách nhiệm trao các quà này cho khách hàng trúng quà. Việc tổ chức chương
                            trình khuyến mại phải đảm bảo tính khách quan và công khai.</p>
                        <p>- Tổng Công ty Viễn thông Viettel được sử dụng tên và hình ảnh của khách hàng trúng quà cho
                            mục đích quảng cáo thương mại.</p>
                        <p>- Trong trường hợp xảy ra tranh chấp liên quan đến chương trình khuyến mại này, Tổng Công ty
                            Viễn thông Viettel có trách nhiệm trực tiếp giải quyết, nếu không thỏa thuận được tranh chấp
                            sẽ được xử lý theo quy định của pháp luật.</p>
                        <p>- Đối với những quà tặng không có người nhận, Tổng Công ty Viễn thông Viettel phải có trách
                            nhiệm trích nộp 50% giá trị đã công bố của giá trị quà tặng đó vào ngân sách nhà nước theo
                            quy định tại khoản 4 điều 96 Luật thương mại.</p>
                    </div>
                    </body>
                </div>
            );
    }
}

export default Thelegame83;
