import React from 'react';
import Head from 'next/head';
import {DOMAIN_IMAGE_STATIC} from "../src/config/Config.js";
import Helper from "../src/utils/helpers/Helper";
const clip_logo = DOMAIN_IMAGE_STATIC + "clip_logo1.png";

class Metaseo extends React.Component {
    render() {

        let {type, data} = this.props;
        type = type ? type : 'home';

        let title = 'Mocha video - Mạng xã hội chia sẻ video';
        let description = 'Cập nhật trending, phim ngắn hot nhất, video hài đặc sắc cùng các thông tin nóng nhất về giải trí, thể thao, âm nhạc, xã hội Việt Nam - Quốc Tế...';
        let image = clip_logo;
        let url = 'http://video.mocha.com.vn';
        var encodeUrl = encodeURIComponent("http://video.mocha.com.vn/");

        if (type === 'video') {
            title = Helper.checkObjExist(data, 'title') ? data.title : 'Mocha video - Mạng xã hội chia sẻ video';
            description = Helper.checkObjExist(data, 'description') ? data.description : 'Cập nhật trending, phim ngắn hot nhất, video hài đặc sắc cùng các thông tin nóng nhất về giải trí, thể thao, âm nhạc, xã hội Việt Nam - Quốc Tế...';
            image = data.image ? data.image : clip_logo;
            url = Helper.checkObjExist(data, 'link') ? data.link : 'http://video.mocha.com.vn';
            encodeUrl = encodeURIComponent("http://video.mocha.com.vn/" + data.path);
        } else if (type === 'channel') {
            title = Helper.checkObjExist(data, 'title') ? ('Kênh - ' + data.title) : 'Mocha video - Mạng xã hội chia sẻ video';
            description = Helper.checkObjExist(data, 'description') ? data.description : 'Cập nhật trending, phim ngắn hot nhất, video hài đặc sắc cùng các thông tin nóng nhất về giải trí, thể thao, âm nhạc, xã hội Việt Nam - Quốc Tế...';
            image = Helper.checkObjExist(data, 'image') ? data.image : clip_logo;
            url = Helper.checkObjExist(data, 'link') ? data.link : 'http://video.mocha.com.vn';
            encodeUrl = encodeURIComponent("http://video.mocha.com.vn/" + data.path);
        } else if (type === 'category') {
            title = Helper.checkObjExist(data, 'title') ? ('Chuyên mục - ' + data.title) : 'Mocha video - Mạng xã hội chia sẻ video';
            description = Helper.checkObjExist(data, 'description') ? data.description : 'Cập nhật trending, phim ngắn hot nhất, video hài đặc sắc cùng các thông tin nóng nhất về giải trí, thể thao, âm nhạc, xã hội Việt Nam - Quốc Tế...';
            image = Helper.checkObjExist(data, 'image') ? data.image : clip_logo;
            encodeUrl = encodeURIComponent("http://video.mocha.com.vn/" + data.path);

            // url = window.location.href;
        } else if (type === 'phim') {
            title = 'Phim - Mocha video';
            description = Helper.checkObjExist(data, 'description') ? data.description : 'Cập nhật trending, phim ngắn hot nhất, video hài đặc sắc cùng các thông tin nóng nhất về giải trí, thể thao, âm nhạc, xã hội Việt Nam - Quốc Tế...';
            image = Helper.checkObjExist(data, 'image') ? data.image : clip_logo;
            url = 'http://video.mocha.com.vn/phim.html';
            encodeUrl = encodeURIComponent("http://video.mocha.com.vn/phim.html");
        }
        return (
            <Head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
                <meta http-equiv="Content-Language" content="vi" />
                <meta http-equiv="X-UA-Compatible" content="requiresActiveX=true"/>
                <meta name="ROBOTS" content="index, follow"/>
                <meta property="fb:app_id" content="482452638561234"/>
                <meta property="fb:admins" content="482452638561234" />

                <title>{title}</title>
                <meta property="title" content={title}/>
                <meta name="description" content={description}/>
                <meta property="og:video:type" content="application/x-shockwave-flash" />
                <meta property="og:image:width" content="390" />
                <meta property="og:image:height" content="320" />

                {/* META FOR FACEBOOK */}
                <meta property="keywords" content="Mocha video - Mạng xã hội chia sẻ video" />
                <meta property="og:type" content="website"/>
                <meta property="og:video" content="" />
                <meta property="og:url"  content="" />
                <meta property="og:image" itemProp="thumbnailUrl" content={image}/>
                <meta property="og:title" content={title} />
                <meta property="og:description" content={description} itemProp="description" />
                {/* END META FOR FACEBOOK */}

                <meta name="al:ios:url" property="al:ios:url" content={"mocha://mochavideo?ref=" + encodeUrl}/>
                <meta name="al:ios:app_store_id" property="al:ios:app_store_id" content="946275483"  />
                <meta name="al:ios:app_name" property="al:ios:app_name" content="Mocha"  />
                <meta name="al:android:url" property="al:android:url" content={"mocha://mochavideo?ref=" + encodeUrl}/>
                <meta name="al:android:package" property="al:android:package" content="com.viettel.mocha.app"  />
                <meta name="al:android:app_name" property="al:android:app_name" content="Mocha"  />

                {/* Twitter Card */}
                <meta name="twitter:url" content={url}/>
                <meta name="twitter:title" content={title}/>
                <meta name="twitter:description" content={description}/>
                <meta name="twitter:image" content={image}/>
                {/* End Twitter Card */}

                <link href="https://plus.google.com/103294122996094564415" rel ="publisher" />
                <meta name="author" content=".vn" />
                <meta name="copyright" content=".vn" />
                <link rel="shortcut icon" href="http://live84.keeng.net/playnow/images/static/web/gioi_thieu_app/favicon.ico"/>
            </Head>
        );
    }
}

export default Metaseo;
