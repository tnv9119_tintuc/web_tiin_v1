import React, {Component} from 'react';
import Helper from "../src/utils/helpers/Helper";
//import '../styles/VideoMocha/VideoItem.css';
import SearchApi from "../src/services/api/Search/SearchApi";
//import {Link} from '../src/routes';
import Link from 'next/link';
//import SlideResponsive from "../../components/Slides/SlideResponsive";hanv:chua co api

import FilmSlideForSearch from "../src/components/Film/partials/FilmSlideForSearch";
import LoadingScreen from "../src/components/Global/Loading/LoadingScreen";
import Channel from "../src/components/Slides/SlideChannelSearch/Channel";
import {DEFAULT_IMG} from "../src/config/Config";

class Search extends Component {
    constructor(props) {
        super(props);

        var values = this.props.query;
        var query = values.key ? values.key : '';

        this.state = {
            isError: null,
            isLoaded: false,
            query: query,
            videos: [],
            channels: [],
            films: []
        };
    }

    static getInitialProps({query}) {
        return {
            query : query
        }

    }

    componentDidMount() {
        this.getDataSearch();
    }

    componentDidUpdate(prevProps, prevState) {
        var values = this.props.query;
        var query = values.key ? values.key : '';

        if (query !== this.state.query) {
            this.setState({
                query: query
            }, () => {
                this.getDataSearch();
            });

        }
    }

    getDataSearch() {
        let query = this.state.query;

        SearchApi.searchVideo(query).then(
            (response) => {
                if(response && response.data) {
                    Helper.renewToken(response.data);

                    this.setState({
                        isLoaded: true,
                        videos: Helper.sortVideoSearch(Helper.checkArrNotEmpty(response.data, 'data.listVideo') ? response.data.data.listVideo : [], query),
                    });
                }
            },
            (error) => {
                this.setState({
                    isError: true
                });
            });

        SearchApi.searchChannel(query).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    isLoaded: true,
                    channels: Helper.sortChannelSearch(Helper.checkArrNotEmpty(data, 'data.listChannel') ? data.data.listChannel : [], query),
                });
            },
            (error) => {
                this.setState({
                    isError: true
                });
            });

        SearchApi.searchFilm(query).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    isLoaded: true,
                    films: Helper.sortFilmSearch(Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : [], query),
                });
            },
            (error) => {
                this.setState({
                    isError: true
                });
            });
    }

    render() {

        const {isError, isLoaded, query, videos, channels, films} = this.state;
        if (isError || !isLoaded) {
            return <LoadingScreen/>;
        } else {
            return (
                <div id="body">

                    <div className="box-search">
                        <div className="mocha-video-box box-search-sub">
                            <div className="breadcrum-clip">
                                <p style={{fontSize: '17px'}}>Kết quả tìm kiếm với từ khóa "{query}" :</p>
                            </div>

                            {/*============= Channel ===========*/}
                            {channels.length > 0 ?
                                <div className="keeng-cate-hot" id="cate_hot">
                                    <div className="wrap-bpk">
                                        <h3 className="search-h3">
                                            <a><span>KÊNH</span></a>
                                        </h3>
                                        <Channel channels={Helper.checkIsChannelNotShow(channels)} numberItem={5}/>
                                    </div>
                                </div>
                                :
                                ''
                            }

                            {/*================ FILM ==============*/}
                            {films.length > 0
                                ?
                                <div className="wrap-bpk search-phim-border">
                                    <h3 className="search-h3 search-h3-phim">
                                        <a><span>PHIM</span></a>
                                    </h3>
                                    {/*chưa có api*/}
                                    {/*<SlideResponsive video={films} type={'boxFilmSearch'}/>*/}
                                    <FilmSlideForSearch films={films} numberItem={5}/>
                                </div>
                                : ''}

                            {/*============= VIDEO ====================*/}
                            {videos.length > 0
                                ?
                                <div className="wrap-bpk">
                                    <h3 className="search-h3 search-h3-video">
                                        <a><span>VIDEO</span></a>
                                    </h3>
                                    <div className="box-list-video">
                                        {this.showVideo(videos)}
                                    </div>
                                </div>
                                : ''}

                        </div>
                    </div>
                </div>
            );

            return null;
        }

    }

    showVideo(results) {
        if (results && results.length > 0) {
            results = Helper.checkIsChannelNotShowVideo(results);
            return results.map((result, index) => {
                if (result && index < 5) {

                    return <div className="chanel-video-small" key={index}>
                        <div className="cvs-lnk-img">
                            <Link href='/video'
                                  as={Helper.replaceUrl(result.link) + '.html'}
                            >
                                <a>
                                    <img title={result.name} className="lazy img-responsive vi-img"
                                         src={result.image_path}
                                         onError={(e) => {
                                             e.target.onerror = null;
                                             e.target.src = DEFAULT_IMG
                                         }}
                                    />
                                </a>

                            </Link>
                        </div>

                        <div className="info-video-search">
                            {/*<div className="cvs-info">*/}
                            <h3 className="cvs-info-h2">
                                <Link href='/video'
                                      as={Helper.replaceUrl(result.link) + '.html'}
                                >
                                    <a title={result.name}>
                                        {result.name}
                                    </a>

                                </Link>
                            </h3>
                            {/*</div>*/}
                            {Helper.checkArrNotEmpty(result, 'channels') && result.channels[0] ?
                                <div className="playnow-home-chanel-new">
                                    <Link href='/video'
                                          as={Helper.replaceUrl(result.link) + '.html'}
                                    >
                                        <a className="playnow-chanel-link-new">
                                            <img alt={result.channels[0].name} src={result.channels[0].url_avatar}
                                                 onError={(e) => {
                                                     e.target.onerror = null;
                                                     e.target.src = DEFAULT_IMG
                                                 }}
                                            />
                                        </a>

                                    </Link>

                                    <div className="play-chanel-info-new">
                                        <Link href='/video'
                                              as={Helper.replaceUrl(result.link) + '.html'}
                                        >
                                            <a title={result.channels[0].name}>
                                                <p className="play-chanel-view-new">
                                                    {Helper.formatNumber(result.isView)} lượt xem
                                                </p>
                                                <p className="play-chanel-name">
                                                    {result.channels[0].name}
                                                </p>
                                            </a>

                                        </Link>
                                    </div>
                                    <a className="chanel-dot-more"/>
                                </div>
                                : ''
                            }
                        </div>
                    </div>
                }
            });
        }
    }
}

export default Search;
