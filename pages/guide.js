import React, {Component} from "react";
//import '../../../styles/GioiThieuApp.scss';
import Link from 'next/link';

import {DOMAIN_IMAGE_STATIC} from "../src/config/Config.js";
const logo_mocha = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_mocha.png";
const logo_viettel = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_viettel.png";
const logo_congthuong = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_congthuong.png";


export class Guide extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id="wrapper">
                <style dangerouslySetInnerHTML={{__html: "\n #header { z-index: 9; width: 100%; background: #f7f7f7; position: static;}\n #body {min-height: 0; height: 100%; overflow: hidden; width: 100%; background: #e0d2f9 !important ; } \n .mocha-guide {padding: 15px 0px; background: none;} \n #footer { background: #f5f5f5 none repeat scroll 0 0; padding: 25px 0px 25px 0px; margin-top: 0;}"}}/>
                <div id="header">
                    <div className="header-top">
                        <Link href="/" >
                            <a style={{float: 'left', marginTop: '5px'}}>
                                <img height={35} src={logo_mocha} />
                            </a>
                        </Link>
                        <Link  href="/contact" as="/contact.html">
                            <a className="top-menu">LIÊN HỆ</a>
                        </Link>
                        <Link  href="/faq" as="/faq.html">
                            <a className="top-menu">HỎI &amp; ĐÁP</a>
                        </Link>
                        <Link href="/gioithieuapp" as="/gioithieuapp.html" >
                            <a className="top-menu">TÍNH NĂNG</a>
                        </Link>
                    </div>
                </div>
                <div id="body">
                    <div className="mocha-guide" style={{paddingTop: '7px'}}>
                        <p style={{width: '100%', color: 'rgb(254, 0, 0)', zIndex: 9999, fontSize: '14px', fontFamily: 'arial', top: '45px', textAlign: 'center', padding: '0px 0px 8px'}}><a style={{color: 'rgb(254, 0, 0)'}} href="http://mocha.com.vn/mochafree.html">THÔNG BÁO: Hiện nay, khách hàng dùng các gói cước Sinh viên của Viettel được ưu đãi MIỄN PHÍ GỌI NỘI MẠNG khi dùng Mocha. Bấm để xem thêm! </a></p>
                        <div className="mocha-guide-content">
                            <p className="mocha-guide-p">Hướng dẫn</p>
                            <div className="mocha-detail">
                                <p>Quá dễ dàng để sử dụng Mocha. Hệ thống sẽ tự động nhận diện số điện thoại và hỗ trợ bạn đăng nhập bằng chính số điện thoại đó. Đảm bảo tiện lợi và tăng tính bảo mật cho bạn khi sử dụng Ứng dụng của Viettel.</p>
                                <p><strong>Tin nhắn thoại</strong></p>
                                <p>Trên cửa sổ chat với người mà  bạn muốn gửi tin nhắn thoại, chọn biểu tượng Microphone. Sau đó, nhấn nút "Giữ để nói"  và bắt đầu thu âm. Thả tay ra để gửi tin nhắn đi, "Giữ và kéo ra ngoài" để hủy.</p>
                                <p><strong>Tính năng trò chuyện nhóm</strong></p>
                                <p>Trong màn hình chính, chọn "Tạo nhóm mới". Sau đó, bạn chỉ cần nhập tên trong danh bạ hoặc số điện thoại của những người bạn muốn cùng trò chuyện rất dễ dàng và nhanh chóng.</p>
                                <p><strong>Tính năng SMS out</strong></p>
                                <p>Duy nhất tại Mocha: Người dùng Mocha sẽ được gửi miễn phí tin nhắn cho tất cả thuê bao Viettel, không giới hạn số lượng tin nhắn sau khi đăng ký gói cước Mocha Free (2.500đ/tuần đầu tiên, sau 7 ngày 5.000đ/tuần).</p>
                                <p><strong>Tính năng Cùng nghe nhạc</strong></p>
                                <p>Trên cửa sổ chat với người mà bạn muốn mời cùng nghe nhạc với mình, chọn biểu tượng tai nghe. Bạn chỉ cần nhập tên bài hát mà bạn muốn chia sẻ vào ô tìm kiếm, chọn bài hát và gửi lời mời cùng nghe. Còn gì tuyệt vời hơn khi có thể vừa cùng chat, cùng nghe một giai điệu với người mà mình yêu quý.</p>
                                <p><strong>Tính năng Chuyển tiền qua Mocha</strong></p>
                                <p><i><strong>Đối tượng chuyển tiền:</strong></i> Tất cả các thuê bao Viettel đang hoạt động hai chiều bao gồm thuê bao trả trước và thuê bao trả sau.</p>
                                <p><i><strong>Điều kiện sử dụng: </strong></i></p>
                                <p>+ Thuê bao trả trước được thực hiện  tính năng Chuyển tiền đối với các thuê bao trả trước.</p>
                                <p>+ Thuê bao trả sau có thể thực hiện tính năng trên đối với cả thuê bao trả trước và trả sau.</p>
                                <p>- Cước phí cho mỗi lần giao dịch là 2000đ. Bạn được phép chuyển tối đa 20.000đ/lần giao dịch và không quá 50.000đ một ngày, không giới hạn số lần giao dịch. Phí giao dịch được trừ vào TK gốc đối với thuê bao trả trước và được ghi vào hóa đơn thanh toán trong tháng thực hiện giao dịch đối với thuê bao trả sau.</p>
                                <p>- Thuê bao bị chặn một chiều vẫn có thể nhận tiền từ thuê bao khác nhưng bạn phải mở chặn dịch vụ để sử dụng số tiền được tặng.</p>
                                <p>- Các thuê bao Smart sim, Sim đa năng, Homephone, Dcom không tham gia dịch vụ này.</p>
                                <p><strong>Gói cước dịch vụ:</strong></p>
                                <p><i><strong>Mocha Video: Xem video giải trí, phim, nhạc, đọc báo, nhắn tin miễn phí Data 3G/4G</strong></i></p>
                                <p>- Gói ngày, soạn tin: XN P gửi 5005 (2.000đ/ngày, gia hạn hàng ngày). Hủy dịch vụ: HUY P gửi 5005 (0đ)</p>
                                <p>- Gói tuần, soạn tin: XN P7 gửi 5005 (10.000đ/tuần, gia hạn hàng tuần). Hủy dịch vụ: HUY P7 gửi 5005 (0đ)</p>
                                <p><i><strong>GameTV: Xem video, livestream game miễn phí Data 3G/4G</strong></i></p>
                                <p>- Gói ngày, soạn tin: XN GA gửi 5005 (3.000đ/ngày, gia hạn hàng ngày). Hủy dịch vụ: HUY GA gửi 5005 (0đ)</p>
                                <p>- Gói tuần, soạn tin: XN GA7 gửi 5005 (10.000đ/tuần, gia hạn hàng tuần). Hủy dịch vụ: HUY GA7 gửi 5005 (0đ)</p>
                                <p><i><strong>Mocha Call Out: Gọi điện miễn phí từ Mocha đến một số điện thoại</strong></i></p>
                                <p>- Gói tuần, soạn tin: EC gửi 5005 (4.000đ/tuần, gia hạn hàng tuần). Hủy dịch vụ: HUY EC gửi 5005 (0đ)</p>
                                <p>- Gói tháng, soạn tin: SC gửi 5005 (10.000đ/tuần, gia hạn hàng tháng). Hủy dịch vụ: HUY SC gửi 5005 (0đ)</p>
                                <p><i><strong>Webtoon: Đọc truyện tranh Webtoon trọn bộ</strong></i></p>
                                <p>- Gói ngày, soạn tin: XND gửi 5005 (3.000đ/ngày, gia hạn hàng ngày). Hủy dịch vụ: HUY DT gửi 5005 (0đ)</p>

                                <p>* Tất cả giá cước trên đã bao gồm VAT và phụ phí</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="footer">
                    <div className="mocha-footer">
                        <Link  href="/">
                            <a className="logo-mocha"></a>
                        </Link>
                        <p className="provision-intro">
                            Cơ quan chủ quản: Viettel Telecom - Chăm sóc khách hàng 18008098 (Miễn phí)
                            <br/>
                            <Link  href="/provision" as="/provision.html">
                                <a className="provision">Điều khoản sử dụng</a>
                            </Link>
                            |
                            <Link  href="/guide" as="/guide.html">
                                <a className="provision">Hướng dẫn sử dụng</a>
                            </Link>
                        </p>
                        <a href="#" className="logo-viettel">
                            <img src={logo_viettel}/>
                        </a>
                        <a href="http://online.gov.vn/HomePage.aspx" className="logo-viettel logo-congthuong">
                            <img src={logo_congthuong}/>
                        </a>
                    </div>
                </div>
            </div>

        );
    }

}

export  default  Guide;
