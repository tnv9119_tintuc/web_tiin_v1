import React, {Component} from "react";
//import '../../../styles/GioiThieuApp.scss';
//import {Link} from "../src/routes";
import Link from 'next/link';

import {DOMAIN_IMAGE_STATIC} from "../src/config/Config.js";
const logo_mocha = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_mocha.png";
const home_01 = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/home_01.png";
const home_02 = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/home_02.png";
const home_03 = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/home_03.png";
const home_06 = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/home_06.png";
const logo_viettel = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_viettel.png";
const logo_congthuong = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_congthuong.png";

export class Gioithieuapp extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div id="wrapper">
                <div className="header">
                    <div className="header-top">
                        <Link href="/" >
                            <a style={{float:'left',marginTop:'5px'}}>
                                <img height="35" src={logo_mocha}/>
                            </a>
                        </Link>
                        <Link  href ="/contact" as ="/contact.html">
                            <a className="top-menu">
                                LIÊN HỆ
                            </a>
                        </Link>
                        <Link href="/faq" as="/faq.html">
                            <a className="top-menu" >HỎI & ĐÁP</a>
                        </Link>
                        <Link  href="/gioithieuapp" as="/gioithieuapp.html">
                            <a className="top-menu active">TÍNH NĂNG</a>
                        </Link>
                    </div>
                </div>
                <div className="body">
                    <p className="notice-mocha"
                       style={{width: '100%', color: '#FFF', textAlign: 'center', zIndex: 9999, fontSize: '14px', fontFamily: 'arial', padding: '7px 0px', top: '45px'}}>
                        <a href="http://mocha.com.vn/callout.html" style={{color: 'rgb(254, 0, 0)'}}>THÔNG BÁO: Hiện nay,
                            khách hàng dùng các gói cước Sinh viên của Viettel được ưu đãi MIỄN PHÍ GỌI NỘI MẠNG khi
                            dùng Mocha. Bấm để xem thêm! </a></p>
                    <div className="mocha-top">
                        <Link  href="/">
                            <a className="logo-a"></a>
                        </Link>
                        <div className="mocha-download">
                            <Link  href="/download" as="/download.html" >
                                <a className="mocha-download-a">TẢI NGAY</a>
                            </Link>
                            <Link  href="/download" as ="/download.html">
                                <a className="download-ios"></a>
                            </Link>
                            <Link  href="/download" as="/download.html">
                                <a className="download-android"></a>
                            </Link>
                            <Link  href="/download" as="/download.html">
                                <a className="download-windơns"></a>
                            </Link>
                        </div>

                    </div>
                    <div className="mocha-content">
                        <div className="box-mocha-content">
                            <div className="box-mocha-left">
                                <p className="mocha-h1">Tại sao dùng Mocha ?</p>
                                <ul className="mocha-ul">
                                    <li className="mocha-li"> Không cần mật khẩu, dễ dàng tham gia ngay lập tức</li>
                                    <li className="mocha-li"> Vừa chat. Vừa cùng nghe nhạc</li>
                                    <li className="mocha-li"> Thoải mái thể hiện cảm xúc qua bộ Voice Sticker độc đáo
                                    </li>
                                    <li className="mocha-li"> Nhắn tin, gọi điện đến bạn bè chưa cài Ứng dụng</li>
                                    <li className="mocha-li"> Nhắn tin thoại với tốc độ gửi tin nhắn cực nhanh</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="mocha-item">
                        <div className="mocha-item-content">
                            <div className="mocha-content-left">
                                <p className="mocha-content-p"> Cùng nghe
                                </p>
                                <span className="mocha-content-span">
                     Còn gì thú vị hơn khi có thể vừa chat, vừa cùng chia sẻ và lắng nghe một bài hát với những người thân yêu                     </span>
                            </div>
                            <div className="mocha-content-right">
                                <img src={home_01}/>
                            </div>

                        </div>

                    </div>
                    <div className="mocha-item">
                        <div className="mocha-item-content">
                            <div className="mocha-content-right">
                                <img src={home_02}/>
                            </div>
                            <div className="mocha-content-left">
                                <p className="mocha-content-p"> Voice sticker
                                </p>
                                <span className="mocha-content-span">
                       Nhãn dán âm thanh ấn tượng giúp người dùng có thể dễ dàng thể hiện cảm xúc khi chat thông qua những hình ảnh và âm thanh ngộ nghĩnh.
                       </span>
                            </div>

                        </div>
                    </div>
                    <div className="mocha-item">
                        <div className="mocha-item-content">
                            <div className="mocha-content-left">
                                <p className="mocha-content-p"> Mocha SMS Out
                                </p>
                                <span className="mocha-content-span">
                    Người dùng Mocha có thể gửi tin nhắn đến toàn bộ thuê bao, kể cả các thuê bao chưa cài đặt ứng dụng Mocha.                     </span>
                            </div>
                            <div className="mocha-content-right">
                                <img src={home_03}/>
                            </div>

                        </div>

                    </div>
                    <div className="mocha-item" style={{border:'0px'}}>
                        <div className="mocha-item-content">
                            <div className="mocha-content-right">
                                <img src={home_06}/>
                            </div>
                            <div className="mocha-content-left">
                                <p className="mocha-content-p"> Mocha Call Out
                                </p>
                                <span className="mocha-content-span">
                      Người dùng Mocha có thể gọi điện thoại miễn phí đến tất cả các thuê bao Viettel, kể cả thuê bao chưa cài Mocha.
                       </span>
                            </div>

                        </div>

                    </div>
                </div>
                <div id="footer" style={{borderTop:'1px solid #e1e1e1'}}>
                    <div className="mocha-footer">
                        <Link  href="/">
                            <a className="logo-mocha"></a>
                        </Link>
                        <p className="provision-intro">
                            Cơ quan chủ quản: Viettel Telecom - Chăm sóc khách hàng 18008098 (Miễn phí)
                            <br/>
                            <Link href="/provision" as="/provision.html">
                                <a className="provision" >Điều khoản sử dụng</a>
                            </Link>
                            |
                            <Link  href="/guide" as="/guide.html">
                                <a className="provision">Hướng dẫn sử dụng</a>
                            </Link>
                        </p>
                        <a href="#" className="logo-viettel">
                            <img src={logo_viettel}/>
                        </a>
                        <a href="http://online.gov.vn/HomePage.aspx" className="logo-viettel logo-congthuong">
                            <img src={logo_congthuong}/>
                        </a>
                    </div>

                </div>


            </div>
        );
    }
}

export default Gioithieuapp;
